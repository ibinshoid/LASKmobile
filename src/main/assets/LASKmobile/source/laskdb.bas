SQL.OPEN db, "lask.db"

!Protokoll Tabelle anlegen
	SQL.exec db, "CREATE TABLE IF NOT EXISTS 'protokoll' (id INTEGER PRIMARY KEY AUTOINCREMENT," ~
														+"jahr INTEGER," ~
														+"feldid INTEGER," ~
														+"feldname TEXT," ~
														+"aktionid INTEGER," ~
														+"aktionname TEXT," ~
														+"was INTEGER," ~
														+"kommentar TEXT," ~
														+"anwender TEXT)"
                                                    
!Betriebsdaten laden
    SQL.Raw_query dbQuery, db, "select * from 'feldjahr' where jahr = '0';"
    SQL.Next last, dbQuery, columns$[]
	betriebName$ = columns$[3]
	betriebAdresse$ = columns$[4]
	betriebNummer$ = "0" + columns$[5]
                                                    

!Mittel in Arrays schreiben
    i = 0
    length = 0

    SQL.Raw_query dbQuery, db, "SELECT * FROM 'mittel';"
    Sql.Query.Length length, dbQuery
    dim mittel$[length, 12]
    WHILE last = 0
        SQL.Next last, dbQuery, columns$[]
        !Verhindert dass letzter Eintrag doppelt ist
        if ((last = 0) & columns$[5] <> "") then
            !Mittel in array kopieren
            i = i + 1
            Array.Length length2, columns$[]
            for i2 = 1 to length2
                mittel$[i, i2] = columns$[i2]
            next i2
            !Masseinheit schätzen, wenn nicht da
            if (mittel$[i, 4] = "2") then
                if (mittel$[i, 6] = "") then
                    mittel$[i, 6] = "m³"
                endif
            elseif (mittel$[i, 4] = "3") then
                if (mittel$[i, 6] = "") then
                    mittel$[i, 6] = "kg"
                endif
            endif
            !Einstellungen extra speichern
            if (mittel$[i, 4] = "7") then
				einstellungen$[1] = mittel$[i, 7]
				einstellungen$[2] = mittel$[i, 8]
				einstellungen$[3] = mittel$[i, 9]
				einstellungen$[4] = mittel$[i, 10]
				einstellungen$[5] = mittel$[i, 11]
				einstellungen$[6] = mittel$[i, 12]
            endif
        endif
    REPEAT

!Erntejahre in Array schreiben
    i = 0
    length2 = 0
    last = 0
    SQL.Raw_query dbQuery, db, "SELECT name FROM sqlite_master WHERE type='table' and name like '2___' ORDER BY name;"
    Sql.Query.Length length2, dbQuery
    dim jahre[length2]
    WHILE last = 0
        SQL.Next last, dbQuery, columns$[]
        !Verhindert dass letzter Eintrag doppelt ist
        if (last = 0) then
			if (is_number(columns$[1])) then
				i = i + 1
				jahre[i] = val(columns$[1])
				ernteJahr = val(columns$[1])
			endif
		endif
    REPEAT

FN.DEF laskDb_felderLaden$(was$)
!Felder aus Datenbank laden
	FN.import db, ernteJahr, onRECBREAK$, onCOLBREAK$, filterListe$
    undim columns$[]
    last = 0
	filterListe$ = _$("Alle")+onRECBREAK$+_$("Feld")+onRECBREAK$+_$("Grünland")+onRECBREAK$+_$("- - -")+onRECBREAK$
	
    SQL.Raw_query dbQuery, db, "select * from 'feldjahr' where jahr = '"+ int$(ernteJahr) +"';"
    WHILE last = 0
        SQL.Next last, dbQuery, columns$[]
        ARRAY.COPY columns$[], feldInfo$[]
        !Verhindert dass letzter Eintrag doppelt ist
        if (last = 0) then
			if ((was$ = "Alle") & (WORD$(columns$[4], 1, ";") = "Feld"))
				felder$ += onRECBREAK$ + columns$[1] + onCOLBREAK$ + "<b>" + columns$[3]+"</b><span class='list-item__subtitle'><b>"+laskDB_feldFrucht$(columns$[1])+"</b> ("+WORD$(columns$[4], 2, ";")+")</span>" + onCOLBREAK$ +  FORMAT$("#%.##", VAL(columns$[5]))
			elseif ((was$ = "Alle") & (WORD$(columns$[4], 1, ";") = "Grünland"))
					felder$ += onRECBREAK$ + columns$[1] + onCOLBREAK$ + "<b>" +  columns$[3]+"</b><span class='list-item__subtitle'><b>"+WORD$(columns$[4], 1, ";")+ "</b>" + onCOLBREAK$ +  FORMAT$("#%.##", VAL(columns$[5]))
			elseif ((was$ = "Grünland") & (WORD$(columns$[4], 1, ";") = "Grünland")) then
					felder$ += onRECBREAK$ + columns$[1] + onCOLBREAK$ + "<b>" +  columns$[3]+"</b><span class='list-item__subtitle'><b>"+WORD$(columns$[4], 1, ";")+ "</b>" + onCOLBREAK$ +  FORMAT$("#%.##", VAL(columns$[5]))
			elseif ((was$ = "Feld") & (WORD$(columns$[4], 1, ";") = "Feld")) then
				felder$ += onRECBREAK$ + columns$[1] + onCOLBREAK$ + "<b>" + columns$[3]+"</b><span class='list-item__subtitle'><b>"+laskDB_feldFrucht$(columns$[1])+"</b> ("+WORD$(columns$[4], 2, ";")+")</span>" + onCOLBREAK$ +  FORMAT$("#%.##", VAL(columns$[5]))
			elseif (was$ = laskDB_feldFrucht$(columns$[1])) then
				felder$ += onRECBREAK$ + columns$[1] + onCOLBREAK$ + "<b>" + columns$[3]+"</b><span class='list-item__subtitle'><b>"+laskDB_feldFrucht$(columns$[1])+"</b> ("+WORD$(columns$[4], 2, ";")+")</span>" + onCOLBREAK$ +  FORMAT$("#%.##", VAL(columns$[5]))
			endif
        endif
    REPEAT
FN.RTN felder$
FN.END

FN.DEF laskDB_feldFrucht$(feld$)
!Fruchtart finden
	FN.import db, ernteJahr, onRECBREAK$, onCOLBREAK$, filterListe$
    undim columns$[]
    last = 0
   	frucht$="Acker"

    SQL.Raw_query dbQuery, db, "select * from '" + int$(ernteJahr) + "' where feld = '" + feld$ + "' order by datum;"
    WHILE last = 0
        SQL.Next last, dbQuery, columns$[]
        !Verhindert dass letzter Eintrag doppelt ist
        if (last = 0) then
            !Datum formatieren
            if (columns$[3] = "Saat") then
				if (val(columns$[9]) = 1) then
					frucht$=columns$[7]
					if (IS_IN(frucht$, filterListe$) = 0) then
						filterListe$ += frucht$ + onRECBREAK$
					endif
				endif
			endif
        endif
    REPEAT

FN.RTN frucht$
FN.END

FN.DEF laskDb_aktionenLaden$(feld$)
!Aktionen aus Datenbank laden
    fn.import db, ernteJahr, onRECBREAK$, onCOLBREAK$

    undim columns$[]
    last = 0
    datum$ = ""
    !Alle Aktionen zum Feld in aktionen$ schreiben
    SQL.Raw_query dbQuery, db, "select * from '" + int$(ernteJahr) + "' where feld = '" + feld$ + "' order by datum;"
    WHILE last = 0
        SQL.Next last, dbQuery, columns$[]
        !Verhindert dass letzter Eintrag doppelt ist
        if (last = 0) then
            !Datum formatieren
            columns$[4] = MID$(columns$[4], 9, 2) + "." + MID$(columns$[4], 6, 2) + "." + MID$(columns$[4], 3, 2)
            if (columns$[3] = "Saat") then
				aktionen$ += onRECBREAK$ + columns$[1] + onCOLBREAK$ +  columns$[4] + onCOLBREAK$ + "<b>" + columns$[3]+"</b><span class='list-item__subtitle'> "+columns$[7]+"</span>"
			elseif (columns$[3] = "Bodenbearbeitung") then
				aktionen$ += onRECBREAK$ + columns$[1] + onCOLBREAK$ +  columns$[4] + onCOLBREAK$ + "<b>" + columns$[3]+"</b><span class='list-item__subtitle'> "+columns$[6]+"</span>"
			elseif (columns$[3] = "Organische Düngung" | columns$[3] = "Mineralische Düngung") then
				aktionen$ += onRECBREAK$ + columns$[1] + onCOLBREAK$ +  columns$[4] + onCOLBREAK$ + "<b>" + columns$[3]+"</b><span class='list-item__subtitle'> "+columns$[6]+"</span>"
			else
				aktionen$ += onRECBREAK$ + columns$[1] + onCOLBREAK$ +  columns$[4] + onCOLBREAK$ + "<b>" + columns$[3]+"</b>"
			endif
        endif
    REPEAT
    FN.RTN aktionen$
FN.END

FN.DEF laskDb_feldInfo(feld$)
!Array feldInfo$[] mit aktuellem Feld füllen
    fn.import db, ernteJahr, feldInfo$[], onRECBREAK$, onCOLBREAK$
    undim columns$[]
    last = 0

    SQL.Raw_query dbQuery, db, "select * from 'feldjahr' where id = '" + feld$ + "';"
    WHILE last = 0
        SQL.Next last, dbQuery, columns$[]
        !Verhindert dass letzter Eintrag doppelt ist
        if (last = 0) then
			ARRAY.COPY columns$[], feldInfo$[]
        endif
    REPEAT
FN.END

FN.DEF laskDb_aktionInfo(aktion$)
!Aktion laden
    fn.import db, ernteJahr, aktionInfo$[], onRECBREAK$, onCOLBREAK$, onFLDBREAK$
    last = 0
    UNDIM columns$[]

    SQL.Raw_query dbQuery, db, "select * from '" + int$(ernteJahr) + "' where id = '" + aktion$ + "';"
    WHILE last = 0
        SQL.Next last, dbQuery, columns$[]
        ARRAY.COPY columns$[], aktionInfo$[]
        !Zahlen müssen Punkt als Komma haben
        aktionInfo$[14] = REPLACE$(columns$[14], ",", ".")
        !Datum formatieren
        aktionInfo$[4] = MID$(aktionInfo$[4], 1, 4) + "-" + MID$(aktionInfo$[4], 6, 2) + "-" + MID$(aktionInfo$[4], 9, 2)
    REPEAT
FN.END

FN.DEF laskDb_aktionEntfernen(aktionId$)
!Aktion löschen
    fn.import db, ernteJahr, aktionInfo$[], feldInfo$[]
!    SQL.DELETE db, "'" + int$(ernteJahr) + "'", ~
!					"id = '" + aktionId$ + "'"
    SQL.UPDATE db, "'" + int$(ernteJahr) + "'", ~
                    "feld", "0": ~
                    "id = '" + aktionId$ + "'"
    SQL.INSERT db, "protokoll", ~
                    "jahr", int$(ernteJahr), ~
                    "feldid", feldInfo$[1], ~
                    "feldname", feldInfo$[3], ~
                    "aktionid", aktionInfo$[1], ~
                    "aktionname", aktionInfo$[3], ~
                    "was", "3", ~
                    "kommentar", "", ~
                    "anwender", ""
    popup "Aktion " + aktionId$ + " wurde gelöscht!"
FN.END

FN.DEF laskDb_aktionAendern(aktAktion$[])
!Aktion Ändern
    fn.import db, ernteJahr, aktionInfo$[], feldInfo$[]
    SQL.UPDATE db, "'" + int$(ernteJahr) + "'", ~
                    "datum", aktAktion$[4], ~
                    "kosten", aktAktion$[5], ~
                    "par1", aktAktion$[6], ~
                    "par2", aktAktion$[7], ~
                    "par3", aktAktion$[8], ~
                    "par4", aktAktion$[9], ~
                    "par5", aktAktion$[10], ~
                    "par6", aktAktion$[11], ~
                    "par7", aktAktion$[12], ~
                    "par8", aktAktion$[13], ~
                    "par9", aktAktion$[14], ~
                    "bbch", aktAktion$[15], ~
                    "schalter", aktAktion$[16], ~
                    "flaeche", aktAktion$[17], ~
                    "kommentar", aktAktion$[18], ~
                    "anwender", aktAktion$[19]: ~
                    "id = '" + aktAktion$[1] + "'"
    SQL.INSERT db, "protokoll", ~
                    "jahr", int$(ernteJahr), ~
                    "feldid", feldInfo$[1], ~
                    "feldname", feldInfo$[3], ~
                    "aktionid", aktAktion$[1], ~
                    "aktionname", aktAktion$[3], ~
                    "was", "2", ~
                    "kommentar", "", ~
                    "anwender", ""
    popup "Aktion " + aktAktion$[1] + " wurde geändert!"
FN.END

FN.DEF laskDb_aktionAnlegen(aktAktion$[])
!Aktion Anlegen
    fn.import db, ernteJahr, feldInfo$[]
    if (VAL(aktAktion$[2]) > -1) then
	    SQL.EXEC db, "INSERT INTO '" + int$(ernteJahr) + "' VALUES (" ~  
	                    + "(SELECT 1 + max(id) FROM '" + int$(ernteJahr) +"'),'" ~
	                    + aktAktion$[2] + "','" ~
	                    + aktAktion$[3] + "','" ~
	                    + aktAktion$[4] + "','" ~
	                    + aktAktion$[5] + "','" ~
	                    + aktAktion$[6] + "','" ~
	                    + aktAktion$[7] + "','" ~
	                    + aktAktion$[8] + "','" ~
	                    + aktAktion$[9] + "','" ~
	                    + aktAktion$[10] + "','" ~
	                    + aktAktion$[11] + "','" ~
	                    + aktAktion$[12] + "','" ~
	                    + aktAktion$[13] + "','" ~
	                    + aktAktion$[14] + "','" ~
	                    + aktAktion$[15] + "','" ~
	                    + aktAktion$[16] + "','" ~
	                    + aktAktion$[17] + "','" ~
	                    + aktAktion$[18] + "','" ~
	                    + aktAktion$[19] + "')"
	
	    SQL.EXEC db, "INSERT INTO 'protokoll' VALUES (" ~
	                    + "(SELECT 1 + max(id) FROM 'protokoll'),'" ~
	                    + int$(ernteJahr) + "','" ~
	                    + feldInfo$[1] + "','" ~
	                    + feldInfo$[3] + "'," ~
	                    + "(SELECT max(id) FROM '" + int$(ernteJahr) + "'),'" ~
	                    + aktAktion$[3] + "','" ~
	                    + "1" + "','" ~
	                    + " " + "','" ~
	                    + " " + "')"
	
	    popup _$("Aktion ") + aktAktion$[3] + _$(" wurde zu ") + feldInfo$[3] + _$(" hinzugefügt!")
	else
	    popup _$("Fehler! Ungültige Aktion")
	
	endif
FN.END
