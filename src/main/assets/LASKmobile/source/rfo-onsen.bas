LIST.CREATE S, tags
LIST.CREATE S, dialogtags
BUNDLE.CREATE onCtrlData
BUNDLE.CREATE onScreenDim

FN.DEF onInitGraphics(w)
	HTML.OPEN 0, 1
	HTML.LOAD.STRING "<html><head><script src='onsen/js/onsenui.min.js'></script></head><body onload=txt=window.innerWidth+':'+window.innerHeight+':'+ons.isReady();Android.dataLink(txt)>Loading ...</body></html>"
	data$ = ""
	DO
		HTML.GET.DATALINK data$
		PAUSE 100
	UNTIL data$ <> ""
	SPLIT sData$[], data$, ":"
	BUNDLE.PUT 2, "onWidth", val(sData$[2])
	BUNDLE.PUT 2, "onHeight", val(sData$[3])
	BUNDLE.PUT 2, "onZoom", w
FN.RTN w/val(sData$[2])*val(sData$[3])
FN.END

FN.DEF onNewForm(color$)
	BUNDLE.CLEAR 1
	LIST.CLEAR 1
	LIST.CLEAR 2
	LIST.ADD 1, "<!DOCTYPE html><html><head><script src='onsen/js/onsenui.min.js'></script><link rel='stylesheet' href='onsen/css/onsenui-core.min.css'><link rel='stylesheet' href='onsen/css/onsen-css-components.min.css'><style>.alert-dialog{top:25%;}.select-input{font-size:5vw;height:100%;}.button{font-size:5vw;}.display{font-size:5vw;}input{font-size:5vw;}</style></head><body onload={numbers='';}>"
	LIST.ADD 1, "<ons-page id='"+int$(s+1)+"'><div style='background:"+color$+"' class='background'></div>"
FN.RTN 0
FN.END

FN.DEF onAddDialog(pcaption$,opener,closer,pwidth,pheight)
	LIST.CLEAR 2
	LIST.SIZE 2, number
	BUNDLE.GET 2, "onWidth", width
	BUNDLE.GET 2, "onHeight", height
	BUNDLE.GET 2, "onZoom", zoom
	pwidth = pwidth/zoom*width
	pheight = pheight/zoom*width
	IF number > 0 THEN END "Only one dialog is allowed in rfo-onsen" 
	style$="<style>.dialog{min-width:unset;min-height:unset;width:"+int$(pwidth)+"px;height:"+int$(pheight)+"px;}</style>"
	js$="<script>document.getElementById('"+int$(opener)+"').onclick=function(){document.getElementById('"+int$(number)+"').show();};"
	js$+="document.getElementById('"+int$(closer)+"').onclick=function(){document.getElementById('"+int$(number)+"').hide();Android.dataLink('"+int$(closer)+": ');};</script>"
	LIST.ADD 2,	style$+js$
	LIST.ADD 2,	"<ons-dialog id='"+int$(number)+"' cancelable><div>"
	LIST.ADD 2,	"<div style='font-size:x-large;width:"+int$(pwidth)+"px;background:blue;color:white;' align='center'>"+pcaption$+"</div>"
FN.RTN number
FN.END

FN.DEF onAddControl(ptype,pcaption$,ptop,pleft,pwidth,pheight,pset,pstyle$)
	LIST.SIZE 1, number
	LIST.ADD 1,	onRenderControl$(number,ptype,pcaption$,ptop,pleft,pwidth,pheight,pset,pstyle$)
FN.RTN number
FN.END

FN.DEF onAddDialogControl(ptype,pcaption$,ptop,pleft,pwidth,pheight,pset,pstyle$)
	LIST.SIZE 2, number
	LIST.ADD 2,	onRenderControl$(1000+number,ptype,pcaption$,ptop,pleft,pwidth,pheight,pset,pstyle$)
FN.RTN 1000+number
FN.END

FN.DEF onModControl(number,ptype,pcaption$,ptop,pleft,pwidth,pheight,pset,pstyle$)
	LIST.REPLACE 1,	number+1, onRenderControl$(number,ptype,pcaption$,ptop,pleft,pwidth,pheight,pset,pstyle$)
FN.END

FN.DEF onModDialogControl(number,ptype,pcaption$,ptop,pleft,pwidth,pheight,pset,pstyle$)
	LIST.REPLACE 2,	number-999, onRenderControl$(number,ptype,pcaption$,ptop,pleft,pwidth,pheight,pset,pstyle$)
FN.END

FN.DEF onRenderControl$(number,ptype,pcaption$,ptop,pleft,pwidth,pheight,pset,pstyle$)
	BUNDLE.GET 2, "onWidth", width
	BUNDLE.GET 2, "onHeight", height
	BUNDLE.GET 2, "onZoom", zoom
	control$=""
	gesten$=""
	ptop = ptop/zoom*width
	pleft = pleft/zoom*width
	pwidth = pwidth/zoom*width
	pheight = pheight/zoom*width
	style$ = " style='direction:ltr;position:fixed;top:"+int$(ptop)+"px;left:"+int$(pleft)+"px;width:"+int$(pwidth)+"px;height:"+int$(pheight)+"px;line-height:"+int$(pheight)+"px;font-size:inherit;"
	IF (IS_IN("p", pstyle$)) THEN gesten$ += "swipeleft "
	IF (IS_IN("q", pstyle$)) THEN gesten$ += "swiperight "
	IF (IS_IN("r", pstyle$)) THEN gesten$ += "swipeup "
	IF (IS_IN("s", pstyle$)) THEN gesten$ += "swipedown "
	IF (IS_IN("t", pstyle$)) THEN gesten$ += "touch "
	IF (IS_IN("u", pstyle$)) THEN gesten$ += "hold "
	IF (IS_IN("v", pstyle$)) THEN gesten$ += "tap "
	IF (IS_IN("w", pstyle$)) THEN gesten$ += "doubletap "
	IF (IS_IN("x", pstyle$)) THEN gesten$ += "pinchin "
	IF (IS_IN("y", pstyle$)) THEN gesten$ += "pinchout "
	IF (IS_IN("z", pstyle$)) THEN gesten$ += "rotate "
	IF (!IS_IN("e", pstyle$)) THEN style$ += "background:white;border:1px solid;"
	IF (IS_IN("a", pstyle$)) THEN style$ += "text-align:left;"
	IF (IS_IN("b", pstyle$)) THEN style$ += "text-align:center;"
	IF (IS_IN("c", pstyle$)) THEN style$ += "text-align:right;"

	IF ptype=18 THEN %TOOLBAR
		BUNDLE.PUT 1, int$(number), pcaption$
		IF IS_IN("A", pstyle$) <> 0 then
			control$= "<ons-toolbar style='background:blue;' id='"+int$(number)+"'><div class='left' id="+int$(100+number)+" onclick={Android.dataLink('"+int$(number)+":Menu');}><ons-toolbar-button style='color:white;'><b>=</b></ons-toolbar-button></div><div class='center' style='direction:rtl;color:white;'>"+pcaption$+"</div></ons-toolbar>"
		ELSEIF IS_IN("B", pstyle$) <> 0 then
			control$= "<ons-toolbar style='background:blue;' id='"+int$(number)+"'><div class='left' id="+int$(100+number)+" onclick={Android.dataLink('"+int$(number)+":Back');}><ons-toolbar-button style='color:white;'><b><-</b></ons-toolbar-button></div><div class='center' style='direction:rtl;color:white;'>"+pcaption$+"</div></ons-toolbar>"
		ELSE
			control$= "<ons-toolbar style='background:blue;' id='"+int$(number)+"'><div class='center'style='direction:rtl;'>"+pcaption$+"</div></ons-toolbar>"	
		ENDIF
		IF gesten$ <> "" then
			control$ += "<script>ons.GestureDetector(document.getElementById('"+int$(number)+"')).on('"+gesten$+"',function(event){Android.dataLink('"+int$(0-number)+":"+pcaption$+"')});</script>"
		ENDIF
	ELSEIF ptype=9 THEN %LISTBOX
		BUNDLE.PUT 1, int$(number), "®"
		control$  = "<div" + style$ + "overflow-y:scroll'>"
		control$ += "<ons-list id='"+int$(number)+"'>"
		SPLIT zeilen$[], pcaption$, "®"
		ARRAY.LENGTH length, zeilen$[]
		FOR i = 1 TO length
			zeile$ = zeilen$[i]
			IF IS_IN("©", zeile$) 
				control$ += "<ons-list-item style='white-space: nowrap; border:1px solid' tappable id='"+WORD$(zeile$, 1, "©")+"'>"
				IF IS_IN("X", pstyle$) <> 0 then
					control$ += "<div class='left'><ons-checkbox id='"+WORD$(zeile$, 1, "©")+"' name='"+int$(number)+"'onclick={" +~
							"numbers='';"+ ~
							"cboxes=document.getElementsByName('"+int$(number)+"');"+ ~
						    "len=cboxes.length;"+ ~
						    "for(i=0;i<len;i++){"+ ~
						    "if(cboxes[i].checked){"+ ~
								"numbers+=cboxes[i].id+'©';"+ ~
						    "}}"+ ~
							"Android.dataLink('"+int$(number)+":®'+numbers.replace(/©©/g,'©'));}></ons-checkbox></div>"
					control$ += "<div class='center' onclick=Android.dataLink('"+int$(number)+":"+WORD$(zeile$, 1, "©")+"®'+numbers.replace(/©©/g,'©'))>"+WORD$(zeile$, 2, "©")+"</div>"
					control$ += "<div class='right' onclick=Android.dataLink('"+int$(number)+":"+WORD$(zeile$, 1, "©")+"®'+numbers.replace(/©©/g,'©'))>"+WORD$(zeile$, 3, "©")+"</div>"
				ELSE
					control$ += "<div class='left' onclick=Android.dataLink('"+int$(number)+":"+WORD$(zeile$, 1, "©")+"')>"+WORD$(zeile$, 2, "©")+"</div>"
					control$ += "<div class='center' style='overflow:hidden;padding-left:10px' onclick=Android.dataLink('"+int$(number)+":"+WORD$(zeile$, 1, "©")+"')>"+WORD$(zeile$, 3, "©")+"</div>"
					control$ += "<div class='right' onclick=Android.dataLink('"+int$(number)+":"+WORD$(zeile$, 1, "©")+"')>"+WORD$(zeile$, 4, "©")+"</div>"
					control$ += "</ons-list-item>"
				ENDIF
				control$ += "</ons-list-item>"
				IF gesten$ <> "" then
					control$ += "<script>ons.GestureDetector(document.getElementById('"+WORD$(zeile$, 1, "©")+"')).on('"+gesten$+"',function(event){Android.dataLink('"+int$(0-number)+":"+WORD$(zeile$, 1, "©")+"')});</script>"
				ENDIF
			ENDIF
		NEXT i
		control$ += "</ons-list></div>"
	ELSEIF ptype=15 THEN %COMBOBOX
		BUNDLE.PUT 1, int$(number), ""
		SPLIT zeilen$[], pcaption$, "®"
		IF IS_IN("d", pstyle$) <> 0 THEN %EDITABLE
			dstyle$ = style$ + "left:"+int$(pleft+pwidth-pheight)+"px;width:"+int$(pheight/2)+"px;padding-right:"+int$(pheight/2)+"px;"
			control$ = "<div class='display d-input' id='entry"+int$(number)+"' onclick={ons.notification.prompt('Text',{cancelable:true,inputType:'text',defaultValue:getElementById('entry"+int$(number)+"').innerHTML}).then(function(input){if(input){getElementById('entry"+int$(number)+"').innerHTML=input;Android.dataLink('"+int$(number)+":'+input);}})}"
			control$ += style$ + "text-align:left;overflow:hidden;word-break:break-word;width:"+int$(pwidth-pheight)+"px;'>"+zeilen$[1]+"</div>"
		ELSE
			control$ = ""
			dstyle$ = style$ + "width:"+int$(pwidth-pheight/2)+"px;padding-right:"+int$(pheight/2)+"px;"
		ENDIF
		IF pset = -1 THEN
			control$ += "<ons-select id='"+int$(number)+"' onchange={Android.dataLink('"+int$(number)+":'+this.options[this.selectedIndex].text);this.options[0].selected=true;}"
		ELSE
			control$ += "<ons-select id='"+int$(number)+"' onchange={Android.dataLink('"+int$(number)+":'+this.options[this.selectedIndex].text);getElementById('entry"+int$(number)+"').innerHTML=this.options[this.selectedIndex].text}"
		ENDIF
		control$ += dstyle$ + "'>"
		ARRAY.LENGTH length, zeilen$[]
		FOR i = 1 TO length
			zeile$ = zeilen$[i]
			IF pset = i THEN
				control$ += "\n   <option selected disabled>"+zeile$+"</option>"
				BUNDLE.PUT 1, int$(number), zeile$
			ELSE
				control$ += "\n   <option>"+zeile$+"</option>"
				BUNDLE.PUT 1, int$(number), zeilen$[1]
			ENDIF
		NEXT i
		control$ += "</ons-select>"
		IF gesten$ <> "" then
			control$ += "<script>ons.GestureDetector(document.getElementById('"+int$(number)+"')).on('"+gesten$+"',function(event){Android.dataLink('"+int$(0-number)+":'+document.getElementById('"+int$(number)+"').options[this.selectedIndex].text)});</script>"
		ENDIF
	ELSEIF ptype=6 THEN %BUTTON
		BUNDLE.PUT 1, int$(number), pcaption$
		control$= "<ons-button id='"+int$(number)+"' modifier='material' onclick={Android.dataLink('"+int$(number)+":"+pcaption$+"');}"
		control$ += style$ + "color:black;'>"+pcaption$+"</ons-button>"
		IF gesten$ <> "" then
			control$ += "<script>ons.GestureDetector(document.getElementById('"+int$(number)+"')).on('"+gesten$+"',function(event){Android.dataLink('"+int$(0-number)+":"+pcaption$+"')});</script>"
		ENDIF
	ELSEIF ptype=2 THEN %INPUT
		BUNDLE.PUT 1, int$(number), pcaption$
		IF IS_IN("A", pstyle$) <> 0 then
			control$="<div class='display d-input' id='"+int$(number)+"' onclick={ons.notification.prompt('Number',{cancelable:true,inputType:'number',defaultValue:getElementById('"+int$(number)+"').innerHTML}).then(function(input){if(input){getElementById('"+int$(number)+"').innerHTML=input;Android.dataLink('"+int$(number)+":'+input);}})}"
		ELSEIF IS_IN("B", pstyle$) <> 0 then
			control$="<div class='display d-input' id='"+int$(number)+"' onclick={ons.notification.prompt('Password',{cancelable:true,inputType:'password',defaultValue:getElementById('"+int$(number)+"').innerHTML}).then(function(input){if(input){getElementById('"+int$(number)+"').innerHTML=input;Android.dataLink('"+int$(number)+":'+input);}})}"
		ELSEIF IS_IN("C", pstyle$) <> 0 then
			control$="<div class='display d-input' id='"+int$(number)+"' onclick={ons.notification.prompt('Date',{cancelable:true,inputType:'date',defaultValue:getElementById('"+int$(number)+"').innerHTML}).then(function(input){if(input){getElementById('"+int$(number)+"').innerHTML=input;Android.dataLink('"+int$(number)+":'+input);}})}"
		ELSEIF IS_IN("D", pstyle$) <> 0 then
			control$="<div class='display d-input' id='"+int$(number)+"' onclick={ons.notification.prompt('Time',{cancelable:true,inputType:'time',defaultValue:getElementById('"+int$(number)+"').innerHTML}).then(function(input){if(input){getElementById('"+int$(number)+"').innerHTML=input;Android.dataLink('"+int$(number)+":'+input);}})}"
		ELSEIF IS_IN("E", pstyle$) <> 0 then
			control$="<div class='display d-input' id='"+int$(number)+"' onclick={ons.notification.prompt('Text',{cancelable:true,inputType:'text',defaultValue:getElementById('"+int$(number)+"').innerHTML}).then(function(input){if(input){getElementById('"+int$(number)+"').innerHTML=input;Android.dataLink('"+int$(number)+":'+input);}})}"
		ELSEIF IS_IN("F", pstyle$) <> 0 then
			control$="<ons-alert-dialog cancelable id='D"+int$(number)+"'><div class='alert-dialog-content'><textarea id='T"+int$(number)+"' rows='5' style='width:100%'>"+pcaption$+"</textarea></div><div class='alert-dialog-footer'><ons-alert-dialog-button onclick={getElementById('"+int$(number)+"').innerHTML=getElementById('T"+int$(number)+"').value;getElementById('D"+int$(number)+"').hide();Android.dataLink('"+int$(number)+":'+getElementById('"+int$(number)+"').innerHTML)}>OK</ons-alert-dialog-button></div></ons-alert-dialog>"
			control$+="<div class='display d-input' id='"+int$(number)+"' onclick={getElementById('D"+int$(number)+"').show()}"
		ELSE
			control$="<div class='display d-input' id='"+int$(number)+"' onclick={ons.notification.prompt('Text',{cancelable:true,inputType:'text',defaultValue:getElementById('"+int$(number)+"').innerHTML}).then(function(input){if(input){getElementById('"+int$(number)+"').innerHTML=input;Android.dataLink('"+int$(number)+":'+input);}})}"
		ENDIF
		control$ += style$ + " overflow:hidden;word-break:break-word;'>"+pcaption$+"</div>"
		IF gesten$ <> "" then
			control$ += "<script>ons.GestureDetector(document.getElementById('"+int$(number)+"')).on('"+gesten$+"',function(event){Android.dataLink('"+int$(0-number)+":'+getElementById('"+int$(number)+"').innerHTML)});</script>"
		ENDIF
	ELSEIF ptype=3 THEN %MULTILINE
		BUNDLE.PUT 1, int$(number), pcaption$
		control$="<textarea class='display d-area' id='"+int$(number)+"' onclick={Android.dataLink('"+int$(number)+":'+this.innerHTML);}"
		control$ += style$ + "'>"+pcaption$+"</textarea>"
		IF gesten$ <> "" then
			control$ += "<script>ons.GestureDetector(document.getElementById('"+int$(number)+"')).on('"+gesten$+"',function(event){Android.dataLink('"+int$(0-number)+":'+document.getElementById('"+int$(number)+"').value)});</script>"
		ENDIF
	ELSEIF ptype=1 THEN %DISPLAY
		BUNDLE.PUT 1, int$(number), pcaption$
		control$="<div class='display' id='"+int$(number)+"' onclick={Android.dataLink('"+int$(number)+":'+this.innerHTML);}"
		control$ += style$ + "'>"+pcaption$+"</div>"
		IF gesten$ <> "" then
			control$ += "<script>ons.GestureDetector(document.getElementById('"+int$(number)+"')).on('"+gesten$+"',function(event){Android.dataLink('"+int$(0-number)+":"+pcaption$+"')});</script>"
		ENDIF
	ELSEIF ptype=5 THEN %DATE
		BUNDLE.PUT 1, int$(number), pcaption$
		control$="<input type='date' id='"+int$(number)+"' onchange={Android.dataLink('"+int$(number)+":'+this.value);}"
		control$ += style$ + ";padding=0px' value='"+pcaption$+"'>"
		IF gesten$ <> "" then
			control$ += "<script>ons.GestureDetector(document.getElementById('"+int$(number)+"')).on('"+gesten$+"',function(event){Android.dataLink('"+int$(0-number)+":'+document.getElementById('"+int$(number)+"').value)});</script>"
		ENDIF
	ELSEIF ptype=16 THEN %TIME
		BUNDLE.PUT 1, int$(number), pcaption$
		control$="<input type='time' id='"+int$(number)+"' ononchange={Android.dataLink('"+int$(number)+":'+this.value);}"
		control$ += style$ + ";padding=0px' value='"+pcaption$+"'>"
		IF gesten$ <> "" then
			control$ += "<script>ons.GestureDetector(document.getElementById('"+int$(number)+"')).on('"+gesten$+"',function(event){Android.dataLink('"+int$(0-number)+":'+document.getElementById('"+int$(number)+"').value)});</script>"
		ENDIF
	ELSEIF ptype=10 THEN %CHECKBOX
		IF pset > 0 THEN
			BUNDLE.PUT 1, int$(number), "1"
			control$="<ons-checkbox id='"+int$(number)+"' checked onclick={Android.dataLink('"+int$(number)+":'+~~this.checked);}"
		ELSE
			BUNDLE.PUT 1, int$(number), "0"
			control$="<ons-checkbox id='"+int$(number)+"' onclick={Android.dataLink('"+int$(number)+":'+~~this.checked);}"
		ENDIF
		control$ += style$ + "'></ons-checkbox>"
		IF gesten$ <> "" then
			control$ += "<script>ons.GestureDetector(document.getElementById('"+int$(number)+"')).on('"+gesten$+"',function(event){Android.dataLink('"+int$(0-number)+":'+~~this.checked')});</script>"
		ENDIF
	ELSEIF ptype=7 THEN %PICTURE
		control$="<img id='"+int$(number)+"' src='"+pcaption$+"' onclick={Android.dataLink('"+int$(number)+":"+pcaption$+"');}"
		IF IS_IN("A", pstyle$) <> 0 then
			control$ += style$ + "transform:rotate("+int$(pset)+"deg);filter:grayscale(100%);'></div>"
		ELSE
			control$ += style$ + "transform:rotate("+int$(pset)+"deg);'></div>"
		ENDIF
		IF gesten$ <> "" then
			control$ += "<script>ons.GestureDetector(document.getElementById('"+int$(number)+"')).on('"+gesten$+"',function(event){Android.dataLink('"+int$(0-number)+":"+pcaption$+"')});</script>"
		ENDIF
	ENDIF
FN.RTN control$
FN.END

FN.DEF onDrawForm()
!	TEXT.OPEN w, file_number, "testfile.html"
	LIST.SIZE 1, s
	FOR i = 1 to s
		LIST.GET 1, i, tag$
		alltags$ += tag$
!		Text.writeln file_number, tag$
	NEXT i
!Dialogs
	LIST.SIZE 2, s
	IF (s>1) then
		LIST.GET 2, 1, tmpTag$
!		LIST.ADD 2, tag$
!		LIST.REMOVE 2, 1
		LIST.SIZE 2, s
		FOR i = 1 to s
			LIST.GET 2, i, tag$
			alltags$ += tag$
!			Text.writeln file_number, tag$+"\n"
		NEXT i
		alltags$ += "</div></ons-dialog>"
!		Text.writeln file_number, "</div></ons-dialog>"
	ENDIF
	alltags$ += "</ons-page></body></html>"
!	Text.writeln file_number, "</ons-page></body></html>"
!	TEXT.CLOSE file_number
	HTML.LOAD.STRING alltags$
!	HTML.LOAD.URL "testfile.html"
FN.END

FN.DEF onTouchCheck()
	data$ = ""
	DO
		HTML.GET.DATALINK data$
		PAUSE 100
	UNTIL data$ <> ""
	SPLIT.ALL sData$[], data$, ":"
	IF(sData$[1] = "BAK") then
		sData$[2] = "999"
	else
		BUNDLE.PUT 1, sData$[2], sData$[3]
!		popup sData$[2]+"->"+sData$[3]
	ENDIF
FN.RTN VAL(sData$[2])
FN.END

FN.DEF onGetCtrlData$(number)
	BUNDLE.GET 1, int$(number), data$
FN.RTN data$
FN.END
