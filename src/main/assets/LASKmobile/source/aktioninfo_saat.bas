goto weiter4
aktionInfo_saat:
!Kosten mit Standartwerten füllen wenn neue Aktion
    if (aktionInfo$[1] = "-1") then
		Kosten$[1] = "0,00"
		Kosten$[2] = einstellungen$[1]
		Kosten$[3] = "0,00"
	endif
gosub aktionInfoTmp

!Fruchtart
    fListe$ = ""
    for i2 = 1 to length
        if (mittel$[i2, 4] = "0") then
            if (Is_In(mittel$[i2, 6], fListe$) = 0) then
                fListe$ += mittel$[i2, 6] + onRECBREAK$
            endif
        endif
    next i2
    frucht = onAddControl(onCOMBOBOX, aktionInfo$[7]+onRECBREAK$+fListe$, 300,420,560,100,0, onDATBOLD$ + onEDITABLE$)
    frucht2 = onAddControl(onDISPLAY, _$("Fruchtart:"), 300,20,380,100,0, onALIGNRIGHT$)
!Sorte
    sListe$ = ""
    for i2 = 1 to length
        if (mittel$[i2, 4] = "0") then
            if (Is_In(mittel$[i2, 5], CtrlCap$) = 0) then
                if (mittel$[i2, 6] = aktionInfo$[7]) then
					sListe$ += mittel$[i2, 5] + onRECBREAK$
                endif
            endif
        endif
    next i2
    sorte = onAddControl(onCOMBOBOX, aktionInfo$[6]+onRECBREAK$+sListe$, 420,420,560,100,0, onDATBOLD$ + onEDITABLE$)
    sorte2 = onAddControl(onDISPLAY, _$("Sorte:"), 420,20,380,100,0, onALIGNRIGHT$)
!Saatmenge + Saateinheit
     if (val(aktionInfo$[8]) < 0) then
		menge = onAddControl(onINPUT, right$(aktionInfo$[8], -1), 540,420,300,100,0, onNUMBER$)
		einheit = onAddControl(onCOMBOBOX, "kg/ha"+onRECBREAK$+_$("Kö/m²"), 540,720,260,100,1, onALIGNRIGHT$)
		onModDialogControl(kosten12, onDISPLAY, "Saatgut(€/kg):",120,20,500,100,0, onALIGNRIGHT$)
     else
		menge = onAddControl(onINPUT, aktionInfo$[8], 540,420,300,100,0, onNUMBER$)
		einheit = onAddControl(onCOMBOBOX, "kg/ha"+onRECBREAK$+_$("Kö/m²"), 540,720,260,100,2, onALIGNRIGHT$)
		onModDialogControl(kosten12, onDISPLAY, "Saatgut(€/10000 Kö.):",120,20,500,100,0, onALIGNRIGHT$)
     endif
     menge2 = onAddControl(onDisplay, _$("Saatmenge:"), 540,20,380,100,0, onDATBOLD$+onAlignRight$)
!Hauptfrucht?
    hauptFrucht = onAddControl(onCHECKBOX, _$("Hauptfrucht:"), 685,420,100,100,val(aktionInfo$[9]),onNoBorder$)
    hauptFrucht2 = onAddControl(onDISPLAY, _$("Hauptfrucht:"), 660,20,380,100,0, onDATBOLD$+onAlignRight$)

    CALL onDrawForm()

!MainLoop
	twx = 0
    WHILE twx = 0
        selCtrl=onTouchCheck()
        SW.BEGIN selCtrl
        SW.CASE frucht
			onModControl(datum, onDATE, onGetCtrlData$(datum), 180,240,400,100,0, onDATBOLD$)
			onModControl(frucht, onCOMBOBOX, onGetCtrlData$(frucht)+onRECBREAK$+fListe$, 300,420,560,100,0, onDATBOLD$+onEDITABLE$)
			onModControl(menge, onINPUT, onGetCtrlData$(menge), 540,420,300,100,0, onNUMBER$)
			onModControl(hauptFrucht, onCHECKBOX, _$("Hauptfrucht:"), 685,420,100,100,val(onGetCtrlData$(hauptFrucht)),onNoBorder$)
			onModControl(kommentar, onINPUT, onGetCtrlData$(kommentar), hoehe-380,340,640,100,0, "")
			onModControl(anwender, onCOMBOBOX, onGetCtrlData$(anwender)+onRECBREAK$+aListe$, hoehe-260,340,640,100,0, onDATBOLD$+onEDITABLE$)
		    sListe$ = ""
		    for i2 = 1 to length
		        if (mittel$[i2, 4] = "0") then
		            if (Is_In(mittel$[i2, 5], sListe$) = 0) then
		                if (mittel$[i2, 6] = onGetCtrlData$(frucht)) then
							sListe$ += mittel$[i2, 5] + onRECBREAK$
		                endif
		            endif
		        endif
		    next i2
	        onModControl(sorte, onCOMBOBOX, ""+onRECBREAK$+sListe$, 420,420,560,100,0, onDATBOLD$ + onEDITABLE$)
			if (onGetCtrlData$(einheit) = "kg/ha") then
				onModControl(einheit, onCOMBOBOX, "kg/ha"+onRECBREAK$+_$("Kö/m²"), 540,720,260,100,1, onALIGNRIGHT$)
				onModDialogControl(kosten12, onDISPLAY, "Saatgut(€/kg):",120,20,500,100,0, onALIGNRIGHT$)
			else
				onModControl(einheit, onCOMBOBOX, "kg/ha"+onRECBREAK$+_$("Kö/m²"), 540,720,260,100,2, onALIGNRIGHT$)
				onModDialogControl(kosten12, onDISPLAY, "Saatgut(€/10000 Kö.):",120,20,500,100,0, onALIGNRIGHT$)
			endif
			if (aktionInfo$[2] = "-1") then
				onModControl(flaeche, onDISPLAY, onGetCtrlData$(flaeche), hoehe-500,790,190,100,0, onNUMBER$)
			else
				onModControl(flaeche, onINPUT, onGetCtrlData$(flaeche), hoehe-500,790,190,100,0, onNUMBER$)
			endif
			onDrawForm()
            W_R.continue
        SW.CASE einheit
			onModControl(datum, onDATE, onGetCtrlData$(datum), 180,240,400,100,0, onDATBOLD$)
			onModControl(frucht, onCOMBOBOX, onGetCtrlData$(frucht)+onRECBREAK$+fListe$, 300,420,560,100,0, onDATBOLD$+onEDITABLE$)
	        onModControl(sorte, onCOMBOBOX, onGetCtrlData$(sorte)+onRECBREAK$+sListe$, 420,420,560,100,0, onDATBOLD$+onEDITABLE$)
			onModControl(menge, onINPUT, onGetCtrlData$(menge), 540,420,300,100,0, onNUMBER$)
			onModControl(hauptFrucht, onCHECKBOX, _$("Hauptfrucht:"), 685,420,100,100,val(onGetCtrlData$(hauptFrucht)),onNoBorder$)
			onModControl(kommentar, onINPUT, onGetCtrlData$(kommentar), hoehe-380,340,640,100,0, "")
			onModControl(anwender, onCOMBOBOX, onGetCtrlData$(anwender)+onRECBREAK$+aListe$, hoehe-260,340,640,100,0, onDATBOLD$+onEDITABLE$)
			if (onGetCtrlData$(einheit) = "kg/ha") then
				onModControl(einheit, onCOMBOBOX, "kg/ha"+onRECBREAK$+_$("Kö/m²"), 540,720,260,100,1, onALIGNRIGHT$)
				onModDialogControl(kosten12, onDISPLAY, "Saatgut(€/kg):",120,20,500,100,0, onALIGNRIGHT$)
			else
				onModControl(einheit, onCOMBOBOX, "kg/ha"+onRECBREAK$+_$("Kö/m²"), 540,720,260,100,2, onALIGNRIGHT$)
				onModDialogControl(kosten12, onDISPLAY, "Saatgut(€/10000 Kö.):",120,20,500,100,0, onALIGNRIGHT$)
			endif
			if (aktionInfo$[2] = "-1") then
				onModControl(flaeche, onDISPLAY, onGetCtrlData$(flaeche), hoehe-500,790,190,100,0, onNUMBER$)
			else
				onModControl(flaeche, onINPUT, onGetCtrlData$(flaeche), hoehe-500,790,190,100,0, onNUMBER$)
			endif
			onDrawForm()
            W_R.continue
        SW.CASE speichernKnopf
            Array.Copy aktionInfo$[], aktAktion$[]
            aktAktion$[4] = onGetCtrlData$(datum)
            aktAktion$[5] = kosten$[1]+";"+kosten$[2]+";"+kosten$[3]
            aktAktion$[6] = onGetCtrlData$(sorte)
            aktAktion$[7] = onGetCtrlData$(frucht)
            if (onGetCtrlData$(einheit) = "kg/ha") then
                aktAktion$[8] = "-"+onGetCtrlData$(menge)
            else
                aktAktion$[8] = onGetCtrlData$(menge)
            endif
                aktAktion$[9] = onGetCtrlData$(hauptFrucht)
            aktAktion$[17] = onGetCtrlData$(flaeche)
            aktAktion$[18] = onGetCtrlData$(kommentar)
            aktAktion$[19] = onGetCtrlData$(anwender)
            if (aktAktion$[1] = "-1") then
				if (flists > 1) then
					FOR i = 1 to flists
						LIST.GET flist, i, aktAktion$[2] 
						LIST.GET flistha, i, aktAktion$[17] 
						laskDb_feldInfo(aktAktion$[2])
						laskDb_aktionAnlegen(aktAktion$[])
					NEXT
					LIST.clear flist
					LIST.clear flistha
					goto felderListe
				else				
					aktAktion$[17] = onGetCtrlData$(flaeche)
					laskDb_aktionAnlegen(aktAktion$[])
					goto aktionenListe
				endif
			else
				aktAktion$[17] = onGetCtrlData$(flaeche)
				laskDb_aktionAendern(aktAktion$[])
				goto aktionenListe
			endif
        SW.CASE kostenOk
			k1$ = onGetCtrlData$(kosten1)
			k2$ = onGetCtrlData$(kosten2)
			k3$ = onGetCtrlData$(kosten3)
			if (is_number(k1$) = 0) then kosten$[1] = "0,00" else kosten$[1] = using$("", "%.2f", val(k1$))
			if (is_number(k2$) = 0) then kosten$[2] = "0,00" else kosten$[2] = using$("", "%.2f", val(k2$))
			if (is_number(k3$) = 0) then kosten$[3] = "0,00" else kosten$[3] = using$("", "%.2f", val(k3$))
        SW.BREAK
        SW.CASE zurKnopf, 999
			if (flists > 1) then
				goto felderListe
			else
				goto aktionenListe
			endif
        SW.END
    REPEAT


weiter4:
