goto weiter8
aktionInfo_bodenbearbeitung:
!Kosten mit Standartwerten füllen wenn neue Aktion
    if (aktionInfo$[1] = "-1") then
		Kosten$[1] = "0,00"
		Kosten$[2] = "0,00"
		Kosten$[3] = "0,00"
	endif
gosub aktionInfoTmp

!Gerät
	gListe$ = ""
    for i2 = 1 to length
        if (mittel$[i2, 4] = "4") then
            gListe$ += mittel$[i2, 5] + onRECBREAK$
        endif
    next i2
    geraet = onAddControl(onCOMBOBOX, aktionInfo$[6]+onRECBREAK$+gListe$,300,340,640,100,0, onDATBOLD$ + onEDITABLE$)
    geraet2 = onAddControl(onDISPLAY, _$("Gerät:"),300,20,300,100,0, onDATBOLD$ + onEDITABLE$)
!Kosten
	onModDialogControl(kosten1,onDISPLAY, "",120,540,240,100,0, onNOBORDER$)
	onModDialogControl(kosten12,onDISPLAY, "",120,20,500,100,0, onNOBORDER$)
	
    CALL onDrawForm()

!MainLoop
    twx = 0
    WHILE twx = 0
        selCtrl=onTouchCheck()
        SW.BEGIN selCtrl
        SW.CASE geraet
			onModControl(geraet, onCOMBOBOX, onGetCtrlData$(geraet)+onRECBREAK$+gListe$,300,340,640,100,0, onDATBOLD$+onEDITABLE$)
			onModControl(datum, onDATE, onGetCtrlData$(datum),180,240,400,100,0, "")
			onModControl(kommentar, onINPUT, onGetCtrlData$(kommentar), hoehe-380,340,640,100,0, "")
			onModControl(anwender, onCOMBOBOX, onGetCtrlData$(anwender)+onRECBREAK$+aListe$, hoehe-260,340,640,100,0, onDATBOLD$+onEDITABLE$)
			if (aktionInfo$[2] = "-1") then
				onModControl(flaeche, onDISPLAY, onGetCtrlData$(flaeche), hoehe-500,790,190,100,0, onNUMBER$)
			else
				onModControl(flaeche, onINPUT, onGetCtrlData$(flaeche), hoehe-500,790,190,100,0, onNUMBER$)
			endif
            for i2 = 1 to length
                if (mittel$[i2, 4] = "4") then
					if (mittel$[i2, 5] = onGetCtrlData$(geraet)) then
						Kosten$[2] = LEFT$(mittel$[i2, 3], -2) + "." + RIGHT$(mittel$[i2, 3], 2)
						onModDialogControl(kosten2,onINPUT, kosten$[2],240,540,240,100,0, onNumber$)
					endif
                endif
            next i2
			onDrawForm()
        SW.BREAK
        SW.CASE speichernKnopf
            Array.Copy aktionInfo$[], aktAktion$[]
            aktAktion$[4] = onGetCtrlData$(datum)
            aktAktion$[5] = kosten$[1]+";"+kosten$[2]+";"+kosten$[3]
            aktAktion$[6] = onGetCtrlData$(geraet)
            aktAktion$[18] = onGetCtrlData$(kommentar)
            aktAktion$[19] = onGetCtrlData$(anwender)
            if (aktAktion$[1] = "-1") then
				if (flists > 1) then
					FOR i = 1 to flists
						LIST.GET flist, i, aktAktion$[2] 
						LIST.GET flistha, i, aktAktion$[17]
						laskDb_feldInfo(aktAktion$[2])
						laskDb_aktionAnlegen(aktAktion$[])
					NEXT
					LIST.clear flist
					LIST.clear flistha
					goto felderListe
				else
					aktAktion$[17] = onGetCtrlData$(flaeche)
					laskDb_aktionAnlegen(aktAktion$[])
					goto aktionenListe
				endif
			else
				aktAktion$[17] = onGetCtrlData$(flaeche)
				laskDb_aktionAendern(aktAktion$[])
				goto aktionenListe
			endif
        SW.BREAK
        SW.CASE kostenOk
			k2$ = onGetCtrlData$(kosten2)
			k3$ = onGetCtrlData$(kosten3)
			if (is_number(k2$) = 0) then kosten$[2] = "0,00" else kosten$[2] = using$("", "%.2f", val(k2$))
			if (is_number(k3$) = 0) then kosten$[3] = "0,00" else kosten$[3] = using$("", "%.2f", val(k3$))
        SW.BREAK
        SW.CASE zurKnopf, 999
			if (flists > 1) then
				goto felderListe
			else
				goto aktionenListe
			endif
       SW.END
    REPEAT

weiter8:
