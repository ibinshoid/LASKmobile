goto weiter5
aktionInfo_ernte:
!Kosten mit Standartwerten füllen wenn neue Aktion
    if (aktionInfo$[1] = "-1") then
		Kosten$[1] = "0,00"
		Kosten$[2] = einstellungen$[5]
		Kosten$[3] = "0,00"
	endif
gosub aktionInfoTmp

!Ertrag
    ertrag = onAddControl(onINPUT, aktionInfo$[8],300,340,220,100,0, onNUMBER$)
    ertrag2 = onAddControl(onDISPLAY, _$("Ertrag dt/ha:"),300,20,300,100,0, onALIGNRIGHT$)
!    rc = SetCtrlData(ertrag, aktionInfo$[8])
!Feuchte
    feuchte = onAddControl(onINPUT, aktionInfo$[10],420,340,220,100,0, onNUMBER$)
    feuchte2 = onAddControl(onDISPLAY, _$("Feuchte %:"),420,20,300,100,0, onALIGNRIGHT$)
!    rc = SetCtrlData(feuchte, aktionInfo$[10])
!Erlös
    erloes = onAddControl(onINPUT, aktionInfo$[9],540,340,220,100,0, onNUMBER$)
    erloes2 = onAddControl(onDISPLAY, _$("Erlös €/dt:"),540,20,300,100,0, onALIGNRIGHT$)
!Kosten
	onModDialogControl(kosten1,onDISPLAY, "",120,540,240,100,0, onNOBORDER$)
	onModDialogControl(kosten12,onDISPLAY, "",120,20,500,100,0, onNOBORDER$)

    onDrawForm()

!MainLoop
    twx = 0
    WHILE twx = 0
        selCtrl=onTouchCheck()
        SW.BEGIN selCtrl
        SW.CASE speichernKnopf
            Array.Copy aktionInfo$[], aktAktion$[]
            aktAktion$[4] = onGetCtrlData$(datum)
            aktAktion$[5] = kosten$[1]+";"+kosten$[2]+";"+kosten$[3]
            aktAktion$[8] = onGetCtrlData$(ertrag)
            aktAktion$[9] = onGetCtrlData$(erloes)
            aktAktion$[10] = onGetCtrlData$(feuchte)
            aktAktion$[17] = onGetCtrlData$(flaeche)
            aktAktion$[18] = onGetCtrlData$(kommentar)
            aktAktion$[19] = onGetCtrlData$(anwender)
            if (aktAktion$[1] = "-1") then
				if (flists > 0) then
					FOR i = 1 to flists
						LIST.GET flist, i, aktAktion$[2] 
						LIST.GET flistha, i, aktAktion$[17] 
						laskDb_feldInfo(aktAktion$[2])
						laskDb_aktionAnlegen(aktAktion$[])
					NEXT
					LIST.clear flist
					LIST.clear flistha
					goto felderListe
				else				
					aktAktion$[17] = onGetCtrlData$(flaeche)
					laskDb_aktionAnlegen(aktAktion$[])
					goto aktionenListe
				endif
			else
				laskDb_aktionAendern(aktAktion$[])
				laskDb_aktionInfo(aktionInfo$[1])
				goto aktionInfo_ernte
			endif
        SW.CASE kostenOk
			k1$ = onGetCtrlData$(kosten1)
			k2$ = onGetCtrlData$(kosten2)
			k3$ = onGetCtrlData$(kosten3)
			if (is_number(k1$) = 0) then kosten$[1] = "0,00" else kosten$[1] = using$("", "%.2f", val(k1$))
			if (is_number(k2$) = 0) then kosten$[2] = "0,00" else kosten$[2] = using$("", "%.2f", val(k2$))
			if (is_number(k3$) = 0) then kosten$[3] = "0,00" else kosten$[3] = using$("", "%.2f", val(k3$))
        SW.BREAK
        SW.CASE zurKnopf, 999
			if (flists > 1) then
				goto felderListe
			else
				goto aktionenListe
			endif
        SW.END
    REPEAT


weiter5:
