goto weiter6

aktionInfoTmp:
!Form öffnen
    CALL onNewForm("aqua")
!Überschrift
    if (flists > 0) then
		zurKnopf = onAddControl(onTOOLBAR, betriebName$ + " -> " + int$(ernteJahr) + " -> " +INT$(flists) + " Felder -> " + aktionInfo$[3] + "   ", 0,0,1000,1000,0, onBACKBTN$)
	else
		zurKnopf = onAddControl(onTOOLBAR, betriebName$ + " -> " + int$(ernteJahr) + " -> " +feldInfo$[3] + " -> " + aktionInfo$[3] + "   ",0,0,1000,1000,0, onBACKBTN$)
	endif
!Zurück Knopf oben links
!    zurKnopf = AddControl(onBUTTON, "GC-Back.png",onBLACK,onLGRAY,0,0, ~
!            1,1,0,118,118,80, onGRAPHIC$)
!Datum
    datum = onAddControl(onDATE, aktionInfo$[4],180,240,400,100,0, onDATBOLD$)
    datum2 = onAddControl(onDISPLAY, _$("Datum:"),180,20,200,100,0, onALIGNRIGHT$)
!    rc = SetCtrlData(datum, aktionInfo$[4])
!    rc = SetCtrlData(datum2, MID$(onGetCtrlData$(datum), 9, 2)+"."+MID$(onGetCtrlData$(datum), 6, 2)+"."+MID$(onGetCtrlData$(datum), 1, 4))
!Kosten
    kosten = onAddControl(onBUTTON, _$("Kosten(€/ha):"),hoehe - 500,20,450,100,0, onDATBOLD$)
    if (aktionInfo$[1] <> "-1") then
		SPLIT kosten$[], REPLACE$(aktionInfo$[5], ",", "."), ";"
	endif
	onAddDialog("Kosten", kosten, 1009,800,600)
	kosten1 = onAddDialogControl(onINPUT, kosten$[1],120,540,240,100,0, onNumber$)
	kosten12 = onAddDialogControl(onDISPLAY, "Mittel(€/?):",120,20,500,100,0, onALIGNRIGHT$)
	kosten2 = onAddDialogControl(onINPUT, kosten$[2],240,540,240,100,0, onNumber$)
	kosten22 = onAddDialogControl(onDISPLAY, "Arbeit(€/ha):",240,20,500,100,0, onALIGNRIGHT$)
	kosten3 = onAddDialogControl(onINPUT, kosten$[3],360,540,240,100,0, onNumber$)
	kosten32 = onAddDialogControl(onDISPLAY, "Festk.(€):",360,20,500,100,0, onALIGNRIGHT$)
	kostenOk = onAddDialogControl(onButton, "Ok",480,200,400,100,0, "")
!Teilfläche
	if (aktionInfo$[2] = "-1") then
		flaeche = onAddControl(onDISPLAY, aktionInfo$[17], hoehe-500,790,190,100,0, onNUMBER$)
	else
		flaeche = onAddControl(onINPUT, aktionInfo$[17], hoehe-500,790,190,100,0, onNUMBER$)
	endif
    flaeche2 = onAddControl(onDISPLAY, _$("Fläche(ha):"), hoehe-500,490,300,100,0, onALIGNRIGHT$)
!    rc = SetCtrlData(flaeche, aktionInfo$[17])
!Kommentar
    kommentar = onAddControl(onINPUT, aktionInfo$[18], hoehe-380,360,620,100,0, onTEXTAREA$)
    kommentar2 = onAddControl(onDISPLAY, _$("Kommentar:"), hoehe-380,20,320,100,0, onALIGNRIGHT$)
!Anwender
    aListe$ = ""
    for i2 = 1 to length
        if (mittel$[i2, 4] = "5") then
            aListe$ += mittel$[i2, 5] + onRECBREAK$
        endif
    next i2
	anwender = onAddControl(onCOMBOBOX, aktionInfo$[19] + onRECBREAK$ + aListe$, hoehe-260,360,620,100,0, onDATBOLD$+onEDITABLE$)
    anwender2 = onAddControl(onDISPLAY, _$("Anwender:"), hoehe-260,20,320,100,0, onALIGNRIGHT$)
!Aktion speichern unten
    speichernKnopf = onAddControl(onBUTTON, _$("Speichern"), hoehe-140,480,500,100,0, onALIGNCENTER$)

return
weiter6:


