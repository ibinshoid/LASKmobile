goto weiter2

aktionenListe:
!Form öffnen
    CALL onNewForm("aqua")
!Überschrift
    ueberschrift = onAddControl(onTOOLBAR, betriebName$ + " -> " + int$(ernteJahr) + " -> " +feldInfo$[3] + "   ", 0,0,1000,1000,0, onBACKBTN$)
!Aktionenliste mitte
    aktionenListe = onAddControl(onLISTBOX, laskDb_aktionenLaden$(feld$[1]), 180,10,980,hoehe-300,0, onSWIPELEFT$)
!Aktion hinzufügen unten rechts
    aktionHinzu = onAddControl(onCOMBOBOX,_$("Hinzufügen")+onRECBREAK$+ ~
								            _$("Saat")+onRECBREAK$+ ~
								            _$("Bodenbearbeitung")+onRECBREAK$+ ~
								            _$("Pflanzenschutz")+onRECBREAK$+ ~
								            _$("Organische Düngung")+onRECBREAK$+ ~
								            _$("Mineralische Düngung")+onRECBREAK$+ ~
								            _$("Ernte")+onRECBREAK$, ~
								            hoehe-110,505,485,100,-1, onMENULIST$)
!Aktion löschen unten links
    entfKnopf = onAddControl(onBUTTON, _$("Entfernen"), hoehe-110,10,485,90,0, onALIGNCENTER$)
    CALL onDrawForm()
!    ModCtrlCap(aktionenListe, laskDb_aktionenLaden$(feld$[1]), 1)
!MainLoop
    twx = 0
    vorher$ = ""
    vorher = -1
    WHILE twx = 0
        selCtrl=onTouchCheck()
        SW.BEGIN selCtrl
        SW.CASE aktionenListe
            SPLIT aktion$[], onGetCtrlData$(aktionenListe), onRECBREAK$
            laskDb_aktionInfo(aktion$[1])
            if (aktion$[1] = vorher$) then
                if(vorher = selCtrl) then
                    if (aktionInfo$[3] = "Saat") then
                        goto aktionInfo_saat
                    elseif (aktionInfo$[3] = "Ernte") then
                        goto aktionInfo_ernte
                    elseif (aktionInfo$[3] = "Organische Düngung") then
                        goto aktionInfo_duengung
                    elseif (aktionInfo$[3] = "Mineralische Düngung") then
                        goto aktionInfo_duengung
                    elseif (aktionInfo$[3] = "Bodenbearbeitung") then
                        goto aktionInfo_bodenbearbeitung
                    elseif (aktionInfo$[3] = "Pflanzenschutz") then
                        goto aktionInfo_psm
                    endif
                endif
            endif
            vorher$ = aktion$[1]
        SW.BREAK
        SW.CASE 0-aktionenListe
            SPLIT aktion$[], onGetCtrlData$(0-aktionenListe), onRECBREAK$
            laskDb_aktionInfo(aktion$[1])
			if (aktionInfo$[3] = "Saat") then
				goto aktionInfo_saat
			elseif (aktionInfo$[3] = "Ernte") then
				goto aktionInfo_ernte
			elseif (aktionInfo$[3] = "Organische Düngung") then
				goto aktionInfo_duengung
			elseif (aktionInfo$[3] = "Mineralische Düngung") then
				goto aktionInfo_duengung
			elseif (aktionInfo$[3] = "Bodenbearbeitung") then
				goto aktionInfo_bodenbearbeitung
			elseif (aktionInfo$[3] = "Pflanzenschutz") then
				goto aktionInfo_psm
			endif
        SW.BREAK
        SW.CASE aktionHinzu
			if (onGetCtrlData$(aktionenListe) <> "")
	            aktion$ = onGetCtrlData$(aktionHinzu)
	 			gosub aktionBauen
				aktionInfo$[2] = feld$[1]
	            aktionInfo$[17] = feldInfo$[5]
	            if (aktion$ = "Saat") then
					aktionInfo$[3] = "Saat"
					aktionInfo$[9] = "1"
					goto aktionInfo_saat
	            elseif (aktion$ = "Bodenbearbeitung") then
					aktionInfo$[3] = "Bodenbearbeitung"
					goto aktionInfo_bodenbearbeitung
	            elseif (aktion$ = "Pflanzenschutz") then
					aktionInfo$[3] = "Pflanzenschutz"
					goto aktionInfo_psm
	            elseif (aktion$ = "Organische Düngung") then
					aktionInfo$[3] = "Organische Düngung"
					goto aktionInfo_duengung
	            elseif (aktion$ = "Mineralische Düngung") then
					aktionInfo$[3] = "Mineralische Düngung"
					goto aktionInfo_duengung
	            elseif (aktion$ = "Ernte") then
					aktionInfo$[3] = "Ernte"
					goto aktionInfo_ernte
	            endif
			endif
        SW.CASE -5
        SW.BREAK
        SW.CASE ueberschrift
            goto felderListe
        SW.CASE entfKnopf
			if (onGetCtrlData$(aktionenListe) <> "")
	            Dialog.Message _$("Frage"), _$("Aktion '") + aktionInfo$[3] +_$("' wirklich löschen?\n Alle Daten gehen verloren"), button, _$("Abbrechen"), _$("OK")
	            if (button = 2) then
	                laskDb_aktionEntfernen(aktionInfo$[1])
	                button = -1
	                goto aktionenListe
	            endif
	        endif
            SW.BREAK
        SW.CASE ueberschrift, 999
            goto felderListe
        SW.END
        vorher = selCtrl
   REPEAT


weiter2:
