goto weiter9
aktionInfo_psm:
!Kosten mit Standartwerten füllen wenn neue Aktion
    if (aktionInfo$[1] = "-1") then
		Kosten$[1] = "0,00"
		Kosten$[2] = einstellungen$[2]
		Kosten$[3] = "0,00"
	endif
gosub aktionInfoTmp

!Mittel Liste laden
    pListe$ = ""
    for i2 = 1 to length
        if (mittel$[i2, 4] = "1") then
            pListe$ += mittel$[i2, 5] + onRECBREAK$ 
        endif
    next i2
!Aktion laden
    DIM PSM$[5]
    SPLIT PSM2$[], REPLACE$(REPLACE$(aktionInfo$[7], "|", "~"), ",", "."), ";"
    Array.copy PSM2$[], PSM$[]
!Mittel
    mittel1 = onAddControl(onCOMBOBOX, WORD$(PSM$[1],1, "~")+onRECBREAK$+pListe$, 300,20,550,100,0, onDATBOLD$ + onEDITABLE$)
    mittel2 = onAddControl(onCOMBOBOX, WORD$(PSM$[2],1, "~")+onRECBREAK$+pListe$, 420,20,550,100,0, onDATBOLD$ + onEDITABLE$)
    mittel3 = onAddControl(onCOMBOBOX, WORD$(PSM$[3],1, "~")+onRECBREAK$+pListe$, 540,20,550,100,0, onDATBOLD$ + onEDITABLE$)
    mittel4 = onAddControl(onCOMBOBOX, WORD$(PSM$[4],1, "~")+onRECBREAK$+pListe$, 660,20,550,100,0, onDATBOLD$ + onEDITABLE$)
    mittel5 = onAddControl(onCOMBOBOX, WORD$(PSM$[5],1, "~")+onRECBREAK$+pListe$, 780,20,550,100,0, onDATBOLD$ + onEDITABLE$)
!Menge
    menge1 = onAddControl(onINPUT, WORD$(PSM$[1],3, "~"), 300,600,200,100,0, onNUMBER$)
    menge12 = onAddControl(onDISPLAY, _$("Liter"), 300,800,170,100,0, onNUMBER$)
    menge2 = onAddControl(onINPUT, WORD$(PSM$[2],3, "~"), 420,600,370,100,0, onNUMBER$)
    menge22 = onAddControl(onDISPLAY, _$("Liter"), 420,800,170,100,0, onNUMBER$)
    menge3 = onAddControl(onINPUT, WORD$(PSM$[3],3, "~"), 540,600,370,100,0, onNUMBER$)
    menge32 = onAddControl(onDISPLAY, _$("Liter"), 540,800,170,100,0, onNUMBER$)
    menge4 = onAddControl(onINPUT, WORD$(PSM$[4],3, "~"), 660,600,370,100,0, onNUMBER$)
    menge42 = onAddControl(onDISPLAY, _$("Liter"), 660,800,170,100,0, onNUMBER$)
    menge5 = onAddControl(onINPUT, WORD$(PSM$[5],3, "~"), 780,600,370,100,0, onNUMBER$)
    menge52 = onAddControl(onDISPLAY, _$("Liter"), 780,800,170,100,0, onNUMBER$)

    CALL onDrawForm()


!MainLoop
    twx = 0
    WHILE twx = 0
        selCtrl=onTouchCheck()
        SW.BEGIN selCtrl
        SW.CASE speichernKnopf
			psm$ = ""
            if (onGetCtrlData$(mittel1) <> "") then
				psm$ = psm$ + onGetCtrlData$(mittel1) + "||" + onGetCtrlData$(menge1) + ";"
            endif 
            if (onGetCtrlData$(mittel2) <> "") then
				psm$ = psm$ + onGetCtrlData$(mittel2) + "||" + onGetCtrlData$(menge2) + ";"
            endif 
            if (onGetCtrlData$(mittel3) <> "") then
				psm$ = psm$ + onGetCtrlData$(mittel3) + "||" + onGetCtrlData$(menge3) + ";"
            endif 
            if (onGetCtrlData$(mittel4) <> "") then
				psm$ = psm$ + onGetCtrlData$(mittel4) + "||" + onGetCtrlData$(menge4) + ";"
            endif 
            if (onGetCtrlData$(mittel5) <> "") then
				psm$ = psm$ + onGetCtrlData$(mittel5) + "||" + onGetCtrlData$(menge5) + ";"
            endif
            psm$ = left$(psm$, -1) 
            Array.Copy aktionInfo$[], aktAktion$[]
            aktAktion$[4] = onGetCtrlData$(datum)
            aktAktion$[5] = kosten$[1]+";"+kosten$[2]+";"+kosten$[3]
            aktAktion$[7] = psm$
            aktAktion$[17] = onGetCtrlData$(flaeche)
            aktAktion$[18] = onGetCtrlData$(kommentar)
            aktAktion$[19] = onGetCtrlData$(anwender)
            if (aktAktion$[1] = "-1") then
				if (flists > 0) then
					FOR i = 1 to flists
						LIST.GET flist, i, aktAktion$[2] 
						LIST.GET flistha, i, aktAktion$[17] 
						laskDb_feldInfo(aktAktion$[2])
						laskDb_aktionAnlegen(aktAktion$[])
					NEXT
					LIST.clear flist
					LIST.clear flistha
					goto felderListe
				else				
					aktAktion$[17] = onGetCtrlData$(flaeche)
					laskDb_aktionAnlegen(aktAktion$[])
					goto aktionenListe
				endif
			else
				aktAktion$[17] = onGetCtrlData$(flaeche)
				laskDb_aktionAendern(aktAktion$[])
				goto aktionenListe
			endif
        SW.CASE kostenOk
			k1$ = onGetCtrlData$(kosten1)
			k2$ = onGetCtrlData$(kosten2)
			k3$ = onGetCtrlData$(kosten3)
			if (is_number(k1$) = 0) then kosten$[1] = "0,00" else kosten$[1] = using$("", "%.2f", val(k1$))
			if (is_number(k2$) = 0) then kosten$[2] = "0,00" else kosten$[2] = using$("", "%.2f", val(k2$))
			if (is_number(k3$) = 0) then kosten$[3] = "0,00" else kosten$[3] = using$("", "%.2f", val(k3$))
        SW.BREAK
        SW.CASE zurKnopf, 999
			if (flists > 1) then
				goto felderListe
			else
				goto aktionenListe
			endif
        SW.END
    REPEAT


weiter9:
