goto weiter7
aktionInfo_duengung:
!Kosten mit Standartwerten füllen wenn neue Aktion
    if (aktionInfo$[1] = "-1") then
		Kosten$[1] = "0,00"
		if (aktionInfo$[3] = "Organische Düngung") then
			Kosten$[2] = einstellungen$[3]
		else 
			Kosten$[2] = einstellungen$[4]
		endif
		Kosten$[3] = "0,00"
	endif
gosub aktionInfoTmp

!Dünger
    dListe$ = ""
    for i2 = 1 to length
        if (aktionInfo$[3] = "Organische Düngung") then
			aktionInfo$[7] = "m³"
            if (mittel$[i2, 4] = "2") then
                dListe$ = dListe$ + mittel$[i2, 5] + onRECBREAK$
            endif
        elseif (aktionInfo$[3] = "Mineralische Düngung") then
			aktionInfo$[7] = "kg"
            if (mittel$[i2, 4] = "3") then
                dListe$ = dListe$ + mittel$[i2, 5] + onRECBREAK$
            endif
        endif
    next i2
    CtrlCap$ = aktionInfo$[6] + onRECBREAK$ + dListe$
    duenger = onAddControl(onCombobox, CtrlCap$, 300,420,560,100,0, onEDITABLE$)
    duenger2 = onAddControl(onDISPLAY, _$("Dünger:"), 300,20,380,100,0, onALIGNRIGHT$)
!Menge
    menge = onAddControl(onINPUT, aktionInfo$[14], 420,420,560,100,0, onNUMBER$)
    menge2 = onAddControl(onDisplay, _$("Menge(") + aktionInfo$[7] + "/ha):",420,20,380,100,0, onALIGNRIGHT$)
!DungN
    dungN = onAddControl(onINPUT, aktionInfo$[8], 540,270,220,100,0, onNUMBER$)
    dungN2 = onAddControl(onDisplay, "N %:", 540,20,230,100,0, onALIGNRIGHT$)
!DungM
    dungM = onAddControl(onINPUT, aktionInfo$[11], 540,760,220,100,0, onNUMBER$)
    dungM2 = onAddControl(onDisplay, "M %:", 540,510,230,100,0, onALIGNRIGHT$)
!DungP
    dungP = onAddControl(onINPUT, aktionInfo$[9], 660,270,220,100,0, onNUMBER$)
    dungP2 = onAddControl(onDisplay, "P %:", 660,20,230,100,0, onALIGNRIGHT$)
!DungS
    dungS = onAddControl(onINPUT, aktionInfo$[12], 660,760,220,100,0, onNUMBER$)
    dungS2 = onAddControl(onDisplay, "S %:", 660,510,230,100,0, onALIGNRIGHT$)
!DungK
    dungK = onAddControl(onINPUT, aktionInfo$[10], 780,270,220,100,0, onNUMBER$)
    dungK2 = onAddControl(onDisplay, "K %:", 780,20,230,100,0, onALIGNRIGHT$)
!DungC
	dungC = onAddControl(onINPUT, aktionInfo$[13], 780,760,220,100,0, onNUMBER$)
    if (aktionInfo$[3] = "Organische Düngung") then
		dungC2 = onAddControl(onDisplay, "NH4 %:", 780,510,230,100,0, onALIGNRIGHT$)
	else
		dungC2 = onAddControl(onDisplay, "C %:", 780,510,230,100,0, onALIGNRIGHT$)
	endif
!Kosten
	onModDialogControl(kosten12, onDISPLAY, "Dünger(€/"+aktionInfo$[7]+"):",120,20,500,100,0, onALIGNRIGHT$)

    CALL onDrawForm()


!MainLoop
    twx = 0
    WHILE twx = 0
        selCtrl=onTouchCheck()
        SW.BEGIN selCtrl
        SW.CASE speichernKnopf
            Array.Copy aktionInfo$[], aktAktion$[]
            aktAktion$[4] = onGetCtrlData$(datum)
            aktAktion$[5] = kosten$[1]+";"+kosten$[2]+";"+kosten$[3]
            aktAktion$[6] = onGetCtrlData$(duenger)
            aktAktion$[8] = onGetCtrlData$(dungN)
            aktAktion$[9] = onGetCtrlData$(dungP)
            aktAktion$[10] = onGetCtrlData$(dungK)
            aktAktion$[11] = onGetCtrlData$(dungM)
            aktAktion$[12] = onGetCtrlData$(dungS)
            aktAktion$[13] = onGetCtrlData$(dungC)
            aktAktion$[14] = onGetCtrlData$(menge)
            aktAktion$[17] = onGetCtrlData$(flaeche)
            aktAktion$[18] = onGetCtrlData$(kommentar)
            aktAktion$[19] = onGetCtrlData$(anwender)
            if (aktAktion$[1] = "-1") then
				if (flists > 1) then
					FOR i = 1 to flists
						LIST.GET flist, i, aktAktion$[2] 
						LIST.GET flistha, i, aktAktion$[17]
						laskDb_feldInfo(aktAktion$[2])
						laskDb_aktionAnlegen(aktAktion$[])
					NEXT
					LIST.clear flist
					LIST.clear flistha
					goto felderListe
				else
					aktAktion$[17] = onGetCtrlData$(flaeche)
					laskDb_aktionAnlegen(aktAktion$[])
					goto aktionenListe
				endif
			else
				aktAktion$[17] = onGetCtrlData$(flaeche)
				laskDb_aktionAendern(aktAktion$[])
				goto aktionenListe
			endif
        SW.BREAK
        SW.CASE duenger
			onModControl(datum, onDATE, onGetCtrlData$(datum), 180,240,400,100,0, onDATBOLD$)
            onModControl(duenger, onCombobox, onGetCtrlData$(duenger)+onRECBREAK$+dListe$, 300,420,560,100,0, onDATBOLD$)
            onModControl(menge, onINPUT, onGetCtrlData$(menge), 420,420,560,100,0, onNUMBER$)
			onModControl(kommentar, onINPUT, onGetCtrlData$(kommentar), hoehe-380,340,640,100,0, "")
			onModControl(anwender, onCOMBOBOX, onGetCtrlData$(anwender)+onRECBREAK$+aListe$, hoehe-260,340,640,100,0, onDATBOLD$+onEDITABLE$)
			if (aktionInfo$[2] = "-1") then
				onModControl(flaeche, onDISPLAY, onGetCtrlData$(flaeche), hoehe-500,790,190,100,0, onNUMBER$)
			else
				onModControl(flaeche, onINPUT, onGetCtrlData$(flaeche), hoehe-500,790,190,100,0, onNUMBER$)
			endif
            for i2 = 1 to length
                if (mittel$[i2, 5] = onGetCtrlData$(duenger)) then
                    aktionInfo$[7] = mittel$[i2, 6]
                    onModControl(menge2, onDisplay, _$("Menge(") + aktionInfo$[7] + "/ha):",420,20,380,100,0, onALIGNRIGHT$)
                    onModControl(dungN, onINPUT, mittel$[i2,7], 540,270,220,100,0, onNUMBER$)
				    onModControl(dungM, onINPUT, mittel$[i2,10], 540,760,220,100,0, onNUMBER$)
				    onModControl(dungP, onINPUT, mittel$[i2,8], 660,270,220,100,0, onNUMBER$)
				    onModControl(dungS, onINPUT, mittel$[i2,11], 660,760,220,100,0, onNUMBER$)
				    onModControl(dungK, onINPUT, mittel$[i2,9], 780,270,220,100,0, onNUMBER$)
					onModControl(dungC, onINPUT, mittel$[i2,12], 780,760,220,100,0, onNUMBER$)
					!Kosten für Dünger eintragen
					Kosten$[1] = str$(round(val(mittel$[i2,3]) / 100, 2))
					onModDialogControl(kosten1, onINPUT, kosten$[1],120,540,240,100,0, onNumber$)
                endif
            next i2
			onDrawForm()
        SW.BREAK
        SW.CASE kostenOk
			k1$ = onGetCtrlData$(kosten1)
			k2$ = onGetCtrlData$(kosten2)
			k3$ = onGetCtrlData$(kosten3)
			if (is_number(k1$) = 0) then kosten$[1] = "0,00" else kosten$[1] = using$("", "%.2f", val(k1$))
			if (is_number(k2$) = 0) then kosten$[2] = "0,00" else kosten$[2] = using$("", "%.2f", val(k2$))
			if (is_number(k3$) = 0) then kosten$[3] = "0,00" else kosten$[3] = using$("", "%.2f", val(k3$))
        SW.BREAK
        SW.CASE zurKnopf, 999
			if (flists > 1) then
				goto felderListe
			else
				goto aktionenListe
			endif
       SW.END
    REPEAT


weiter7:
