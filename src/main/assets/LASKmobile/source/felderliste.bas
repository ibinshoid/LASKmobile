goto weiter

felderListe:
!Datenbank vorher einlesen, dann gehts nachher flüssiger
felderListe$ = laskDb_felderLaden$(was$)
!Form öffnen
    CALL onNewForm("aqua")
!Überschrift
    ueberschrift = onAddControl(onTOOLBAR, betriebName$ + " -> " + int$(ernteJahr), 0,0,0,100,0, onMENUBTN$+onALIGNCENTER$)
!FelderFilter
	if (was$ = "Alle") then
		felderFilter = onAddControl(onCOMBOBOX, filterListe$, 180,10,980,100,1, onALIGNRIGHT$)
	else
		felderFilter = onAddControl(onCOMBOBOX, was$+onRECBREAK$+filterListe$, 180,10,980,100,1, onALIGNRIGHT$)
	endif
!Aktion hinzufügen unten
    aktionHinzu = onAddControl(onCOMBOBOX,_$("Hinzufügen")+onRECBREAK$+ ~
								            _$("Saat")+onRECBREAK$+ ~
								            _$("Bodenbearbeitung")+onRECBREAK$+ ~
								            _$("Pflanzenschutz")+onRECBREAK$+ ~
								            _$("Organische Düngung")+onRECBREAK$+ ~
								            _$("Mineralische Düngung")+onRECBREAK$+ ~
								            _$("Ernte")+onRECBREAK$, ~
											hoehe-110,10,980,100,-1, onALIGNRIGHT$)
!FelderListe
   felderListe = onAddControl(onLISTBOX, felderliste$, 290,10,980,hoehe-410,0,onCHECKBOX$+onSWIPELEFT$)
!JahrWahl Dialog
	onAddDialog("Erntejahr", 100+ueberschrift, 1003,460,400)
	jListe$ = ""
    for i = 1 to length2
		jListe$ += int$(jahre[i]) + onRECBREAK$
		if (jahre[i]= ernteJahr) then
			aktJahr  = i
		endif
	next i
	weg = onAddDialogControl(onButton, "Ok",250,20,400,100,0, onDATBOLD$)
	jahrWahl = onAddDialogControl(onCOMBOBOX, jListe$,120,20,400,100,aktJahr, "")
    onDrawForm()
!MainLoop
    twx = 0
    vorher$ = ""
    vorher = -1

    WHILE twx = 0
        selCtrl=onTouchCheck()
        SW.BEGIN selCtrl
        SW.CASE felderFilter
			if (onGetCtrlData$(felderFilter) <> "- - -") then
				was$ = onGetCtrlData$(felderFilter)
			endif
			onModControl(felderFilter, onCOMBOBOX, was$+onRECBREAK$+filterListe$, 180,10,980,100,1, onALIGNRIGHT$)
			onModControl(felderListe, onLISTBOX, laskDb_felderLaden$(was$), 290,10,980,hoehe-410,0,onCHECKBOX$+onSWIPELEFT$)
			onDrawForm()
        SW.BREAK
        SW.CASE felderListe
            SPLIT feld$[], onGetCtrlData$(felderListe), onRECBREAK$
            if (feld$[1] <> "") then
				laskDb_feldInfo(feld$[1])
	            if (feld$[1] = vorher$) then
	                if(vorher = selCtrl) then
	                    goto aktionenListe
	                endif
	            endif
            endif
            vorher$ = feld$[1]
        SW.BREAK
        SW.CASE 0-felderListe
            SPLIT feld$[], onGetCtrlData$(0-felderListe), onRECBREAK$
            if (feld$[1] <> "") then
				laskDb_feldInfo(feld$[1])
                goto aktionenListe
            endif
        SW.BREAK
        SW.CASE aktionHinzu
			aktion$ = onGetCtrlData$(aktionHinzu)
			SPLIT.ALL feld$[], onGetCtrlData$(felderListe), onRECBREAK$
            if (feld$[2] <> "") then
				!Selektierte Felder in flist speichern
				SPLIT felder$[], feld$[2], onCOLBREAK$
				ARRAY.length felderl, felder$[]
				LIST.clear flist
				LIST.clear flistha
				flaeche = 0
				FOR i = 1 to felderl
					laskDb_feldInfo(felder$[i])
					LIST.add flist, felder$[i]
					LIST.add flistha, feldInfo$[5]
					flaeche += val(feldInfo$[5])
				NEXT
				gosub aktionBauen
				LIST.SIZE flist, flists
				if (felderl = 1) then
					feld$[1] = felder$[1]
					aktionInfo$[2] = felder$[1]
					aktionInfo$[17] = feldInfo$[5]
				else
					aktionInfo$[2] = "-1"
					aktionInfo$[17] = str$(flaeche)
				endif
	            if (aktion$ = "Saat") then
					aktionInfo$[3] = "Saat"
					aktionInfo$[9] = "1"
					goto aktionInfo_saat
	            elseif (aktion$ = "Bodenbearbeitung") then
					aktionInfo$[3] = "Bodenbearbeitung"
					goto aktionInfo_bodenbearbeitung
	            elseif (aktion$ = "Pflanzenschutz") then
					aktionInfo$[3] = "Pflanzenschutz"
					goto aktionInfo_psm
	            elseif (aktion$ = "Organische Düngung") then
					aktionInfo$[3] = "Organische Düngung"
					goto aktionInfo_duengung
	            elseif (aktion$ = "Mineralische Düngung") then
					aktionInfo$[3] = "Mineralische Düngung"
					goto aktionInfo_duengung
	            elseif (aktion$ = "Ernte") then
					aktionInfo$[3] = "Ernte"
					goto aktionInfo_ernte
	            endif
            endif
        SW.BREAK
        SW.CASE menuKnopf
            menu$ = onGetCtrlData$(menuKnopf)
            if (menu$ = _$("Beenden")) then
                EXIT
            else if (menu$ = _$("Erntejahr")) then
				ShowCtrl(jahrWahl, 1)	
            else if (menu$ = _$("Info")) then
            Dialog.message , "LASKmobile " + version$ + "\n" + ~
							 "Autor: Andreas Strasser\n" + ~
							 "www.codeberg.org/ibinshoid/LASKmobile", go, "OK"
            endif
        SW.BREAK
        SW.CASE weg
		ernteJahr=val(onGetCtrlData$(jahrWahl))
		goto felderListe
		SW.BREAK
		SW.END
		vorher = selCtrl
    REPEAT
return
weiter:
