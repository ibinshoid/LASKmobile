INCLUDE rfo-onsen.bas
INCLUDE rfo-onsen-const.bas
FILE.exists fi, "../databases/lask.db"
if (fi = 0) then
	byte.open r, fid, "../data/lask.tmp"
	byte.copy fid, "../databases/lask.db"
	byte.close fid
endif
FILE.exists fi, "../data/onsen/js"
if (fi = 0) then
	FILE.mkdir "onsen"
	FILE.mkdir "onsen/js"
	FILE.mkdir "onsen/css"
	byte.open r, fid, "./onsen/js/onsenui.min.js"
	byte.copy fid, "./onsen/js/onsenui.min.js"
	byte.close fid
	byte.open r, fid, "./onsen/css/onsenui-core.min.css"
	byte.copy fid, "./onsen/css/onsenui-core.min.css"
	byte.close fid
	byte.open r, fid, "./onsen/css/onsen-css-components.min.css"
	byte.copy fid, "./onsen/css/onsen-css-components.min.css"
	byte.close fid
endif
	version$ = "0.7.9"
	woBinIch = 0
    ernteJahr = 0
	picPath$ = "../data/"
	filterListe$ = ""
	was$ = "Alle"
DIM aktionInfo$[20]
DIM feldInfo$[20]
DIM aktAktion$[20]
DIM Kosten$[3]
DIM tmpKosten$[3]
DIM einstellungen$[6]
BUNDLE.CREATE i18n
LIST.create S, flist
LIST.create S, flistha
wohin = 0
filter2$ = "Alle"

INCLUDE gettext.bas
!INCLUDE GraphicControls.bas
INCLUDE laskdb.bas
INCLUDE felderliste.bas
INCLUDE aktionenliste.bas
INCLUDE aktioninfo.bas
INCLUDE aktioninfo_saat.bas
INCLUDE aktioninfo_ernte.bas
INCLUDE aktioninfo_duengung.bas
INCLUDE aktioninfo_bodenbearbeitung.bas
INCLUDE aktioninfo_psm.bas

goto weiter10

!Tasten abfangen
OnBackKey:
!	wohin = -1
Back.resume   

aktionBauen:
	dim aktionInfo$[20]
	dim dateTime$[3]
	
	time dateTime$[1], dateTime$[2], dateTime$[3],,,,,
	aktionInfo$[1] = "-1"
	aktionInfo$[2] = ""
	aktionInfo$[3] = ""
	join dateTime$[], aktionInfo$[4], "-" 
	aktionInfo$[5] = "0,00;0,00;0,00"
	aktionInfo$[8] = "0.0"
	aktionInfo$[9] = "0.0"
	aktionInfo$[10] = "0.0"
	aktionInfo$[11] = "0.0"
	aktionInfo$[12] = "0.0"
	aktionInfo$[13] = "0.0"
	aktionInfo$[14] = "0.0"
	aktionInfo$[15] = "0.0"
	aktionInfo$[16] = "0.0"
	aktionInfo$[17] = "0.0"
return
weiter10:
!Grafikmodus starten
    hoehe = onInitGraphics(1000)
!felderListe starten
    GOTO felderListe
!GR.CLOSE
Sql.Close db
END ""

