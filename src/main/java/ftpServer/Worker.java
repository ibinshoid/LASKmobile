package ftpServer;

import java.util.ArrayList;					// -humpty: use an array of files for LIST instead of strings
import java.text.SimpleDateFormat;
import java.util.Date;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Class for a FTP server worker thread.
 *
 * @author Moritz Stueckler (SID 20414726)
 *
 */
/* hFTPS	Modified by -humpty
 * 		v1.0 Nov 2020 for android hBasic + enhancements
 *      v1.1 Sep 2021 added RNFR, RNTO to rename a path
 *		v1.2 Nov 2021 allow changeable user,password and welcome string
 *		v1.3 Improved compatibilty.
 */

public class Worker extends Thread
{
	/**
	 *  Enable debugging output to console
	 */
	private boolean debugMode = false;

	/**
	 * Indicating the last set transfer Type
	 */
	private enum transferType {
		ASCII, BINARY
	}

	/**
	 * Indicates the authentification status of a user
	 */
	private enum userStatus {
		NOTLOGGEDIN, ENTEREDUSERNAME, LOGGEDIN
	}

	private enum listType {
		DETAILED,			// for LIST
		NO_DETAIL,			// for NLST
	}

	// Path information
	private String root;							// will take server's root later
	private String currDirectory;

	// Intermediate store
	private File renameFrom = null;

	// control connection
	private Socket controlSocket;
	private PrintWriter controlOutWriter;
	private BufferedReader controlIn;

	// data Connection
	private ServerSocket dataSocket;
	private Socket dataConnection;
	private PrintWriter dataOutWriter;

	private int dataPort;
	private transferType transferMode = transferType.ASCII;

	// user properly logged in?
	private userStatus currentUserStatus = userStatus.NOTLOGGEDIN;
	public static String validUser = "hbasic";			// (changeable)
	public static String validPassword = "hbasic";		// (changeable)

	// welcome string identifies app
	public static String welcomeStr="Welcome to the hBasic's FTP-Server"; // (changeable)

	private boolean quitCommandLoop = false;

	/**
	 * Create new worker with given client socket
	 * @param client the socket for the current client
	 * @param dataPort the port for the data connection
	 */
	public Worker(Socket client, int dataPort)
	{
		super();
		this.controlSocket = client;
		this.dataPort = dataPort;
		this.root = Server.root;					// take the same root as Server
		if (root.endsWith("/")) root=root.substring(0,root.length()-1);	// root never ends with a slash
													// user never gets to see our root, only sees it as "/"
		this.currDirectory = "/";					// always starts with a slash
													// always ends with a slash
													// always relative to our root
	}
	/**
	 * Run method required by Java thread model
	 */
	public void run()
	{

		try
		{
			// Input from client
			InputStreamReader in = new InputStreamReader(controlSocket.getInputStream());
			controlIn = new BufferedReader(in);

			// Output to client, automatically flushed after each print
			controlOutWriter = new PrintWriter(controlSocket.getOutputStream(), true);

			// Greeting
			sendMsgToClient("220 "+welcomeStr);

			// Get new command from client
			String cmd = "";
			while (!quitCommandLoop)	// don't let readline block indefinetly, we need to poll if server stopped
			{
//~				if (controlIn.ready())				// (unreliable - returns true and still blocks)
				if (in.ready())				// if something to read
				{
					cmd = controlIn.readLine();		// get the cmd
					if (cmd!=null) executeCommand(cmd);
				}
				else								// (never gets here if we use controlIn.ready)
				{
					Thread.sleep(100);				// give the cpu some slack
					if (!Server.serverRunning) quitCommandLoop=true;	// check if server stopped
				}
			}//_while

		}//_try
		catch (Exception e)
		{
//~			e.printStackTrace();
			debugOutput("Worker Exception.");
		}
		finally
		{
			// Clean up
			try
			{
//~				controlIn.close();
//~				controlOutWriter.close();
				if (!controlSocket.isClosed()) controlSocket.close();	// should close underlying streams too
				debugOutput("cleanup> Sockets closed and worker stopped");
			}
			catch(IOException e)
			{
//~				e.printStackTrace();
				debugOutput("cleanup> Could not close sockets");
			}
		}

	}//_run
	/**
	 * Main command dispatcher method.
	 * Separates the command from the arguments and dispatches it to single handler functions.
	 * @param c the raw input from the socket consisting of command and arguments
	 */
	private void executeCommand(String c)
	{
		// split command and arguments
		int index = c.indexOf(' ');
		String command = ((index == -1)? c.toUpperCase() : (c.substring(0, index)).toUpperCase());
		String args = ((index == -1)? null : c.substring(index+1, c.length()));

		if (args==null)
		{
//~			debugOutput("args empty");
			args = "";
		}
		debugOutput("Command: " + command + " Args: " + args);

		// dispatcher mechanism for different commands
		switch(command) {
			case "USER":
				handleUser(args);
				break;

			case "PASS":
				handlePass(args);
				break;

			case "PASV":
				handlePasv();
				break;

			case "TYPE":
				handleType(args);
				break;

			case "CDUP":
				args = "..";		// just a convenience for CWD ..

			case "CWD":
				handleCwd(args);
				break;

			case "LIST":
				handleList(args);
				break;

			case "NLST":
				handleNlst(args);
				break;

			case "PWD":
				handlePwd();
				break;

			case "RETR":
				handleRetr(args);
				break;

			case "STOR":
				handleStor(args);
				break;

			case "DELE":
				handleDele(args);
				break;

			case "RNFR":
						handleRnFr(args);	// -humpty rename a path from
						break;
			case "RNTO":
						handleRnTo(args);	// -humpty rename a path to
						break;
			case "QUIT":
				handleQuit();
				break;

			case "EPSV":
				handleEpsv();
				break;

			case "SYST":
				handleSyst();
				break;

			case "FEAT":
				handleFeat();
				break;

			case "PORT":
				handlePort(args);
				break;

			case "EPRT":
				handlePort(parseExtendedArguments(args));
				break;

			case "MKD":
				handleMkd(args);
				break;

			case "RMD":
				handleRmd(args);
				break;

			default:
				sendMsgToClient("501 Unknown command");			// or syntax error
//~				sendMsgToClient("502 Command not implemented.");
				break;
		}
	}
	/**
	 * Sends a message to the connected client over the control connection.
	 * Flushing is automatically performed by the stream.
	 * @param msg The message that will be sent
	 */
	private void sendMsgToClient(String msg)
	{
		controlOutWriter.println(msg + '\r');
		debugOutput ("sent> "+ msg);
	}
	/**
	 * Send a message to the connected client over the data connection.
	 * @param msg Message to be sent
	 */
	private void sendDataMsgToClient(String msg)
	{
		if (dataConnection == null || dataConnection.isClosed())
		{
			sendMsgToClient("425 No data connection was established");
		}
		else
		{
			dataOutWriter.print(msg + '\r' + '\n');
			debugOutput ("data> "+ msg);
		}
	}
	/**
	 * Open a new data connection socket and wait for new incoming connection from client.
	 * Used for passive mode.
	 * @param port Port on which to listen for new incoming connection
	 */
	private boolean openDataConnectionPassive(int port, String connectMsg)
	{
		int timeout = 3000;	// miliseconds - this should be enough for the client to connect
		try
		{
			dataSocket = new ServerSocket(port);
			dataSocket.setSoTimeout(timeout);			// don't wait forever. fail if timeout.

//			debugOutput (connectMsg);					// tell client we are connecting
			sendMsgToClient(connectMsg);				// send IP and data port to client.
														// It seems we must accept immediately
														// otherwise we might miss the incoming connection
			dataConnection = dataSocket.accept();		// blocks until timeout

			dataOutWriter = new PrintWriter(dataConnection.getOutputStream(), true);
			debugOutput("Data connection Passive Mode established on port "+port);
		}
		catch (IOException e)							// timeout will be caught here
		{
//~			debugOutput("Could not create data connection.");
//~			e.printStackTrace();
			return false;								// failed
		}
		return true;									// success
	}
	/**
	 * Connect to client socket for data connection.
	 * Used for active mode.
	 * @param ipAddress Client IP address to connect to
	 * @param port Client port to connect to
	 */
	private void openDataConnectionActive(String ipAddress, int port)
	{
		try
		{
			dataConnection = new Socket(ipAddress, port);
			dataOutWriter = new PrintWriter(dataConnection.getOutputStream(), true);
			debugOutput("Data connection - Active Mode - established");
		} catch (IOException e)
		{
			debugOutput("Could not connect to client data socket");
//~			e.printStackTrace();
		}
	}
	/**
	 * Close previously established data connection sockets and streams
	 */
	private void closeDataConnection()
	{
		try
		{
			dataOutWriter.close();
			dataConnection.close();
			if (dataSocket != null)
			{
				dataSocket.close();
			}
			debugOutput("Data connection was closed");
		}
		catch (IOException e)
		{
			debugOutput("Could not close data connection");
//~			e.printStackTrace();
		}
		dataOutWriter = null;
		dataConnection = null;
		dataSocket = null;
	}
	/**
	 * Handler for USER command.
	 * User identifies the client.
	 * @param username Username entered by the user
	 */
	private void handleUser(String username)
	{
		if (username.toLowerCase().equals(validUser))
		{
			sendMsgToClient("331 User name ok, need password");
			currentUserStatus = userStatus.ENTEREDUSERNAME;
		}
		else if (currentUserStatus == userStatus.LOGGEDIN)
		{
			sendMsgToClient("530 User already logged in");
		}
		else
		{
			sendMsgToClient("530 Not logged in");
		}
	}
	/**
	 * Handler for PASS command.
	 * PASS receives the user password and checks if it's valid.
	 * @param password Password entered by the user
	 */
	private void handlePass(String password)
	{
		// User has entered a valid username and password is correct
		if (currentUserStatus == userStatus.ENTEREDUSERNAME && password.equals(validPassword))
		{
			currentUserStatus = userStatus.LOGGEDIN;
			sendMsgToClient("230 User logged in successfully");
		}
		// User is already logged in
		else if (currentUserStatus == userStatus.LOGGEDIN)
		{
			sendMsgToClient("530 User already logged in");
		}
		// Wrong password
		else
		{
			sendMsgToClient("530 Not logged in");
		}
	}
	/**
	 * Handler for CWD (change working directory) command.
	 * @param args New directory to be set to current
	 */
	// -humpty: newpath is treated relative to the current directory, unless..
	// 			any path that starts with slash is treated relative to root
	//			root never changes and is used to limit the path

	private void handleCwd(String args)
	{
		String newpath = currDirectory;				// start from the current directory

													// cut off the last slash
		if (newpath.endsWith("/")) newpath=newpath.substring(0,newpath.length()-1);
		
		if (args.equals(".."))						// go one level up (cd ..) from current dir
		{
			int ind = newpath.lastIndexOf("/");
			if (ind >= 0)
			{
				newpath = newpath.substring(0, ind+1);	// cut off last dir but keep trailing slash
			}
		}
		// if argument is anything else (cd . or empty path does nothing)
		else if ((args != null) && (!args.equals(".")) && (args.length()>0) )
		{

			if (args.charAt(0) == '/')				// if path does starts with slash
				newpath = args;						// then treat as relative to our root
			else									// else it is relative to current dir
				newpath = newpath + "/" + args;		// 		so just append path to it
		}

													// our newpath always starts and ends with a slash
		if (!newpath.endsWith("/")) newpath+="/";


		File f = new File(root+newpath);			// get a File using complete path

		if (f.exists() && f.isDirectory())			// check if path exists and is a directory
		{
			currDirectory = newpath;				// update the current dir
			sendMsgToClient("250 The current directory has been changed to " + currDirectory);
		}
		else
		{
			sendMsgToClient("550 Requested action not taken. Dir error > "+f.toString());
			if (!f.exists()) debugOutput("Dir error > "+f.toString());
			if (!f.isDirectory()) debugOutput("Not Dir");
//~			debugOutput("root    > "+root);
//~			debugOutput("newpath > "+newpath);
		}
	}//_handleCwd
	/**
	 * Handler for LIST (detailed List) command.
	 * Lists the directory content in linux 'ls' format
	 * @param args The directory or filename to be listed
	 */
	private void handleList(String args)				// empty arg implies current directory
	{
		if (dataConnection == null || dataConnection.isClosed())
		{
			sendMsgToClient("425 No data connection was established.");
			debugOutput("LIST> No data connection was established.");
		}
		else
		{
			String[] dirContent = listHelper(args, listType.DETAILED);

			if (dirContent == null)
			{
				sendMsgToClient("550 File or Dir does not exist > "+args);
				debugOutput("LIST> File or Dir does not exist");
			}
			else								// send the list to the client
			{
				sendMsgToClient("125 Opening ASCII mode data connection for file list.");
				for (int i = 0; i < dirContent.length; i++)
				{
					sendDataMsgToClient(dirContent[i]);
				}
				sendMsgToClient("226 Transfer complete.");
				closeDataConnection();
			}
		}
	}//_handleList
	/**
	 * Handler for NLST (Named List) command.
	 * Lists the directory content in a short format (names only)
	 * @param args The directory to be listed
	 */
	private void handleNlst(String args)				// emtpy arg implies current directory
	{													// arg MUST be a directory or empty
		if (dataConnection == null || dataConnection.isClosed())
		{
			sendMsgToClient("425 No data connection was established");
		}
		else
		{
			String[] dirContent = listHelper(args, listType.NO_DETAIL);

			if (dirContent == null)
			{
				sendMsgToClient("550 Dir does not exist > "+args);
				debugOutput("NLST> Dir does not exist");
			}
			else								// send the list to the client
			{
				sendMsgToClient("125 Opening ASCII mode data connection for file list.");

				for (int i = 0; i < dirContent.length; i++)
				{
					sendDataMsgToClient(dirContent[i]);
				}

				sendMsgToClient("226 Transfer complete.");
				closeDataConnection();
			}
		}
	}//_handleNlst
	/**
	 * A helper for the LIST and NLST commands. The directory name is obtained by
	 * appending "args" to the current directory. NLST does not accept a filename.
	 * @param args The directory or file to list
	 * @return an array containing names of files in a directory. If the given
	 * name is that of a file for LIST, then return an array containing only one element
	 * (the file). If the file or directory does not exist, return nul.
	 */
	private String[] listHelper(String name, listType mode)
	{
											// mode: DETAILED  => for LIST dir or filename
											//       NO_DETAIL => for NLST dir

		String[] result= {"error"};			// default one-element array to return
		ArrayList<String> build = new ArrayList<String>();	// the list to build

		String line = "";									// the current build line
		String isDir = "d";
		String isFile = "-";
		String isType = isFile;								// file type (dir d or file -)
		String permString ="---------";						// user permissions e.g rw-rw-r--
		String hLinks ="  1";								// no.of hard links e.g 2
		String owner ="user    ";							// owner e.g userfoo
		String group ="group   ";							// group e.g groupfoo
		String size ="      0";								// file size
		String date ="Mar 28 01:12";						// date and time (last modified)
		String fname = "error";								// file name

		// Construct the path of the directory (name)
		String filepath = currDirectory;					// start from current diectory, this is
															// this is the default if name is empty
		if (name == null) name="";
		if (name.startsWith ("/")) name=name.substring(1);	// cut off leading slash (works even for a single '/')

		filepath = filepath + name;						// this could be a dir or a file

		// Get a File object, and see if the name we got exists.
		File f = new File(root+filepath);

		if (!f.exists()) return null;				// not a file nor directory

		if (f.isFile())							// it's just a single file
		{
//~			debugOutput ("listHelper > isFile");
			if (mode == listType.NO_DETAIL) return null;	// NLST does not accept a file

			String[] oneFile = new String[1];	// return a 1 entry array for LIST
			result[0] = f.getName();
			return result;
		}

		if (f.isDirectory())
		{
//~			debugOutput ("listHelper > isDir");
			if (mode == listType.NO_DETAIL)			// for NLST
			{
				return f.list();					// string array - simplest way to get directory list
			}
													// else for LIST
			File[] files = f.listFiles();			// file array - for more details
//~			debugOutput ("listHelper > files length="+files.length);

			for (File file : files)
			{
//~				debugOutput(">"+file.getName());
//~				build.add ("----------   1 user     group      11430 Dec 03 2020  ftpServer.jar");

				if (file.isFile())	isType = isFile;	// get type dir or file
				else				isType = isDir;
														// get file size (bytes)
				size = String.format("%7d",((int)file.length())%10000000);
														// get date	e.g Nov 01 2020
				String dateformat = "MMM dd HH:mm";
				Date d = new Date(file.lastModified());
				if ((new Date()).after(new Date((d.getTime() + 15552000000l))))
					dateformat = "MMM dd yyyy ";		// older than 180 days
				date = new SimpleDateFormat(dateformat).format(new Date(file.lastModified()));

				fname = file.getName();					// get file name
														// build an 'ls -l' type of line
				build.add	(isType + permString+" "+hLinks+" "+owner+" "+group+
							" "+size+" "+date+" "+fname
							);
			}
			if (build.size()<1) result = new String[0];	// return an emtpy array if there are zero files
			else result = build.toArray(result);		// convert arraylist to array
			return result;
		}//_if dir
		return null;					// neither a directory nor a file
	}//_listHelper
	/**
	 * Handler for the PORT command.
	 * The client issues a PORT command to the server in active mode, so the
	 * server can open a data connection to the client through the given address
	 * and port number.
	 * @param args The first four segments (separated by comma) are the IP address.
	 *        The last two segments encode the port number (port = seg1*256 + seg2)
	 */
	private void handlePort(String args)
	{
		// Extract IP address and port number from arguments
		String[] stringSplit = args.split(",");
		String hostName = stringSplit[0] + "." + stringSplit[1] + "." +
				stringSplit[2] + "." + stringSplit[3];

		int p = Integer.parseInt(stringSplit[4])*256 + Integer.parseInt(stringSplit[5]);

		// Initiate data connection to client
		openDataConnectionActive(hostName, p);
		sendMsgToClient("200 Command OK");
	}
	/**
	 * Handler for PWD (Print working directory) command.
	 * Returns the path of the current directory back to the client.
	 */
	private void handlePwd()
	{
		debugOutput("PWD > \""+ currDirectory + "\"");
		sendMsgToClient("257 \"" + currDirectory + "\"");
	}
	/**
	 * Handler for PASV command which initiates the passive mode.
	 * In passive mode the client initiates the data connection to the server.
	 * In active mode the server initiates the data connection to the client.
	 */
	private void handlePasv()
	{
//~		String myIP = Server.getIP();
		String myIP = Server.myIP;					// get the IP saved in server class

		if (myIP == "")								// failed
		{
			sendMsgToClient("425 Can't open data connection.");
//~			sendMsgToClient("451 Requested action aborted. Local error in processing.");
		 return;
		}
											// build a normal PASV reply msg
		String split[] = myIP.split("\\.");
		int p1 = dataPort/256;
		int p2 = dataPort%256;
		String msg =
					"227 Entering Passive Mode ("
					+ split[0] +"," + split[1] + "," + split[2] + ","
					+ split[3] + "," + p1 + "," + p2 +")";

//~		debugOutput (msg);
//~		sendMsgToClient(msg);						// don't send yet, we might miss the connection

		if (!openDataConnectionPassive(dataPort, msg))		// send the msg inside this function
			debugOutput ("PASV> Can't open data connection.");
	}//_handlePasv
	/**
	 * Handler for EPSV command which initiates extended passive mode.
	 * Similar to PASV but for newer clients (IPv6 support is possible but not implemented here).
	 */
	private void handleEpsv()
	{
												// format for EPSV IPv4.	(IPv6 not supported)
		String msg = "229 Entering Extended Passive Mode (|||" + dataPort + "|)";
//~		debugOutput (msg);
//~		sendMsgToClient(msg);							// don't send yet, we might miss connection
		if (!openDataConnectionPassive(dataPort, msg))	// send the msg inside this function
			debugOutput ("EPSV> Can't open data connection.");
	}
	/**
	 * Handler for the QUIT command.
	 */
	private void handleQuit()
	{
		sendMsgToClient("221 Closing connection");
		quitCommandLoop = true;
	}

	private void handleSyst()
	{
		sendMsgToClient("215 UNIX");
	}
	/**
	 * Handler for the FEAT (features) command.
	 * Feat transmits the abilities/features of the server to the client.
	 * Needed for some ftp clients.
	 * This is just a dummy message to satisfy clients, no real feature information included.
	 */
	private void handleFeat()
	{
		sendMsgToClient("211-Extensions supported:");
		sendMsgToClient("211 END");
	}
	/**
	 * Handler for the MKD (make directory) command.
	 * Creates a new directory on the server.
	 * @param args Directory name
	 */
	private void handleMkd(String dir)
	{
		String filepath = "";
		String msg = "550 MKD> Internal Error";			// should never happen

		if (dir == null)
		{
			sendMsgToClient("550 MKD> Internal Error > null Dir name.");
			return;
		}

//~		// Allow only alphanumeric characters
//~		if (args != null && args.matches("^[a-zA-Z0-9]+$"))
		// -humpty allow almost any name for now..

		if (dir.startsWith ("/"))				// if it starts with a slash
		{
			filepath = dir;						// treat relative to root
			dir = dir.substring(1);				// cut off the slash for following name check
		}
		else
		{										// does not start with a slash,
			filepath = currDirectory + dir;		// so treat relative to current directory
		}
												// some obvious names that are not allowed
		if (dir.equals("") || dir.equals(".") || dir.equals("..") || dir.equals("/"))
		{
			msg="550 MKD> Invalid Dir name > "+dir;
			sendMsgToClient(msg);
			return;
		}
		File d = new File(root + filepath);
		try
		{
			if ( d.mkdir() )
			{
				msg="250 Directory successfully created";
			}
			else
			{
				msg="550 MKD> Failed to remove directory > "+d.toString();
			}
		}
		catch (Exception e)
		{
			msg="550 MKD> Could not remove directory > "+d.toString();
		}
		sendMsgToClient(msg);
	}//_handleMkd
	/**
	 * Handler for RMD (remove directory) command.
	 * Removes a directory.
	 * @param dir directory to be deleted.
	 */
	private void handleRmd(String dir)
	{
		String filepath = "";
		String msg = "550 RMD> Internal Error";			// should never happen

		if (dir == null)
		{
			sendMsgToClient("550 RM> Internal Error > null Dir name.");
			return;
		}
//~		// Allow only alphanumeric characters
//~		if (args != null && args.matches("^[a-zA-Z0-9]+$"))
		// -humpty allow almost any name for now..
		if (dir.startsWith ("/"))				// if it starts with a slash
		{
			filepath = dir;						// treat relative to root
			dir = dir.substring(1);				// cut off the slash for following name check
		}
		else
		{										// does not start with a slash,
			filepath = currDirectory + dir;		// so treat relative to current directory
		}
												// some obvious names that are not allowed
		if (dir.equals("") || dir.equals(".") || dir.equals("..") || dir.equals("/"))
		{
			sendMsgToClient ("550 RMD> Invalid Dir name > "+dir);
			return;
		}
		File d = new File(root + filepath);
		if (!d.exists() || !d.isDirectory())			// check if path exists and is a directory
		{
			sendMsgToClient ("550 RMD> Cannot find Dir for delete > "+dir);
			return;
		}
		try
		{
			if ( d.delete() )
			{
				msg="250 Directory successfully removed";
			}
			else
			{
				msg="550 RMD> Failed to remove directory > "+d.toString();
			}
		}
		catch (Exception e)
		{
			msg="550 RMD> Could not remove directory > "+d.toString();
		}
		sendMsgToClient(msg);
	}//_handleRmd
	/**
	 * Handler for DELE (delete file) command.
	 * Deletes a file in the current directory	// -humpty OR from root if startswith slash
	 * @param file to be deleted.
	 */
	private void handleDele(String file)
	{
		String filepath = "";
		String msg = "550 DELE> Internal Error";			// should never happen

		if (file == null)
		{
			sendMsgToClient("550 DELE> Internal Error > null file name.");
			return;
		}
//~		// Allow only alphanumeric characters
//~		if (args != null && args.matches("^[a-zA-Z0-9]+$"))
		// -humpty allow almost any name for now..
		if (file.startsWith ("/"))				// if it starts with a slash
		{
			filepath = file;					// treat relative to root
			file = file.substring(1);			// cut off the slash for following name check
		}
		else
		{										// does not start with a slash,
			filepath = currDirectory + file;	// so treat relative to current directory
		}
												// some obvious names that are not allowed
		if (file.equals("") || file.equals(".") || file.equals("..") || file.equals("/"))
		{
			sendMsgToClient ("550 DELE> Invalid file name > "+file);
			return;
		}
		File f = new File(root + filepath);
		if (!f.exists() || !f.isFile())			// check if path exists and is a file
		{
			sendMsgToClient ("550 DELE> Cannot find file for delete > "+filepath);
			return;
		}
		try
		{
			if ( f.delete() )
			{
				msg="250 Directory successfully removed";
			}
			else
			{
				msg="550 RMD> Failed to remove directory > "+f.toString();
			}
		}
		catch (Exception e)
		{
			msg="550 RMD> Could not remove directory > "+f.toString();
		}
		sendMsgToClient(msg);
	}//_handleDele
	/** -humpty
	 * Handler for RNFR (rename from path) command.
	 * Holds a path in the current directory or from root if startswith slash
	 * @param file to be renamed.
	 */
	private void handleRnFr(String file)
	{
		String filepath = "";
		String msg = "550 RNFR> Internal Error";			// should never happen

		if (file == null)
		{
			sendMsgToClient("550 RNFR> Internal Error > null file name.");
			return;
		}
//~		// Allow only alphanumeric characters
//~		if (args != null && args.matches("^[a-zA-Z0-9]+$"))
		// -humpty allow almost any name for now..
		if (file.startsWith ("/"))				// if it starts with a slash
		{
			filepath = file;					// treat relative to root
			file = file.substring(1);			// cut off the slash for following name check
		}
		else
		{										// does not start with a slash,
			filepath = currDirectory + file;	// so treat relative to current directory
		}
												// some obvious names that are not allowed
		if (file.equals("") || file.equals(".") || file.equals("..") || file.equals("/"))
		{
			sendMsgToClient ("550 RNFR> Invalid file name > "+file);
			return;
		}
		File f = new File(root + filepath);
		if (!f.exists())						// check if path exists
		{
			sendMsgToClient ("550 RNFR> Cannot find path > "+filepath);
			return;
		}
		this.renameFrom = f;					// hold this path
		msg="350 waiting for file rename TO";
		sendMsgToClient(msg);
	}//_handleRnFr
	/** -humpty
	 * Handler for RNTO (rename to file) command.
	 * Renames a file in the current directory or from root if startswith slash
	 * Will not rename to a file that already exists
	 * @param filename to rename.
	 */
	private void handleRnTo(String file)
	{
		String filepath = "";
		String msg = "550 RNTO> Internal Error";			// should never happen

		if (this.renameFrom == null)			// check there was a valid RnFr
		{
			sendMsgToClient ("550 RNTO> Invalid rename FROM ");
			return;
		}
		if (file == null)
		{
			sendMsgToClient("550 RNTO> Internal Error > null file name.");
			return;
		}
//~		// Allow only alphanumeric characters
//~		if (args != null && args.matches("^[a-zA-Z0-9]+$"))
		// -humpty allow almost any name for now..
		if (file.startsWith ("/"))				// if it starts with a slash
		{
			filepath = file;					// treat relative to root
			file = file.substring(1);			// cut off the slash for following name check
		}
		else
		{										// does not start with a slash,
			filepath = currDirectory + file;	// so treat relative to current directory
		}
												// some obvious names that are not allowed
		if (file.equals("") || file.equals(".") || file.equals("..") || file.equals("/"))
		{
			sendMsgToClient ("550 RNTO> Invalid path name > "+file);
			return;
		}
		File f = new File(root + filepath);
		if (f.exists())							// check that path does not already exist
		{
			sendMsgToClient ("550 RNTO> Cannot rename TO > "+filepath);
			return;
		}
		try
		{
			if ( (this.renameFrom).renameTo(f) )
			{
				msg="250 renamed to "+f.toString();
				this.renameFrom = null;					// reset RnFr if success
			}
			else
			{
				msg="550 RNTO> Failed to rename > "+this.renameFrom.toString();
			}
		}
		catch (Exception e)
		{
			msg="550 RNTO> Error renaming > "+this.renameFrom.toString();
		}
		sendMsgToClient(msg);
	}//_handleRnTo
	/**
	 * Handler for the TYPE command.
	 * The type command sets the transfer mode to either binary or ascii mode
	 * @param mode Transfer mode: "a" for Ascii. "i" for image/binary.
	 */
	private void handleType(String mode)
	{
		if(mode.toUpperCase().equals("A"))
		{
			transferMode = transferType.ASCII;
			sendMsgToClient("200 OK");
		}
		else if(mode.toUpperCase().equals("I"))
		{
			transferMode = transferType.BINARY;
			sendMsgToClient("200 OK");
		}
		else
			sendMsgToClient("504 Not OK");;
	}
	/**
	 * Handler for the RETR (retrieve) command.
	 * Retrieve transfers a file from the ftp server to the client.
	 * @param file The file to transfer to the user
	 */
	private void handleRetr(String file)
	{
		boolean failed = false;					// assume all ok

		if (file == null)
		{
			sendMsgToClient("550 RETR> Internal Error > null file name.");
			closeDataConnection();
			return;
		}
												// some obvious names that are not allowed
		if (file.equals("") || file.equals(".") || file.equals("..") || file.equals("/"))
		{
			sendMsgToClient ("550 RETR> Invalid file name > "+file);
			closeDataConnection();
			return;
		}

		File f =  new File(root + currDirectory + file);

		if(!f.exists())
		{
			sendMsgToClient ("550 RETR File does not exist> "+f.toString());
			closeDataConnection();
			return;
		}
		// Binary mode
		if (transferMode == transferType.BINARY)
		{
			BufferedOutputStream fout = null;
			BufferedInputStream fin = null;

			sendMsgToClient("150 Opening binary mode data connection for requested file " + f.getName());

			try
			{
				//create streams
				fout = new BufferedOutputStream(dataConnection.getOutputStream());
				fin = new BufferedInputStream(new FileInputStream(f));
			}
			catch (Exception e)
			{
				sendMsgToClient("451 Requested action aborted. Could not create file streams.");
				closeDataConnection();
				return;
			}
			debugOutput("File Send Start: " + f.getName());

			// write file with buffer
			byte[] buf = new byte[1024];
			int l = 0;
			try
			{
				while ((l = fin.read(buf,0,1024)) != -1)
				{
					fout.write(buf,0,l);
				}
			}
			catch (IOException e)
			{
				sendMsgToClient("451 Requested action aborted. read/write error.");
				failed = true;
//~				e.printStackTrace();
			}	// drop through to close
			//close streams
			try
			{
				fin.close();
				fout.close();
			}
			catch (IOException e)
			{
				if (!failed)			// if no msg was already sent, send this failure
				{
					sendMsgToClient("451 Requested action aborted. could not close streams.");
					failed = true;
				}
//~				e.printStackTrace();
			}
			finally
			{
				if (failed)
				{
					closeDataConnection();
					return;								// return immediately
				}
			}
		}//_if_binary
		else // ASCII mode
		{
			sendMsgToClient("150 Opening ASCII mode data connection for requested file " + f.getName());

			BufferedReader rin = null;
			PrintWriter rout = null;

			try
			{
				rin = new BufferedReader(new FileReader(f));
				rout = new PrintWriter(dataConnection.getOutputStream(),true);

			}
			catch (IOException e)
			{
				sendMsgToClient("451 Requested action aborted. Could not create file streams.");
				closeDataConnection();
				return;
			}
			String s;
			try
			{
				while((s = rin.readLine()) != null)
				{
					rout.println(s);
				}
			}
			catch (IOException e)
			{
				sendMsgToClient("451 Requested action aborted. read/write error.");
				failed = true;
//~				e.printStackTrace();
			}	// drop through to close

			//close streams
			try
			{
				rout.close();
				rin.close();
			}
			catch (IOException e)
			{
				if (!failed)			// if no msg was already sent, send this failure
				{
					sendMsgToClient("451 Requested action aborted. could not close streams.");
					failed = true;
				}
//~				e.printStackTrace();
			}
			finally
			{
				if (failed)
				{
					closeDataConnection();
					return;								// return immediately
				}
			}
		}//_else_ascii
														// success
		sendMsgToClient("226 File transfer successful. Closing data connection.");
		closeDataConnection();
	}//_handleRetr
	/**
	 * Handler for STOR (Store) command.
	 * Store receives a file from the client and saves it to the ftp server.
	 * @param file The file that the user wants to store on the server
	 */
	private void handleStor(String file)
	{
		boolean failed = false;					// assume all ok

		if (file == null)
		{
			sendMsgToClient("550 STOR> Internal Error > null file name.");
			closeDataConnection();
			return;
		}
												// some obvious names that are not allowed
		if (file.equals("") || file.equals(".") || file.equals("..") || file.equals("/"))
		{
			sendMsgToClient ("550 STOR> Invalid file name > "+file);
			closeDataConnection();
			return;
		}

		File f =  new File(root + currDirectory + file);

//~		if(f.exists())
//~		{
//~			sendMsgToClient("550 File already exists");
//~		}								// -humpty: don't reject. rfc959 accepts a STOR,
										//			even if the file already exists, it will be overwritten.
		// Binary mode
		if (transferMode == transferType.BINARY)
		{
			BufferedOutputStream fout = null;
			BufferedInputStream fin = null;

			sendMsgToClient("150 Opening binary mode data connection for requested file " + f.getName());
			try
			{
				// create streams
				fout = new BufferedOutputStream(new FileOutputStream(f));
				fin = new BufferedInputStream(dataConnection.getInputStream());
			}
			catch (Exception e)
			{
				sendMsgToClient("451 Requested action aborted. Could not create file streams.");
				closeDataConnection();
				return;
			}
			debugOutput("File Receive Start: " + f.getName());

			// write file with buffer
			byte[] buf = new byte[1024];
			int l = 0;
			try
			{
				while ((l = fin.read(buf,0,1024)) != -1)
				{
					fout.write(buf,0,l);
				}
			}
			catch (IOException e)
			{
				sendMsgToClient("451 Requested action aborted. read/write error.");
				failed = true;
//~				e.printStackTrace();
			}	// drop through to close
			//close streams
			try
			{
				fin.close();
				fout.close();
			}
			catch (IOException e)
			{
				if (!failed)			// if no msg was already sent, send this failure
				{
					sendMsgToClient("451 Requested action aborted. could not close streams.");
					failed = true;
				}
//~				e.printStackTrace();
			}
			finally
			{
				if (failed)
				{
					closeDataConnection();
					return;								// return immediately
				}
			}
		}//_binary
		else // ASCII mode
		{
			sendMsgToClient("150 Opening ASCII mode data connection for requested file " + f.getName());

			BufferedReader rin = null;
			PrintWriter rout = null;

			try
			{
				rin = new BufferedReader(new InputStreamReader(dataConnection.getInputStream()));
				rout = new PrintWriter(new FileOutputStream(f),true);

			}
			catch (IOException e)
			{
				sendMsgToClient("451 Requested action aborted. Could not create file streams.");
				closeDataConnection();
				return;							// return immediately
			}
			String s;
			try
			{
				while((s = rin.readLine()) != null)
				{
					rout.println(s);
				}
			}
			catch (IOException e)
			{
				sendMsgToClient("451 Requested action aborted. read/write error.");
				failed = true;
//~				e.printStackTrace();
			}	// drop through to close
			try
			{
				rout.close();
				rin.close();
			}
			catch (IOException e)
			{
				if (!failed)			// if no msg was already sent, send this failure
				{
					sendMsgToClient("451 Requested action aborted. could not close streams.");
					failed = true;
				}
//~				e.printStackTrace();
			}
			finally
			{
				if (failed)
				{
					closeDataConnection();
					return;								// return immediately
				}
			}
		}//_else_ascii
		sendMsgToClient("226 File transfer successful. Closing data connection.");
		closeDataConnection();
	}//_handleStor
	/**
	 * Helper method to parse the arguments of the EXXX commands (e.g. EPRT).
	 * EXXX commands are newer and support IPv6 (not supported here). The arguments
	 * get translated back to a "regular" argument.
	 * @param extArg The extended argument
	 * @return The regular argument
	 */
	private String parseExtendedArguments(String extArg)
	{
		String[] splitArgs = extArg.split("\\|");
		String ipAddress = splitArgs[2].replace('.', ',');
		int port = Integer.parseInt(splitArgs[3]);
		int p1 = port/256;
		int p2 = port%256;

		return ipAddress + "," + p1 + "," + p2;
	}
	/**
	 * Debug output to the console. Also includes the Thread ID for better readability.
	 * @param msg Debug message
	 */
	private void debugOutput(String msg)
	{
		if (debugMode)
		{
			System.out.println("Thread " + this.getId() + ": " + msg);
		}
	}
}//_Worker
