package ftpServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import java.net.InetAddress;				// -humpty: to get myIP
import java.net.DatagramSocket;

/**
 * FTP Server class.
 * On receiving a new connection it creates a new worker thread.
 * 
 * @author Moritz Stueckler (SID 20414726)
 *
 */
/*
 * hFTPS 	v1.0 Modified by -humpty Nov 2020 - Sep 2021 cater for android + enhancements
 * 			v1.3 (see Worker.java)
 */
public class Server extends Thread				// for embedded mode, create object and use start instead of run.
{												// there should only ever be one object for the server,
												// which in turn can create more than one worker.

    public static boolean serverRunning = false;	// the only one for this class
    public static int controlPort = 2345;			// ditto.
	public static String myIP = "(error)";			// ditto. For the caller to display ip and for worker to use
	public static String root;						// the server root directory and for worker to use
													// set externally if embedded or in main if cmdline

    public ServerSocket welcomeSocket;				// this is for the caller to close us
	private Socket client = null;

    public static void main(String[] args)			// for command line invocation, use run to keep on same thread
    {
		root = System.getProperty("user.dir");	// limit root to here for PC as host
        Server mainServer = new Server();
		mainServer.run();
    }

    public Server()
    {
        super();
	}

    public void run()
    {
        try
        {
            welcomeSocket = new ServerSocket(controlPort);
			serverRunning = true;
        }
        catch (Exception e)
        {
			serverRunning = false;
            System.out.println("Could not create server socket");
			return;
//~            System.exit(-1);
        }
        
		myIP = getIP();									// save it for caller and also a for worker(s)
        System.out.println("hFTP Server started. Listening.. " + myIP + ":" + controlPort);
        int noOfThreads = 0;
        
        while (serverRunning)
        {
            try
            {
                client = welcomeSocket.accept();				// (this will block)
                
                // Port for incoming dataConnection (for passive mode) is the controlPort + number of created threads + 1
                int dataPort = controlPort + noOfThreads + 1;
                
                // Create new worker thread for new connection
                Worker worker = new Worker(client, dataPort);

                System.out.println("New connection received. Worker was created.");
                noOfThreads++;
                worker.start();
            }
            catch (IOException e)						// closed by caller, otherwise something else above
            {
                System.out.println("Server Exception, closing..");
				break;									// quit the loop for shutdown
//~				e.printStackTrace();
            }
        }//_while
		try
        {
            if (!welcomeSocket.isClosed()) welcomeSocket.close();
			serverRunning = false;						// flag that we have stopped
            System.out.println("Server was stopped");
        }
		catch (IOException e)
        {
            System.out.println("Problem closing server"); 
//            System.exit(-1);
        }
    }//_run
//----------------------------------------------
	public static String getIP ()							// get my IP into myIP
	{
		String myIP = "";							// assume failed = ""
		//-Moritz Stückler (2016)
		// "Using fixed IP for connections on the same machine
		//  For usage on separate hosts, we'd need to get the local IP address from somewhere
		//  Java sockets did not offer a good method for this".
		// (A)	String myIP = "127.0.0.1";		// this only works if client and server is the same machine

		//-humpty (2020)
		// or
		// (B)	use the simplest method even though it may not work if the network is complicated
		//		myIP = InetAddress.getLocalHost().getHostAddress();		// (B) doesn't work, gets 127.0.0.1
		// or
		// (C)	use NetworkInterface to get a list of IPs and pick the first one
		// 		that isn't loopback or link-local		(aka Basic's SOCKET.MYIP)
		// or
		// (D) use a DatagramSocket 		(https://stackoverflow.com/questions/9481865/)
		try(final DatagramSocket socket = new DatagramSocket())
		{
			socket.connect(InetAddress.getByName("8.8.8.8"), 10002);	// (this doesn't have to be reachable)
			myIP = socket.getLocalAddress().getHostAddress();
			if (myIP.equals("::")) myIP = "127.0.0.1";				// fallback to loopback on same machine
		}
		catch(Exception e)
		{
			System.out.println("Could not get myIP");
			myIP = "127.0.0.1";								// don't fail, return something
		}
		return myIP;
	}//_getIP
//----------------------------------------------
}//_Server
