package com.rfo.LASKmobile;

//import android.support.v4.provider.DocumentFile;	// saf	-- not working - use Compat instead
import com.commonsware.cwac.document.DocumentFileCompat;

import java.io.FileDescriptor;
import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;

import java.io.IOException;
import android.content.Context;
//import android.widget.Toast;
//----------------------------------------------
public class SAFtools							// Utils using SAF API to Secondary External Shared Storage
{
												// uses static methods (no object required)

	public static DocumentFileCompat FindFile ( String path )	// find a saf file or dir
	{															// accept a path from the base
																// return null = failed
																// else df of file
										// (a filename could also be a directory but must not end with a '/')
	 DocumentFileCompat saf_file, saf_dir;
	 saf_dir = Run.saf_base;							// start from base and work our way into path
	 if (saf_dir == null) return null;

	 String[] sdirs = path.split("/");					// get sub dirs into an array
	 int z = sdirs.length;
	 if (z<1) return null;								// no items left (all empty trails)
	 for ( int i=0 ; i<z-1; i++ )
	 {
		String sub_dir = sdirs[i];
		if (sub_dir.equals ("") && i==0) continue;		// ignore slash at beginning of path

		saf_dir = saf_dir.findFile(sub_dir);				// go into sub dir
		if (saf_dir==null || !saf_dir.isDirectory()) return null;
	 }//_for
												// we should now be in the last	dir
	 String fileName = sdirs[z-1];				// get filename
	 if (fileName.equals("")) return null;		// it's empty

	 saf_file = saf_dir.findFile(fileName);		// null if not exists

	 if (saf_file == null || !saf_file.exists())	// file not found
	  return null;

	 return saf_file;								// found (file or dir)
	}//_FindFile
//----------------------------------------------
	public static DocumentFileCompat CreateDir ( String path)	// create a dir (if it doesn't exist)
	{															// accept a path from the base
																// return null = failed
																// else df of dir
																// (path must not end with a '/')
	 DocumentFileCompat saf_dir, saf_hold;
	 saf_dir = Run.saf_base;							// start from base and work our way into path
	 if (saf_dir == null) return null;

	 String[] sdirs = path.split("/");					// get sub dirs into an array
	 int z = sdirs.length;
	 if (z<1) return null;								// no items left (all empty trails)
	 for ( int i=0 ; i<z; i++ )							// for all in path
	 {
		String sub_dir = sdirs[i];						// next sub dir
		if (sub_dir.equals ("") && i==0) continue;		// ignore slash at beginning of path

		saf_hold=saf_dir;
		saf_dir = saf_dir.findFile(sub_dir);				// try to go into sub dir
		if (saf_dir!=null)									// something exists
		{
			if (!saf_dir.isDirectory()) return null;		// file exists with the same name
		}
		else											// not yet exists
		{
		 try { saf_dir = saf_hold.createDirectory(sub_dir);}	// create it (null if failed)
		 catch (Exception e) { saf_dir = null; }			// don't error
		}
		if (saf_dir == null) return null;					// failed to create

	 }//_for

	 if (!saf_dir.exists()) return null;				// sanity check

	 return saf_dir;									// done, return df
	}//_CreateDir
//----------------------------------------------
	public static DocumentFileCompat FindDir ( String path )	// find a saf dir before a file path
	{															// accept a path from the base
																// return null = failed
																// else df of the dir
																// dir should end with '/<file>'
	 DocumentFileCompat saf_file, saf_dir;
	 saf_dir = Run.saf_base;							// start from base and work our way into path
	 if (saf_dir == null) return null;

	 String[] sdirs = path.split("/",-1);				// get sub dirs into an array, keep trailing
														// ..filename or empty string 
	 int z = sdirs.length;
	 if (z<1) return null;								// sanity check, should not happen
	 for ( int i=0 ; i<z-1; i++ )						// ignore last entry (filename or empty string)
	 {
		String sub_dir = sdirs[i];
		if (sub_dir.equals ("") && i==0) continue;		// ignore slash at beginning of path

		saf_dir = saf_dir.findFile(sub_dir);			// go into sub dir

		if (saf_dir==null || !saf_dir.isDirectory()) return null;
	 }//_for
													// we should now be in the last	dir
	 return saf_dir;								// just return it
	}//_FindDir
//----------------------------------------------
	public static Run.FileInfo OpenTextReader ( String path )
	{
											// open text file for read
											// do not error
											// return	new TextReaderInfo = ok
											//			null = failed

	 DocumentFileCompat saf_file = FindFile (path);
	 if (saf_file==null || !saf_file.isFile()) return null;	// failed, not found or not a file

	 int flen = (int)Math.min(saf_file.length(), Integer.MAX_VALUE); // file length

	 BufferedReader buf = null;
	 try 
	 {
		buf = new BufferedReader(new InputStreamReader(saf_file.openInputStream()));
	 }
	 catch (Exception e)				// (do not error)
	 {
		buf = null;
	 }
	 if (buf == null) return null;			// failed

	 Run.TextReaderInfo fInfo = new Run.TextReaderInfo(0);	// Prepare the FileTable object
	 fInfo.mTextReader = buf;				// store reader in fInfo
	 try
	 {
		if ( buf.markSupported() )
		{
		 buf.mark(flen);
		 fInfo.mark(1, flen);
		}
	 }
	 catch (Exception e)				// (do not error)
	 {
		fInfo=null;
	 }
	 return fInfo;							// and return it
	}//_OpenTextReader()
//----------------------------------------------
	public static Run.FileInfo OpenTextWriter ( String path, int mode, Context appContext )
	{										// open text file for write
											// mode : 1=>write, 2=>append
											// do not error
											// return	new TextWriterInfo = ok
											//			null = failed

	 DocumentFileCompat saf_dir = FindDir (path);
	 if (saf_dir == null) return null;		// failed, dir not found

	 DocumentFileCompat saf_file = FindFile (path);
	 if (saf_file!=null && saf_file.isDirectory()) return null;	// cannot overwrite a dir !

	 String fileName="";
	 if (saf_file == null || !saf_file.exists())		// not found or not exist
	 {
	 	try
		{
			String[] s = path.split("/");					// split the path
			fileName = s[s.length - 1];						// get the file name
			saf_file = saf_dir.createFile ("*/*",fileName);	// create the file
		}
		catch (Exception e)
		{											// don't error
			saf_file = null;
		}
	 }
 
	 if (saf_file == null || !saf_file.exists() || !saf_file.canWrite())	// failed to create ?
		return null;

	 long fsize = saf_file.length();
	 String fmode = (mode==1)?"w":"rw";		// append requires read+write

	 OutputStreamWriter writer = null;		// -humpty 0271
	 try
	 {										// open the writer
	  FileDescriptor fd = appContext.getContentResolver()
							  .openFileDescriptor(saf_file.getUri(), fmode)
							  .getFileDescriptor();
	  FileOutputStream fos = new FileOutputStream(fd);
	  if (mode==2)							//if append requested
	  {
		FileChannel fc = fos.getChannel();
		fc.position (fsize);				// goto eof (zero based)
	  }

	  writer = new OutputStreamWriter (fos);

	 }
	 catch (IOException e)
	 {
	  writer = null;
	 }

	 if (writer == null) return null;		// failed to create Writer

	 Run.TextWriterInfo fInfo = new Run.TextWriterInfo(1);	// Prepare the FileTable object
	 fInfo.mTextWriter = writer;			// store writer in fInfo
	 return fInfo;							// and return it
	}//_OpenTextWriter()
//----------------------------------------------
	public static Run.FileInfo OpenByteReader ( String path )
	{
											// open byte file for read
											// do not error
											// return	new ByteReaderInfo = ok
											//			null = failed

	 DocumentFileCompat saf_file = FindFile (path);
	 if (saf_file==null || !saf_file.isFile()) return null;	// failed, not found or not a file

	 int flen = (int)Math.min(saf_file.length(), Integer.MAX_VALUE); // file length

	 BufferedInputStream buf = null;
	 try 
	 {
		buf = new BufferedInputStream(saf_file.openInputStream());
	 }
	 catch (Exception e)				// (do not error)
	 {
	  buf = null;
	 }
	 if (buf == null) return null;			// failed

     Run.ByteReaderInfo fInfo = new Run.ByteReaderInfo(0);	// Prepare the FileTable object

	 fInfo.mByteReader = buf;				// store reader in fInfo
	 try
	 {
		 if ( buf.markSupported() )
		 {
		  buf.mark(flen);
		  fInfo.mark(1, flen);
		 }
	 }
	 catch (Exception e)				// (do not error)
	 {
	  fInfo=null;
	 }
	 return fInfo;							// and return it
	}//_OpenByteReader()
//----------------------------------------------
	public static Run.FileInfo OpenByteWriter ( String path, int mode, Context appContext )
	{										// open byte file for read
											// mode : 1=>write, 2=>append
											// do not error
											// return	new ByteWriterInfo = ok
											//			null = failed

	 DocumentFileCompat saf_dir = FindDir (path);
	 if (saf_dir == null) return null;		// failed, dir not found

	 DocumentFileCompat saf_file = FindFile (path);
	 if (saf_file!=null && saf_file.isDirectory()) return null;	// cannot overwrite a dir !

	 String fileName="";
	 if (saf_file == null || !saf_file.exists())		// not found or not exist
	 {
		try
		{
			String[] s = path.split("/");				// split the path
			fileName = s[s.length - 1];					// get the file name
			saf_file = saf_dir.createFile ("*/*",fileName);	// create the file
		}
		catch (Exception e)
		{									// don't error
			saf_file = null;
		}
	 }

	 if (saf_file == null || !saf_file.exists() || !saf_file.canWrite())	// failed to create ?
		return null;

	 long fsize = saf_file.length();
	 String fmode = (mode==1)?"w":"rw";		// append requires read+write

	 FileOutputStream fos = null;
	 try
	 {										// open the writer
	  FileDescriptor fd = appContext.getContentResolver()
							  .openFileDescriptor(saf_file.getUri(), fmode)
							  .getFileDescriptor();
	  fos = new FileOutputStream (fd);
	  if (mode==2)							//if append requested
	  {
		FileChannel fc = fos.getChannel();
		fc.position (fsize);				// goto eof (zero based)
	  }
	 }
	 catch (Exception e)
	 {
	  fos = null;
	 }
	 if (fos == null) return null;		// failed to create FileOutputStream

     Run.ByteWriterInfo fInfo = new Run.ByteWriterInfo(1);	// Prepare the FileTable object
	 fInfo.mByteWriter = fos;			// store writer in fInfo
	 return fInfo;							// and return it
	}//_OpenByteWriter()
//----------------------------------------------
}//_SAFtools
