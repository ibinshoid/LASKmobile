/****************************************************************************************************

BASIC! is an implementation of the Basic programming language for
Android devices.

Copyright (C) 2010 - 2016 Paul Laughton

This file is part of BASIC! for Android

    BASIC! is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BASIC! is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BASIC!.  If not, see <http://www.gnu.org/licenses/>.

    You may contact the author or current maintainers at http://rfobasic.freeforums.org

*************************************************************************************************/

package com.rfo.LASKmobile;

import static com.rfo.LASKmobile.Run.EventHolder.*;

import java.util.ArrayList;
import java.util.Iterator;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Region;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;
import android.graphics.RectF;
import android.graphics.Bitmap;

import android.view.WindowInsets;				// -humpty 0304
import android.view.WindowInsets.Type;
import android.view.WindowInsetsController;
import android.view.ViewGroup;
import android.view.View.OnApplyWindowInsetsListener;
import android.os.Handler;
import android.graphics.drawable.ColorDrawable;
import android.text.Spanned;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.app.ActionBar;
import android.app.ActionBar.OnMenuVisibilityListener;

public class GR extends Activity {
	private static final String LOGTAG = "GR";

	public static final String EXTRA_SHOW_DECORS = "decors";
	public static final String EXTRA_ORIENTATION = "orientation";
	public static final String EXTRA_BACKGROUND_COLOR = "background";

	public static Object LOCK = new Object();
	public static boolean waitForLock = false;

	public static Context context;
	public static DrawView drawView;
	public static Bitmap screenBitmap = null;
//	public static boolean NewTouch[] = {false, false};
//	public static double TouchX[] = {0,0};
//	public static double TouchY[] = {0,0};
	public static float scaleX = 1f;
	public static float scaleY = 1f;
	public static boolean Running = false;				// flag set when Open object runs
	public static boolean RunPaused = true;				// -humpty: flag set when OnPause
	private boolean mCreated = false;					// flat set when onCreate is complete
	public static boolean NullBitMap = false;
	public static float Brightness = -1;

	public static boolean doSTT = false;
	public static boolean doEnableBT = false;
	public static boolean startConnectBT = false;

	public enum VISIBLE { SHOW, HIDE, TOGGLE; }

	private static boolean showStatusBar = true;
	private static boolean showNavBar = true;
	private static boolean saved_showStatusBar = true;
	private static boolean saved_showNavBar = true;

	private	boolean showActionBar =	false;
	private boolean SysBarInCall = false;
	private View.OnSystemUiVisibilityChangeListener sysBarListener1 = null;
	private View.OnApplyWindowInsetsListener sysBarListener2 = null;
	private boolean restoringBars = false;
	private boolean darkText = false;						// dark text for hidden status/nav bars

	public static String TitleText = "Graphics";			// actionbar title text
	public static Integer TitleTextColor = null;			// actionbar text color
	public static Integer TitleBackColor = null;			// actionbar bg color
	public static String TitleSubText = "";					// subtitle text
	public static Integer TitleSubColor = null;				// subtitle text color

	public static Handler mHandler;							// GR post handler

	// ************* enumeration of BASIC! Drawable Object types **************

	public enum Type {
		Null("null",				new String[0]),
		Point("point",				new String[]
				{ "x", "y" } ),
		Line("line",				new String[]
				{ "x1", "y1", "x2", "y2" } ),
		Rect("rect",				new String[]
				{ "left", "right", "top", "bottom" } ),
		Circle("circle",			new String[]
				{ "x", "y", "radius" } ),
		Oval("oval",				new String[]
				{ "left", "right", "top", "bottom" } ),
		Arc("arc",					new String[]
				{ "left", "right", "top", "bottom",
					"start_angle", "sweep_angle", "fill_mode" } ),
		Poly("poly",				new String[]
				{ "x", "y", "list" } ),
		Bitmap("bitmap",			new String[]
				{ "x", "y", "bitmap" } ),
		SetPixels("set.pixels",		new String[]
				{ "x", "y" } ),
		Text("text",				new String[]
				{ "x", "y", "text" } ),
		Group("group",				new String[]
				{ "list" } ),
		RotateStart("rotate.start",	new String[]
				{ "x", "y", "angle" } ),
		RotateEnd("rotate.end",		new String[0]),
		ClipStart("clip.start",		new String[]
				{ "left", "right", "top", "bottom", "RO" } ),
		ClipEnd("clip.end",			new String[0]),				// -humpty 0261
		Open("open",				new String[0]),
		Close("close",				new String[0]);

		private final String mType;
		private final String[] mParameters;		// all parameters except "paint" and "alpha"
		Type(String type, String[] parameters) {
			mType = type;
			mParameters = parameters;
		}

		public String type()			{ return mType; }
		public String[] parameters()	{ return mParameters; }

		public boolean hasParameter(String parameter) {
			for (String p : mParameters) {
				if (parameter.equals(p)) { return true; }
			}
			if (parameter.equals("paint")) { return true; }
			if (parameter.equals("alpha")) { return true; }
			return false;
		}
	}

	// ********************* BASIC! Drawable Object class *********************
	// Objects go on the Display List. Not related to Android's Drawable class.

	public static class BDraw {
		private final Type mType;
		private String mErrorMsg;

		private int mBitmap;							// index into the BitmapList
		private int mPaint;								// index into the PaintList
		private int mAlpha;
		private boolean mVisible;

		private String mText;							// for Type.Text
		private int mClipOpIndex;						// for getValue
		private Region.Op mClipOp;						// for Type.Clip
		private int mListIndex;
		private ArrayList<Double> mList;
		private Var.ArrayDef mArray;					// for Type.SetPixels
		private int mArrayStart;						// position in array to start pixel array
		private int mArraySublength;					// length of array segment to use as pixel array
		private int mRadius;							// for Type.Circle
		private float mAngle_1;							// for Type.Rotate, Arc
		private float mAngle_2;							// for Type.Arc
		private int mFillMode;							// for getValue
		private boolean mUseCenter;						// for Type.Arc

		private int mLeft;								// left, x, or x1
		private int mRight;								// right or x2
		private int mTop;								// top, y, or y1
		private int mBottom;							// bottom or y2

		public BDraw(Type type) {
			mType = type;
			mVisible = true;
			mErrorMsg = "";
		}

		public void common(int paintIndex, int alpha) { mPaint = paintIndex; mAlpha = alpha; }

		// setters
		// For now, range checking for bitmap, paint, and list must be done in Run.
		public void bitmap(int bitmap) { mBitmap = bitmap; }	// index into the BitmapList
		public void paint(int paint) { mPaint = paint; }		// index into the PaintList
		public void alpha(int alpha) { mAlpha = alpha & 255; }

		public void xy(int[] xy) { mLeft = mRight = xy[0]; mTop = mBottom = xy[1]; }
		public void ltrb(int[] ltrb) { mLeft = ltrb[0]; mTop = ltrb[1]; mRight = ltrb[2]; mBottom = ltrb[3]; }
		public void radius(int radius) { mRadius = radius; }
		public void text(String text) { mText = text; }
		public void array(Var.ArrayDef array, int start, int sublength) {
			mArray = array;
			mArrayStart = start;
			mArraySublength = sublength;
		}
		public void angle(float angle) { mAngle_1 = angle; }

		public void show(VISIBLE show) {
			switch (show) {
				case SHOW: mVisible = true; break;
				case HIDE: mVisible = false; break;
				case TOGGLE: mVisible = !mVisible; break;
			}
		}

		public void clipOp(int opIndex) {
			Region.Op[] ops = {
				Region.Op.INTERSECT, Region.Op.DIFFERENCE			// -humpty 0261 only entertain these two OPs
			};
			mClipOpIndex = opIndex;
			mClipOp = ops[opIndex];
		}

		public void list(int index, ArrayList<Double> list) {
			mListIndex = index;
			mList = list;
		}

		public void useCenter(int fillMode) {
			mFillMode = fillMode;
			mUseCenter = (fillMode != 0);
		}

		public void arc(int[] ltrb, float startAngle, float sweepAngle, int fillMode) {
			ltrb(ltrb);
			mAngle_1 = startAngle;
			mAngle_2 = sweepAngle;
			useCenter(fillMode);
		}

		public void circle(int[] xy, int radius) {
			xy(xy);					// (x, y) in mTop and mLeft marks the CENTER of the circle
			mRadius = radius;
		}

		public void move(int[] dxdy) {
			int dx = dxdy[0];
			int dy = dxdy[1];
			mLeft += dx; mRight += dx;
			mTop += dy; mBottom += dy;
		}

		// universal getters
		public Type type()			{ return mType; }
		public String errorMsg()	{ return mErrorMsg; }
		public int bitmap()			{ return mBitmap; }	// index into the BitmapList
		public int paint()			{ return mPaint; }	// index into the PaintList
		public int alpha()			{ return mAlpha; }
		public boolean isHidden()	{ return !mVisible; }
		public boolean isVisible()	{ return mVisible; }

		// type-specific getters
		public String text()		{ return mText; }
		public Region.Op clipOp()	{ return mClipOp; }
		public ArrayList<Double> list() {
			if (mList == null) { mList = new ArrayList<Double>(); }
			return mList;
		}
		public Var.ArrayDef array()	{ return mArray; }
		public int arrayStart()		{ return mArrayStart; }
		public int arraySublength()	{ return mArraySublength; }
		public int radius()			{ return mRadius; }
		public float angle()		{ return mAngle_1; }
		public float arcStart()		{ return mAngle_1; }
		public float arcSweep()		{ return mAngle_2; }
		public boolean useCenter()	{ return mUseCenter; }

		// coordinate getters
		public int x()				{ return mLeft; }
		public int x1()				{ return mLeft; }
		public int left()			{ return mLeft; }
		public int y()				{ return mTop; }
		public int y1()				{ return mTop; }
		public int top()			{ return mTop; }
		public int x2() 			{ return mRight; }
		public int right()			{ return mRight; }
		public int y2()				{ return mBottom; }
		public int bottom()			{ return mBottom; }

		// For GR.Get.Value
		public double getValue(String p) {
			if (p.equals("paint"))	{ return mPaint; }
			if (p.equals("alpha"))	{ return mAlpha; }

			switch (mType) {
				case Circle:	if (p.equals("radius")) { return mRadius; }
				case Point:
				case SetPixels:
				case Text:	// For now, "text" must be handled by Run
					if (p.equals("x"))				{ return mLeft; }
					if (p.equals("y"))				{ return mTop; }
					break;
				case Line:
					if (p.equals("x1"))				{ return mLeft; }
					if (p.equals("y1"))				{ return mTop; }
					if (p.equals("x2"))				{ return mRight; }
					if (p.equals("y2"))				{ return mBottom; }
					break;
				case ClipStart:		if (p.equals("RO")) { return mClipOpIndex; }
				case Oval:
				case Rect:
					if (p.equals("left"))			{ return mLeft; }
					if (p.equals("top"))			{ return mTop; }
					if (p.equals("right"))			{ return mRight; }
					if (p.equals("bottom"))			{ return mBottom; }
					break;
				case Arc:
					if (p.equals("start_angle"))	{ return mAngle_1; }
					if (p.equals("sweep_angle"))	{ return mAngle_2; }
					if (p.equals("fill_mode"))		{ return mFillMode; }
					if (p.equals("left"))			{ return mLeft; }
					if (p.equals("top"))			{ return mTop; }
					if (p.equals("right"))			{ return mRight; }
					if (p.equals("bottom"))			{ return mBottom; }
					break;
				case Poly:
					if (p.equals("x"))				{ return mLeft; }
					if (p.equals("y"))				{ return mTop; }
				case Group:
					if (p.equals("list"))			{ return mListIndex; }
					break;
				case Bitmap:
					if (p.equals("bitmap"))			{ return mBitmap; }
					if (p.equals("x"))				{ return mLeft; }
					if (p.equals("y"))				{ return mTop; }
					break;
				case RotateStart:
					if (p.equals("angle"))			{ return mAngle_1; }
					if (p.equals("x"))				{ return mLeft; }
					if (p.equals("y"))				{ return mTop; }
					break;
				case Close:
				case Null:
				case RotateEnd:
				case ClipEnd:						// -humpty 0261
				default:							break;
			}
			return 0.0;
		} // getValue(String)

		// For GR.Modify
		private boolean mod_xy(String p, int val) {
			if (p.equals("x"))		{ mLeft = mRight  = val; return true; }
			if (p.equals("y"))		{ mTop  = mBottom = val; return true; }
			return false;
		}
		private boolean mod_x2y2(String p, int val) {
			if (p.equals("x1"))		{ mLeft   = val; return true; }
			if (p.equals("y1"))		{ mTop    = val; return true; }
			if (p.equals("x2"))		{ mRight  = val; return true; }
			if (p.equals("y2"))		{ mBottom = val; return true; }
			return false;
		}
		private boolean mod_ltrb(String p, int val) {
			if (p.equals("left"))	{ mLeft   = val; return true; }
			if (p.equals("top"))	{ mTop    = val; return true; }
			if (p.equals("right"))	{ mRight  = val; return true; }
			if (p.equals("bottom"))	{ mBottom = val; return true; }
			return false;
		}
		public boolean modify(String p, int iVal, float fVal, String text) {
														// -humpty 0303 Groups are still handled in Run.
														// 	All below are still single objects

			if (p.equals("paint"))						// paint is common to all
			{
				if ((iVal < 1) || (iVal >= Run.PaintList.size()))	// paint range check is now done here
					{ mErrorMsg = "Invalid Paint object number" + iVal; return false; }
				paint (iVal); return true;
			}
														// alpha is common to all
			if (p.equals("alpha"))	{ alpha(iVal); return true; }

			switch (mType) {
				case Circle:	if (p.equals("radius"))	{ mRadius = iVal; return true; }
				case Bitmap:	// BitmapList range check now done here

								if (p.equals("bitmap"))
								{
									if ((iVal < 0) | (iVal >= Run.BitmapList.size()))
										{mErrorMsg = "Bitmap pointer "+iVal+" out of range"; return false;}
									bitmap(iVal); return true;
								}
				case Point:
				case Poly:		// for now, list parm must be handled in Run
				case SetPixels:	if (mod_xy(p, iVal))	{ return true; } else { break; }

				case Line:		if (mod_x2y2(p, iVal))	{ return true; } else { break; }

				case ClipStart:		if (p.equals("RO"))		{ clipOp(iVal); return true; }
				case Oval:
				case Rect:		if (mod_ltrb(p, iVal))	{ return true; } else { break; }

				case Arc:
					if (p.equals("start_angle"))	{ mAngle_1 = fVal; return true; }
					if (p.equals("sweep_angle"))	{ mAngle_2 = fVal; return true; }
					if (p.equals("fill_mode"))		{ useCenter(iVal); return true; }
					if (mod_ltrb(p, iVal))			{ return true; }
					break;

				case RotateStart:
					if (p.equals("angle"))			{ mAngle_1 = fVal; return true; }
					if (mod_xy(p, iVal))			{ return true; }
					break;

				case Text:
					if (p.equals("text"))			{ mText = text; return true; }
					if (mod_xy(p, iVal))			{ return true; }
					break;

				case Close:
				case Group:		// for now, list parm must be handled in Run
				case Null:
				case RotateEnd:
				case ClipEnd:						// -humpty 0261
				default:							break;
			}
			mErrorMsg = "Object does not contain: " + p;
			return false;
		} // modify(String, int, float, String)
	} // BDraw class
//------------------------------------------------------
	public void grSetTitle()						// change GR actionbar title and colors
	{
		if (!showActionBar) return;			// do nothing if no actionbar
		Runnable r = new Runnable()
		{
			@Override
		    public void run()
			{
				if (TitleBackColor!=null)
					getActionBar().setBackgroundDrawable(new ColorDrawable(TitleBackColor));

				SpannableString ss = new SpannableString(TitleText);
				if (TitleTextColor!=null)
					ss.setSpan(new ForegroundColorSpan(TitleTextColor), 0, TitleText.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
				getActionBar().setTitle(ss);
			}
		};
		mHandler.post(r);
	}//_grSetTitle
//------------------------------------------------------
	public void grSetSubTitle()						// change GR subtitle and/or color
	{
		if (!showActionBar) return;			// do nothing if no actionbar
		Runnable r = new Runnable()
		{
			@Override
		    public void run()
			{
				SpannableString ss = new SpannableString(TitleSubText);
				if (TitleSubColor!=null)
					ss.setSpan(new ForegroundColorSpan(TitleSubColor), 0, TitleSubText.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
				getActionBar().setSubtitle(ss);
			}
		};
		mHandler.post(r);
	}//_grSetSubTitle
//------------------------------------------------------
	private void restoreBars()						// restore bars after a swipe
	{
		if (restoringBars)  return;					// don't overlap
		restoringBars = true;

		Handler handler = new Handler();
		Runnable r = new Runnable()
		{
			@Override
		    public void run()
			{
				setStatusBar(showStatusBar);	// restore statusbar to default state
				setNavBar (showNavBar);			// restore navbar to default state
				restoringBars = false;	// done
			}
		};
		handler.postDelayed(r, 3000);
	}//_restoreBars()
//------------------------------------------------------
	private void install_BarListener()							// listen for bar changes
	{
			final View decor = getWindow().getDecorView();

			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
			{
				if (sysBarListener1 != null) return;			// don't install twice
				sysBarListener1 = new View.OnSystemUiVisibilityChangeListener()
				{
					@Override
					public void onSystemUiVisibilityChange(int visibility)
					{
						SysBarInCall =
						(!showStatusBar && ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN)==0))
						||
						(!showNavBar && ((visibility & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)==0))
						;
						if (SysBarInCall) restoreBars();
					}
				};//_sysBarListener1
				decor.setOnSystemUiVisibilityChangeListener(sysBarListener1);
			}
			else	// Android 11+
			{
				if (sysBarListener2 != null) return;			// don't install twice
				sysBarListener2 = new View.OnApplyWindowInsetsListener() 
				{
					@Override
					public WindowInsets onApplyWindowInsets(View v, WindowInsets insets)
					{
						SysBarInCall =
						(!showStatusBar && insets.isVisible(WindowInsets.Type.statusBars()))
						||
						(!showNavBar && insets.isVisible(WindowInsets.Type.navigationBars()))
						;
						if (SysBarInCall) restoreBars();
						return insets;
					}//_onApply
				};//_navBarListener2
				decor.setOnApplyWindowInsetsListener (sysBarListener2);
			}//_Android 11+
	}//_install_BarListener
//------------------------------------------------------
	private void setHideColors ()						// set text color for hidden bars API 30+
	{
		if (Build.VERSION.SDK_INT < 30) return;			// < android R not supported
        final WindowInsetsController insetsController = getWindow().getInsetsController();
									// statusbar color always follows a hidden nav color
									// and vice-versa. light bars = dark text.
		int mask =	WindowInsetsController.APPEARANCE_LIGHT_NAVIGATION_BARS
				+	WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS;
		int toggle=0;
		if (darkText) toggle = mask;					// dark text uses mask else 0

		insetsController.setSystemBarsAppearance (toggle, mask);
	}//_setHideColors
//------------------------------------------------------
	public void setStatusBar (boolean show)				// set statusbar to default state
	{
		showStatusBar = show;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
		{
			if (show)
			{														// show status bar
				View decor = getWindow().getDecorView();
				int flags = decor.getSystemUiVisibility()
				&
				(
					~View.SYSTEM_UI_FLAG_FULLSCREEN				// Api 16-29
				&	~View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN		// Api 16-29
				);
				decor.setSystemUiVisibility (flags);
			}
			else
			{														// hide status bar
				View decor = getWindow().getDecorView();
				int flags = decor.getSystemUiVisibility()
				|
				(
					View.SYSTEM_UI_FLAG_FULLSCREEN				// Api 16-29
				|	View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN		// Api 16-29
				);
				decor.setSystemUiVisibility (flags);
			}
		}//_< Android 11
		else	// Android 11+
		{
	        final WindowInsetsController insetsController = getWindow().getInsetsController();

			if (insetsController == null) return;				// (should not happen)
			if (show)
				insetsController.show(WindowInsets.Type.statusBars()); // show status bar
			else
			{
				insetsController.hide(WindowInsets.Type.statusBars());	// hide status bar
				setHideColors ();								// dark text
			}
		}//_Android 11+
	}//_setStatusBar
//------------------------------------------------------
	private void setNavBar (boolean show)						// set navbar to default state
	{
		showNavBar = show;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) 
		{
			View decor = getWindow().getDecorView();
			int flags = 0;

			if (show)
			{
				flags = decor.getSystemUiVisibility()		// show
				&
				(
					~View.SYSTEM_UI_FLAG_HIDE_NAVIGATION	//Api 14
				&	~View.SYSTEM_UI_FLAG_IMMERSIVE			// Api 19
				);
			}//_show
			else
			{
				flags = decor.getSystemUiVisibility()			// hide
				|
				(
					View.SYSTEM_UI_FLAG_HIDE_NAVIGATION			//Api 14
				|	View.SYSTEM_UI_FLAG_IMMERSIVE				// Api 19
				);
			}//_hide
			decor.setSystemUiVisibility (flags);
		}//_< Android 11
		else	// Android 11+
		{
	        final WindowInsetsController insetsController = getWindow().getInsetsController();
			if (insetsController == null) return;				// (should not happen)

			if (show)
			{
				insetsController.show(WindowInsets.Type.navigationBars());	// show nav bars
			}//_show
			else												// hide nav bar
			{
				insetsController.hide(WindowInsets.Type.navigationBars());	// hide nav bars
//			if (Build.VERSION.SDK_INT < 31)						// swipe up to see
				insetsController.setSystemBarsBehavior(WindowInsetsController.BEHAVIOR_SHOW_BARS_BY_SWIPE); //Api 30 only
//				insetsController.setSystemBarsBehavior(WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
//			else												// ???
//				insetsController.setSystemBarsBehavior(WindowInsetsController.BEHAVIOR_DEFAULT); //Api 31
				setHideColors ();								// dark text
			}//_hide

//			if (!showStatusBar)			// if status bar is also hidden
//			{							// let's use up the empty area
//				getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//				getWindow().getAttributes().layoutInDisplayCutoutMode =
//					WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
//			}
		}//_Android 11+
	}//_setNavBar
//------------------------------------------------------
	public static void open_GRbars ()					// open the bars for a dialog
	{													// and save their previous state
		GR gr = (GR)(Activity) Basic.mContextMgr.getContext(ContextManager.ACTIVITY_GR);
		if (gr==null) return;
		saved_showStatusBar = showStatusBar;
		saved_showNavBar 	= showNavBar;

		if (!showStatusBar)	gr.setStatusBar(true);
		if (!showNavBar)	gr.setNavBar(true);
	}//_open_GRbars
//------------------------------------------------------
	public static void restore_GRbars ()				// restore bars to last saved state
	{
		GR gr = (GR)(Activity) Basic.mContextMgr.getContext(ContextManager.ACTIVITY_GR);
		if (gr==null) return;
		if (!saved_showStatusBar)	gr.setStatusBar(false);
		if (!saved_showNavBar)		gr.setNavBar(false);
	}//_restore_GRbars
//------------------------------------------------------
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.v(LOGTAG, "onCreate");
		super.onCreate(savedInstanceState);
		ContextManager cm = Basic.getContextManager();
		cm.registerContext(ContextManager.ACTIVITY_GR, this);
		cm.setCurrent(ContextManager.ACTIVITY_GR);

		Intent intent = getIntent();
		int flags = intent.getIntExtra(EXTRA_SHOW_DECORS, 1);
		showStatusBar		= (flags & 1)==1;				// -humpty 0409
		showActionBar		= (flags & 2)==2;
		showNavBar			= (flags & 4)==0;				// navbar inverts
		darkText			= (flags & 8)==8;
		int orientation		= intent.getIntExtra(EXTRA_ORIENTATION, -1);
		int backgroundColor = intent.getIntExtra(EXTRA_BACKGROUND_COLOR, 0xFF000000);

		setOrientation(orientation);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);

		scaleX = 1.0f;
		scaleY = 1.0f;
		Brightness = -1;

		drawView = new DrawView(this);

		if (showActionBar)							// show actionbar if wanted
		{
			getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
			mHandler = new Handler();
			grSetTitle(); grSetSubTitle();
			getActionBar().addOnMenuVisibilityListener(Run.menu_Listener());
			getActionBar().show();
		}

		setContentView(drawView);

		setStatusBar(showStatusBar);					// set statusbar to default state
		setNavBar 	(showNavBar);						// set navbar to default state
		if (!showStatusBar || !showNavBar) install_BarListener(); // listen for bar changes

		drawView.requestFocus();
		drawView.setBackgroundColor(backgroundColor);
		drawView.setId(33);

		synchronized (drawView) {
			mCreated = true;
			condReleaseLOCK();
		}

	}//_onCreate
//--------------------------------------------------
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{		// Called when the menu key is pressed.
		super.onCreateOptionsMenu(menu);
		if (!Settings.getConsoleMenu(this)) { return false; }

		Run.menu_copy (Run.UserMenu, menu);			// copy the Run menu
		return true;
	}//_onCreateOptionsMenu
//--------------------------------------------------
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) 	// Executed when Menu key is pressed (before onCreateOptionsMenu() above.
	{
		super.onPrepareOptionsMenu(menu);
		invalidateOptionsMenu();					// force an update
		Run.ItemSelected = false;
		return true;
	}//_onPrepareOptionsMenu
//--------------------------------------------------
	@Override
	public boolean onOptionsItemSelected(MenuItem item) // A menu item is selected
	{
		Menu rM = Run.mMenu;
		rM.performIdentifierAction(item.getItemId(), 0);	// activate Run item
		Run.ItemSelected = true;
		return true;
	}//_onOptionsItemSelected
//--------------------------------------------------
	@Override
	protected void onStart() {
		super.onStart();

		Log.v(LOGTAG, "onStart");
	}

	@Override
	protected void onResume() {
		Log.v(LOGTAG, "onResume " + this.toString());
		if (context != this) {
			Log.d(LOGTAG, "Context changed from " + context + " to " + this);
			context = this;
		}
		Basic.getContextManager().onResume(ContextManager.ACTIVITY_GR);
		Run.mEventList.add(new Run.EventHolder(GR_STATE, ON_RESUME, null));
		super.onResume();
		RunPaused = false;
	}

	@Override
	protected void onPause() {
		Log.v(LOGTAG, "onPause " + this.toString());
		Basic.getContextManager().onPause(ContextManager.ACTIVITY_GR);
		Run.mEventList.add(new Run.EventHolder(GR_STATE, ON_PAUSE, null));
		if (drawView.mKB != null) { drawView.mKB.forceHide(); }
		super.onPause();
		RunPaused = true;
	}

	protected void onStop() {
		Log.v(LOGTAG, "onStop");
		super.onStop();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.v(LOGTAG, "onRestart");
	}

	@Override
	public void finish() {
		// Tell the ContextManager we're done, if it doesn't already know.
		Basic.getContextManager().unregisterContext(ContextManager.ACTIVITY_GR, this);
		super.finish();
	}

	@Override
	protected void onDestroy() {
		Log.v(LOGTAG, "onDestroy " + this.toString());
		// if a new instance has started, don't let this one mess it up
		if (context == this) {
			Running = mCreated = false;
			context = null;
			releaseLOCK();								// don't leave GR.command hanging
		}
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		Run.mEventList.add(new Run.EventHolder(GR_BACK_KEY_PRESSED, 0, null));
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Log.v(LOGTAG, "keyDown " + keyCode);
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return super.onKeyDown(keyCode, event);
		}
		if (!Run.mBlockVolKeys && (
				(keyCode == KeyEvent.KEYCODE_VOLUME_UP)   ||
				(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) ||
				(keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) ||
				(keyCode == KeyEvent.KEYCODE_MUTE)        ||
				(keyCode == KeyEvent.KEYCODE_HEADSETHOOK) ))
		{
			return super.onKeyDown(keyCode, event);
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			// Do not put the KeyEvent on the EventList. This keeps Run.onKeyDown() from building a menu.
			Run.mEventList.add(new Run.EventHolder(KEY_DOWN, keyCode, null));
		}
		return true;									// ignore anything else
	}

	public boolean onKeyUp(int keyCode, KeyEvent event)  {						// The user hit a key
		// Log.v(LOGTAG, "keyUp " + keyCode);
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return super.onKeyUp(keyCode, event);
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			// Do not put the KeyEvent on the EventList. This keeps Run.onKeyDown() from building a menu.
			event = null;
		}
		Run.mEventList.add(new Run.EventHolder(KEY_UP, keyCode, event));
		return true;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
        case Run.REQUEST_CONNECT_DEVICE_SECURE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                connectDevice(data, true);
            }
            break;
        case Run.REQUEST_CONNECT_DEVICE_INSECURE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                connectDevice(data, Run.bt_Secure);
            }
            break;
        case Run.REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {
                // Bluetooth is now enabled, so set up a chat session
            	Run.bt_enabled = 1;
            } else {
                Run.bt_enabled = -1;
            }
            break;

		case Run.VOICE_RECOGNITION_REQUEST_CODE:
			if (resultCode == RESULT_OK) {
				Run.sttResults = new ArrayList<String>();
				Run.sttResults = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
			}
			Run.sttDone = true;
		}
	}

	private void condReleaseLOCK() {					// conditionally release LOCK
		if (mCreated & Running) { releaseLOCK(); }
	}

	private void releaseLOCK() {						// unconditionally release LOCK
		if (waitForLock) {
			synchronized (LOCK) {
				waitForLock = false;
				LOCK.notify();							// release GR.OPEN or .CLOSE if it is waiting
			}
		}
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	public void setOrientation(int orientation) {		// Convert and apply orientation setting
		Log.v(LOGTAG, "Set orientation " + orientation);
		switch (orientation) {
			default:
			case 1:  orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT; break;
			case 3:  orientation = (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD)
								 ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
								 : ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
				break;
			case 0:  orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE; break;
			case 2:  orientation = (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD)
								 ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
								 : ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
				break;
			case -1: orientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR; break;
		}
		setRequestedOrientation(orientation);
	}

    public void connectDevice(Intent data, boolean secure) {

        String address = data.getExtras()
            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        Run.btConnectDevice = null;
        try {
        	Run.btConnectDevice = Run.mBluetoothAdapter.getRemoteDevice(address);
	        if ( Run.btConnectDevice != null) Run.mChatService.connect(Run.btConnectDevice, secure);
        }
        catch (Exception e){ 
//        	RunTimeError("Connect error: " + e);
        }
        // Attempt to connect to the device
    }

    public void startsBTConnect() {
    	Intent serverIntent = null;
        serverIntent = new Intent(this, DeviceListActivity.class);
        if (serverIntent != null){
        	if (Run.bt_Secure) {
        		startActivityForResult(serverIntent, Run.REQUEST_CONNECT_DEVICE_SECURE);
        	}else {
        		startActivityForResult(serverIntent, Run.REQUEST_CONNECT_DEVICE_INSECURE);
        	}
        }

    }

    public void enableBT() {
        Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableIntent, Run.REQUEST_ENABLE_BT);
    }

	public class DrawView extends View {
		private static final String LOGTAG = "GR.DrawView";

		public KeyboardManager mKB;

		@SuppressLint("NewApi")
		public DrawView(Context context) {
			super(context);
			setFocusable(true);
			setFocusableInTouchMode(true);
			mKB = new KeyboardManager(context, this, new KeyboardManager.KeyboardChangeListener() {
				public void kbChanged() {
					Run.mEventList.add(new Run.EventHolder(GR_KB_CHANGED, 0, null));
				}
			});

//			if (Build.VERSION.SDK_INT >= 11) {				// Hardware acceleration is supported starting API 11
				// Assume hardware acceleration is enabled for the app.
				// Choose whether to use it in DrawView based on user Preference.
				int layerType = Settings.getGraphicAcceleration(context)
								? View.LAYER_TYPE_HARDWARE	// use hardware acceleration
								: View.LAYER_TYPE_SOFTWARE;	// disable hardware acceleration
				setLayerType(layerType, null);
//			}
		}

		synchronized public void setOrientation(int orientation) {	// synchronized orientation change
			Log.v(LOGTAG, "Set orientation " + orientation);
			GR.this.setOrientation(orientation);
		}

		@Override
		public boolean onKeyPreIme(int keyCode, KeyEvent event) {
			return (mKB != null) && mKB.onKeyPreIme(keyCode, event); // delegate to KeyboardManager
		}

		@SuppressWarnings("deprecation")
		@SuppressLint("NewApi")
		public int getWindowMetrics(Point outSize) {	// return size in Point, density as return value
			// This can be called when the DrawView does not yet know what size it is,
			// so get the size from the WindowManager.
			Display display = getWindowManager().getDefaultDisplay();
			if (Build.VERSION.SDK_INT < 13) {
				outSize.set(display.getWidth(), display.getHeight());
			} else {
				display.getSize(outSize);
			}
			DisplayMetrics dm = new DisplayMetrics();
			display.getMetrics(dm);
			return dm.densityDpi;
		}

		public boolean onTouchEvent(MotionEvent event) {

			super.onTouchEvent(event);

			if (SysBarInCall) return true;				// ignore system bar swipes

			int action = event.getAction() & MotionEvent.ACTION_MASK;	// Get action type, mask off index field
			int numPointers = event.getPointerCount();

			for (int i = 0; i < numPointers; i++) {
				int pid = event.getPointerId(i);
				if (pid > 1)  { continue; }				// currently, we allow only two pointers

				Run.TouchX[pid] = (double)event.getX(i);
				Run.TouchY[pid] = (double)event.getY(i);
				if (action == MotionEvent.ACTION_DOWN ||
					action == MotionEvent.ACTION_POINTER_DOWN) {
					Run.NewTouch[pid] = true;			// which pointer (0 or 1), cleared on UP
					Run.mEventList.add(new Run.EventHolder(GR_TOUCH, 0, null));
				}
				else if	(action == MotionEvent.ACTION_MOVE) {
					Run.NewTouch[pid] = true;
				} else if (action == MotionEvent.ACTION_UP ||
					action == MotionEvent.ACTION_POINTER_UP) {
					Run.NewTouch[pid] = false;
				}
			}
			return true;
		}

		public Paint newPaint(Paint fromPaint) {
			Typeface tf = fromPaint.getTypeface();
			Paint rPaint = new Paint(fromPaint);
			rPaint.setTypeface(tf);
			return rPaint;
		}

		@Override
		synchronized public void onDraw(Canvas canvas) {
			if (doEnableBT) {							// If this activity is running
				enableBT();								// Bluetooth must be enabled here
				doEnableBT = false;
			}

			if (startConnectBT) {
				startsBTConnect();
				startConnectBT = false;
			}

			if (doSTT) {
				Intent intent = Run.buildVoiceRecognitionIntent();
				startActivityForResult(intent, Run.VOICE_RECOGNITION_REQUEST_CODE);
				doSTT = false;
			}

			if ((Run.RealDisplayList == null) || (Run.DisplayList == null)) {
				Log.e(LOGTAG, "GR.onDraw: null DisplayList");
				finish();								// lost context, bail out
				return;
			}

			// float scale = getResources().getDisplayMetrics().density;
			drawView.setDrawingCacheEnabled(true);
			canvas.scale(scaleX, scaleY);

			if (Brightness != -1) {
				WindowManager.LayoutParams lp = getWindow().getAttributes();
				lp.screenBrightness = Brightness;
				getWindow().setAttributes(lp);
				Brightness = -1;						// do it only once
			}

			synchronized (Run.DisplayList) {
				for (int di : Run.RealDisplayList) {
					if (di >= Run.DisplayList.size()) continue;
					BDraw b = Run.DisplayList.get(di);
					if (!doDraw(canvas, b)) {
						finish();
						break;
					}
				}
				condReleaseLOCK();
			}
		} // onDraw()

		public boolean doDraw(Canvas canvas, BDraw b) {
			float fx1;
			float fy1;
			RectF rectf;

			Paint thePaint;
			Bitmap theBitmap;
//			canvas.scale(scaleX, scaleY);

			thePaint = null;
			Type type = Type.Null;
			if ((b != null) && b.isVisible()) {
				type = b.type();
				int pIndex = b.paint();
				if (Run.PaintList.size() == 0)						return true;
				if (pIndex < 1 || pIndex >= Run.PaintList.size())	return true;

				thePaint = Run.PaintList.get(pIndex);
				int alpha = b.alpha();
				if ((alpha < 256) && (alpha != thePaint.getAlpha())) {
					thePaint = newPaint(thePaint);
					thePaint.setAlpha(alpha);
				}
			}

			switch (type) {
				case Group:
				case Null:
					break;
				case Open:
					Running = true;						// flag for GR.Open
					break;
				case Close:
					Running = false;
					return false;

				case Circle:
					canvas.drawCircle(b.x(),b.y(),b.radius(), thePaint);
					break;
				case Rect:
					canvas.drawRect(b.left(), b.top(), b.right(), b.bottom(), thePaint);
					break;
				case ClipStart:
					canvas.save();						// -humpty 0261
					canvas.clipRect(b.left(), b.top(), b.right(), b.bottom(), b.clipOp());
					break;
				case Oval:
					rectf = new RectF(b.left(), b.top(), b.right(), b.bottom());
					canvas.drawOval(rectf, thePaint);
					break;
				case Arc:
					rectf = new RectF(b.left(), b.top(), b.right(), b.bottom());
					canvas.drawArc(rectf, b.arcStart(), b.arcSweep(), b.useCenter(), thePaint);
					break;
				case Line:
					canvas.drawLine(b.x1(),b.y1(),b.x2(),b.y2(), thePaint);
					break;
				case Point:
					canvas.drawPoint(b.x(),b.y(), thePaint);
					break;
				case SetPixels:
					fx1 = b.x();
					fy1 = b.y();
					Var.ArrayDef array = b.array();
					int pBase = b.arrayStart();
					int pLength = b.arraySublength();
					float[] pixels = new float[pLength];
					for (int j = 0; j < pLength; ++j) {
						pixels[j] = (float)array.nval(pBase + j) + fx1;
						++j;
						pixels[j] = (float)array.nval(pBase + j) + fy1;
					}
					canvas.drawPoints(pixels, thePaint);
					break;
				case Poly:
					ArrayList<Double> thisList = b.list();
					// User may have changed the list. If it has
					// an odd number of coordinates, ignore the last.
					int points = thisList.size() / 2;
					if (points >= 2) {					// do nothing if only one point
						fx1 = b.x();
						fy1 = b.y();
						Path path = new Path();
						Iterator<Double> listIt = thisList.iterator();
						float firstX = listIt.next().floatValue() + fx1;
						float firstY = listIt.next().floatValue() + fy1;
						path.moveTo(firstX, firstY);
						for (int p = 1; p < points; ++p) {
							float x = listIt.next().floatValue() + fx1;
							float y = listIt.next().floatValue() + fy1;
							path.lineTo(x, y);
						}
						path.lineTo(firstX, firstY);
						path.close();
						canvas.drawPath(path, thePaint);
					}
					break;
				case Text:
					canvas.drawText(b.text(), b.x(), b.y(), thePaint);
					break;
				case Bitmap:
					int bitmapIndex = b.bitmap();
					theBitmap = Run.BitmapList.get(bitmapIndex);
					if (theBitmap != null) {
						if (theBitmap.isRecycled()) {
							Run.BitmapList.set(bitmapIndex, null);
							theBitmap = null;
						}
					}
					if (theBitmap != null) {
						canvas.drawBitmap(theBitmap, b.x(), b.y(), thePaint);
					} else {
						NullBitMap = true;
					}
					break;
				case RotateStart:
					canvas.save();
					canvas.rotate(b.angle(), b.x1(), b.y1());
					break;
				case RotateEnd:
				case ClipEnd:			// -humpty 0261 (both rotate and clip share the same canvas stack (sorry folks)
					canvas.restore();
					break;
				default:
					break;
			}
			return true;
		} // doDraw()

	} // class DrawView
//--------------------------------------------
// ************************************** Colors & Utilities **************************************

	private static final	String colorNames[] = {"acidgreen","algaegreen","aliceblue","aliengray","aliengreen","aloeveragreen","antiquebronze","antiquewhite","aqua","aquamarine","aquamarinestone","armybrown","armygreen","ashgray","ashwhite","avocadogreen","aztechpurple","azure","azureblue","babyblue","babypink","bakersbrown","balloonblue","bananayellow","bashfulpink","basketballorange","battleshipgray","beanred","beer","beetlegreen","beeyellow","beige","bisque","black","blackbean","blackcat","blackcow","blackeel","blanchedalmond","blonde","bloodnight","bloodred","blossompink","blue","blueangel","blueberryblue","bluediamond","bluedress","blueeyes","bluegray","bluegreen","bluehosta","blueivy","bluejay","bluekoi","bluelagoon","bluelotus","bluemossgreen","blueorchid","blueribbon","blueturquoise","blueviolet","bluewhale","bluezircon","blushpink","blushred","boldyellow","brass","brightblue","brightcyan","brightgold","brightgreen","brightlilac","brightmaroon","brightnavyblue","brightneonpink","brightorange","brightpurple","brightturquoise","bronze","brown","brownbear","brownsand","brownsugar","bulletshell","burgundy","burlywood","burntpink","butterflyblue","cadetblue","cadillacpink","camelbrown","camouflagegreen","canaryblue","canaryyellow","cantaloupe","caramel","carbongray","carbonred","cardboardbrown","carnationpink","carrotorange","celeste","chameleongreen","champagne","charcoal","chartreuse","cherryred","chestnut","chestnutred","chillipepper","chocolate","chocolatebrown","cinnamon","clemantisviolet","cloudygray","clovergreen","cobaltblue","coffee","columbiablue","constructionconeorange","cookiebrown","copper","copperred","coral","coralblue","cornflowerblue","cornsilk","cornyellow","cotton","cottoncandy","cranberry","cream","crimson","crimsonpurple","crocuspurple","crystalblue","cyan","cyanopaque","darkbisque","darkblue","darkbluegrey","darkbrown","darkcarnationpink","darkcoffee","darkcyan","darkforestgreen","darkgoldenrod","darkgray","darkgreen","darkgrey","darkhotpink","darkkhaki","darklimegreen","darkmagenta","darkmint","darkmoccasin","darkolivegreen","darkorange","darkorchid","darkpink","darkpurple","darkred","darksalmon","darkseagreen","darksienna","darkslate","darkslateblue","darkslategray","darkslategrey","darkturquoise","darkviolet","darkyellow","dayskyblue","deepemeraldgreen","deepmauve","deeppeach","deeppink","deeppurple","deepred","deeprose","deepsea","deepseablue","deepseagreen","deepskyblue","deepteal","deepturquoise","deepyellow","deerbrown","denimblue","denimdarkblue","desertsand","dimgray","dimgrey","dimorphothecamagenta","dinosaurgreen","dodgerblue","dollarbillgreen","donutpink","dragongreen","dullgreenyellow","dullpurple","dullseagreen","earthblue","earthgreen","eggplant","eggshell","electricblue","emerald","emeraldgreen","fallforestgreen","fallleafbrown","ferngreen","ferrarired","firebrick","fireenginered","flamingopink","floralwhite","forestgreen","frenchlilac","froggreen","fuchsia","gainsboro","ghostwhite","gingerbrown","glacialblueice","gold","goldenbrown","goldenrod","goldensilk","goldenyellow","granite","grape","grapefruit","gray","graybrown","graycloud","graydolphin","graygoose","grayishturquoise","graywolf","green","greenapple","greenishblue","greenonion","greenpeas","greenpepper","greensnake","greenthumb","greenyellow","grey","gulfblue","gunmetal","halloweenorange","harvestgold","hazel","hazelgreen","heavenlyblue","heliotropepurple","honeydew","hotdeeppink","hotpink","hummingbirdgreen","iceberg","iguanagreen","indianred","indigo","iridium","irongray","ivory","jadegreen","jasminepurple","jeansblue","jellyfish","jetgray","junglegreen","kellygreen","khaki","khakirose","lapisblue","lavared","lavender","lavenderblue","lavenderblush","lavenderpinocchio","lavenderpurple","lawngreen","lemonchiffon","lightaquamarine","lightblue","lightbrown","lightcopper","lightcoral","lightcyan","lightdayblue","lightfrenchbeige","lightgoldenrodyellow","lightgray","lightgreen","lightgrey","lightjade","lightorange","lightpink","lightpurple","lightpurpleblue","lightred","lightrose","lightrosegreen","lightsalmon","lightsalmonrose","lightseagreen","lightskyblue","lightslate","lightslateblue","lightslategray","lightslategrey","lightsteelblue","lightwhite","lightyellow","lilac","lime","limegreen","limemintgreen","linen","lipstickpink","lovelypurple","lovered","macaroniandcheese","macawbluegreen","magenta","magicmint","mahogany","mangoorange","marbleblue","maroon","mauve","mauvetaupe","mediumaquamarine","mediumblue","mediumforestgreen","mediumorchid","mediumpurple","mediumseagreen","mediumslateblue","mediumspringgreen","mediumteal","mediumturquoise","mediumvioletred","metallicgold","metallicsilver","middayblue","midnight","midnightblue","milkchocolate","milkwhite","mint","mintcream","mintgreen","mistblue","mistyrose","moccasin","mocha","mustardyellow","navajowhite","navy","nebulagreen","neonblue","neongreen","neonorange","neonpink","newmidnightblue","night","nightblue","northernlightsblue","oakbrown","oceanblue","offwhite","oil","oldburgundy","oldlace","olive","olivedrab","olivegreen","orange","orangegold","orangered","orangesalmon","orangeyellow","orchid","orchidpurple","organicbrown","palebluelily","palegoldenrod","palegreen","palelilac","palesilver","paleturquoise","palevioletred","papayaorange","papayawhip","parchment","parrotgreen","pastelblue","pastelgreen","pastellightblue","pastelorange","pastelpink","pastelpurple","pastelred","pastelviolet","pastelyellow","peach","peachpuff","peagreen","pearl","periwinkle","peru","pigpink","pinegreen","pink","pinkbrown","pinkbubblegum","pinkcoral","pinkcupcake","pinkdaisy","pinklemonade","pinkplum","pinkrose","pinkviolet","pistachiogreen","platinum","platinumgray","platinumsilver","plum","plumpie","plumpurple","plumvelvet","powderblue","puce","pumpkinorange","purple","purpleamethyst","purpledaffodil","purpledragon","purpleflower","purplehaze","purpleiris","purplejam","purplelily","purplemaroon","purplemimosa","purplemonster","purplenavy","purplepink","purpleplum","purplesagebush","purplethistle","purpleviolet","ratgray","rebeccapurple","red","redblood","reddirt","redfox","redwine","rice","richlilac","robineggblue","roguepink","romansilver","rose","rosedust","rosegold","rosered","rosybrown","rosyfinch","rosypink","royalblue","rubberduckyyellow","rubyred","rust","saddlebrown","saffron","sage","sagegreen","saladgreen","salmon","sand","sandstone","sandybrown","sangria","sapphireblue","scarlet","schoolbusyellow","seablue","seafoamgreen","seagreen","seashell","seaturtlegreen","seaweedgreen","sedona","sepia","shamrockgreen","shockingorange","sienna","silkblue","silver","silverpink","skyblue","skybluedress","slateblue","slatebluegrey","slategranitegray","slategray","slategrey","slimegreen","smokeygray","snow","softivory","sonicsilver","springgreen","steelblue","stoplightgogreen","sunriseorange","sunyellow","tan","tanbrown","tangerine","taupe","teagreen","teal","thistle","tiffanyblue","tigerorange","tomato","tronblue","tulippink","turquoise","tyrianpurple","unbleachedsilk","valentinered","vampiregray","vanilla","velvetmaroon","venomgreen","violapurple","violet","violetred","water","watermelonpink","wheat","white","whitechocolate","whitesmoke","windowsblue","winered","wisteriapurple","wood","yellow","yellowgreen","yellowgreengrosbeak","yellowlawngreen","yelloworange","zombiegreen"};
	private static final	int colorInts[] = {11583258,6613382,15792383,7565166,7128087,10024214,6708510,16444375,65535,8388564,3442561,8551264,4936480,6710114,15328468,11715144,8993791,15794175,4744096,9812423,16428986,6042391,2842846,16114198,12735107,16286040,8684674,16211289,16494871,5011582,15313687,16119260,16770244,0,4000770,4274233,4998726,4603455,16772045,16512729,5576198,8271127,16365567,255,12046060,16834,5169900,1408492,1403335,10006471,8113333,7847879,3182791,2839678,6659783,9366508,6906092,3954267,2049532,3174143,4441819,9055202,3419518,5766911,15116780,15036052,16374564,11904578,592383,720895,16633879,6749952,14193135,12788040,1668306,16004095,16736031,6950317,1499893,13467442,10824234,8608827,15637069,14853999,11508576,9175066,14596231,12657255,3714284,6266528,14912174,12687979,7898731,2692853,16772864,16754223,13012503,6446429,10947882,15587956,16218273,16285719,5303276,12449046,16246734,3418156,8388352,12731969,9782581,12798508,12655383,13789470,4128783,12945687,8662478,7170408,4104277,8386,7294519,8892359,16282673,13083415,12088115,13331793,16744272,11525356,6591981,16775388,16774016,16514041,16572415,10420239,16777164,14423100,14825708,9532140,6075391,65535,9619399,12084480,139,2704987,6636321,12657283,3878703,35723,2441495,12092939,11119017,25600,11119017,16146603,12433259,4301591,9109643,3248238,8550457,5597999,16747520,10040012,15160448,4915536,9109504,15308410,9419919,9060631,2832470,4734347,2439228,2439228,52945,9699539,9142272,8571647,287495,14644180,16763812,16716947,3539263,8389911,16497593,3906716,1193046,3172180,49151,212542,4771021,16170496,15122307,7977708,1383309,15583663,6908265,6908265,14889373,7577964,2003199,8764261,16428990,7011218,11664151,8344157,5146997,165,3450223,6373457,16775651,10157823,5294200,6290199,5149272,13153632,6716454,16190746,11674146,16132119,16361392,16775920,2263842,8806542,10077838,16711935,14474460,16316671,13221474,3574721,16766720,15384855,14329120,15983555,16768768,8617596,6183552,14432287,8421504,4011573,11974324,6051928,13750478,6192510,5261899,32768,5030935,3177854,6988065,9028444,4890668,7125820,11922090,11403055,8421504,13230060,2897209,15100972,15590005,9336344,6388824,13033215,13918975,15794160,16066695,16738740,8382487,5678572,10268785,13458524,4915330,4013114,5396829,16777200,6224750,10632172,10538988,4638663,6385022,3439660,5031250,15787660,12947598,1388926,14950935,15132410,14935290,16773365,15457762,9862070,8190976,16775885,9699304,11393254,11887901,14322279,15761536,14745599,11395071,13151615,16448210,13882323,9498256,13882323,12844472,16701617,16758465,8677335,7507918,16764107,16502733,14416347,16752762,16356971,2142890,8900346,13434879,7564031,7833753,7833753,11587550,16777207,16777184,13148872,65280,3329330,3601791,16445670,12879763,8337644,14949143,15907686,4440007,16711935,11202769,12599296,16744512,5664126,8388608,14725375,9527149,6737322,205,3437109,12211667,9662683,3978097,8087790,64154,286559,4772300,13047173,13938487,12371660,3914239,2824983,1644912,5323548,16710911,4109449,16121850,10026904,6581630,16770273,16770229,4799782,16767832,16768685,128,5892119,1411583,1504553,16738048,16070058,160,788746,1383252,7915463,8414487,2844140,16314595,3879217,4403246,16643558,8421376,7048739,12236908,16753920,13934615,16729344,12874833,16756290,14315734,11552949,14940582,13626604,15657130,10025880,14471423,13222075,11529966,14381203,15034135,16773077,16777154,1223979,11849708,7855479,14014186,16300171,16688042,15901416,16151168,13799868,16447620,16770484,16767673,5427223,16641780,15323116,13468991,16635876,3701828,16761035,12878217,16768989,15168625,14966429,15178147,14952572,12139407,15180208,13247083,10338825,15066338,7960953,13553358,14524637,8193345,5781337,8193362,11591910,8346200,16282135,8388736,7089607,11551231,12816071,10963655,5126270,5708670,6957182,5573173,8455489,10386431,4594558,5132672,13723015,9319919,8019399,13810131,9255113,7175053,6697881,16711680,6684672,8344087,12802071,10027026,16446959,11953874,12447231,12658793,8620438,15248810,10055792,15517120,12721750,12357519,8343122,11764865,4286945,16766977,16130583,12804673,9127187,16496919,12368010,8686457,10602805,16416882,12759680,7892319,16032864,8271895,2446535,16720896,15246103,12771327,4106655,3050327,16774638,4427136,4422679,13395456,8341036,3439639,15031100,10506797,4754119,12632256,12889773,8900331,6723839,6970061,7568545,6648707,7372944,7372944,12380500,7499373,16775930,16445661,7697781,65407,4620980,5761380,15103057,16771196,13808780,15525302,15174241,4734002,13433693,32896,14204888,8509648,13140289,16737095,8257022,12737148,4251856,12868332,16768458,15029329,5656657,15984043,8271181,7506944,8280190,15631086,16135562,15463674,16542853,16113331,16777215,15591126,16119285,3505863,10027026,13020871,9858867,16776960,10145074,14873878,8910615,16756290,5555569};
	private static final	int csize = colorNames.length;

//--------------------------------------------
//	public static Integer findColor (String key)	// search for a color
//	{
//		int i =	bSearch (colorNames, key);
//		if (i > -1) return (Integer)colorInts[i];	// found
//		return null;								// not found
//	}//_findColor
//--------------------------------------------
	public static Integer findColor (String s)	// find a color string
											// 		string "#RRGGBB"
											// or	string "#AARRGGBB"
											// or 	string "color_name {<.|#>alpha>}"
		// not supported yet -->				// or   string ".r.g.b"
											// return null if error
	{
		int alpha = 0xFF000000;				// default solid
		int len = s.length();
		int found = 0, i = 0;
		String num = "";
		boolean err = false;

		if (len < 1) return null;
		if (s.charAt(0) == '#')				// -humpty 0330 if this is a hex color
		{
			try	{ found = Color.parseColor(s);}
			catch (IllegalArgumentException ex) {err=true;}
			if (err) return null;			// hex color not found
			return (Integer) found;			// found hex color
		}
											// remove any leading underscore
		if (s.charAt(0) == '_') { s = s.substring(1); len--; }
		if (len < 1) return null;

		if ( (i=s.lastIndexOf('#')) > 0)	// hex aplha ?
		{
			num = s.substring(i+1);
			try	{alpha = Integer.parseInt (num,16);}
				catch (NumberFormatException ex) {return null;}
			alpha <<= 24;
			s = s.substring(0,i);			// cut off aplha
		}   
		else 
		if ( (i=s.lastIndexOf('.')) > 0)	// dec alpha ?
		{
			num = s.substring(i+1);
			try	{alpha = Integer.parseInt (num);}
				catch (NumberFormatException ex) {return null;}
			alpha <<= 24;
			s = s.substring(0,i);			// cut off aplha
		}

		i =	Basic.bSearch (colorNames, s);
		if (i<0) return null;				// not found

		found = alpha | colorInts[i];
		return (Integer) found;
	}//_findColor
}//_GR
