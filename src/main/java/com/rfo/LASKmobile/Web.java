/****************************************************************************************************

BASIC! is an implementation of the Basic programming language for
Android devices.

Copyright (C) 2010 - 2016 Paul Laughton

This file is part of BASIC! for Android

    BASIC! is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BASIC! is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BASIC!.  If not, see <http://www.gnu.org/licenses/>.

    You may contact the author or current maintainers at http://rfobasic.freeforums.org

*************************************************************************************************/

package com.rfo.LASKmobile;

import static com.rfo.LASKmobile.Run.EventHolder.*;

import java.net.URLDecoder;
import java.util.ArrayList;

import org.apache.http.util.EncodingUtils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowInsets;				// -humpty 0415
import android.view.WindowInsets.Type;
import android.view.WindowInsetsController;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.graphics.drawable.ColorDrawable;
import android.text.style.ForegroundColorSpan;
import android.text.Spanned;
import android.text.SpannableString;

public class Web extends Activity {
	//Log.v(LOGTAG, "Line Buffer " + ExecutingLineBuffer);
	private static final String LOGTAG = "Web";

	public static final String EXTRA_SHOW_DECORS = "decors";
	public static final String EXTRA_ORIENTATION = "orientation";

	public static Context mContext = null;
	public static TheWebView aWebView = null;
	private WebView engine;

	private static boolean showStatusBar = true;
	private static boolean showNavBar = true;
	private static boolean saved_showStatusBar = true;
	private static boolean saved_showNavBar = true;

	private	boolean showActionBar =	false;
	private boolean SysBarInCall = false;
	private View.OnSystemUiVisibilityChangeListener sysBarListener1 = null;
	private View.OnApplyWindowInsetsListener sysBarListener2 = null;
	private boolean restoringBars = false;
	private boolean darkText = false;						// dark text for hidden status/nav bars

	public static String TitleText = "Html";				// actionbar title text
	public static Integer TitleTextColor = null;			// actionbar text color
	public static Integer TitleBackColor = null;			// actionbar bg color
	public static String TitleSubText = "";					// subtitle text
	public static Integer TitleSubColor = null;				// subtitle text color

	public static Handler mHandler;							// GR post handler
	//******************************** Intercept BACK Key *************************************

	@Override
	public void onBackPressed() {
		if (engine.canGoBack()) {						// if can go back then do it
			addData("BAK", "1");						// tell user the back key was hit
		} else {
			addData("BAK", "0");						// tell user the back key was hit
			super.onBackPressed();						// done
		}
	}
	//******************************** helpers *************************************
	public void htSetTitle()				// -humpty 0415 change actionbar title and colors
	{
		if (!showActionBar) return;			// do nothing if no actionbar
		Runnable r = new Runnable()
		{
			@Override
		    public void run()
			{
				if (TitleBackColor!=null)
					getActionBar().setBackgroundDrawable(new ColorDrawable(TitleBackColor));

				SpannableString ss = new SpannableString(TitleText);
				if (TitleTextColor!=null)
					ss.setSpan(new ForegroundColorSpan(TitleTextColor), 0, TitleText.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
				getActionBar().setTitle(ss);
			}
		};
		mHandler.post(r);
	}//_htSetTitle
//------------------------------------------------------
	public void htSetSubTitle()				// -humpty 0426 change sub title and/or color
	{
		if (!showActionBar) return;			// do nothing if no actionbar
		Runnable r = new Runnable()
		{
			@Override
		    public void run()
			{
				SpannableString ss = new SpannableString(TitleSubText);
				if (TitleSubColor!=null)
					ss.setSpan(new ForegroundColorSpan(TitleSubColor), 0, TitleSubText.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
				getActionBar().setSubtitle(ss);
			}
		};
		mHandler.post(r);
	}//_htSetSubTitle
//------------------------------------------------------
	private void restoreBars()						// restore bars after a swipe
	{
		if (restoringBars)  return;					// don't overlap
		restoringBars = true;

		Handler handler = new Handler();
		Runnable r = new Runnable()
		{
			@Override
		    public void run()
			{
				setStatusBar(showStatusBar);	// restore statusbar to default state
				setNavBar (showNavBar);			// restore navbar to default state
				restoringBars = false;	// done
			}
		};
		handler.postDelayed(r, 3000);
	}//_restoreBars()
//------------------------------------------------------
	private void install_BarListener()							// listen for bar changes
	{
			final View decor = getWindow().getDecorView();

			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
			{
				if (sysBarListener1 != null) return;			// don't install twice
				sysBarListener1 = new View.OnSystemUiVisibilityChangeListener()
				{
					@Override
					public void onSystemUiVisibilityChange(int visibility)
					{
						SysBarInCall =
						(!showStatusBar && ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN)==0))
						||
						(!showNavBar && ((visibility & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)==0))
						;
						if (SysBarInCall) restoreBars();
					}
				};//_sysBarListener1
				decor.setOnSystemUiVisibilityChangeListener(sysBarListener1);
			}
			else	// Android 11+
			{
				if (sysBarListener2 != null) return;			// don't install twice
				sysBarListener2 = new View.OnApplyWindowInsetsListener() 
				{
					@Override
					public WindowInsets onApplyWindowInsets(View v, WindowInsets insets)
					{
						SysBarInCall =
						(!showStatusBar && insets.isVisible(WindowInsets.Type.statusBars()))
						||
						(!showNavBar && insets.isVisible(WindowInsets.Type.navigationBars()))
						;
						if (SysBarInCall) restoreBars();
						return insets;
					}//_onApply
				};//_navBarListener2
				decor.setOnApplyWindowInsetsListener (sysBarListener2);
			}//_Android 11+
	}//_install_BarListener
//------------------------------------------------------
	private void setHideColors ()						// set text color for hidden bars API 30+
	{
		if (Build.VERSION.SDK_INT < 30) return;			// < android R not supported
        final WindowInsetsController insetsController = getWindow().getInsetsController();
									// statusbar color always follows a hidden nav color
									// and vice-versa. light bars = dark text.
		int mask =	WindowInsetsController.APPEARANCE_LIGHT_NAVIGATION_BARS
				+	WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS;
		int toggle=0;
		if (darkText) toggle = mask;					// dark text uses mask else 0

		insetsController.setSystemBarsAppearance (toggle, mask);
	}//_setHideColors
//------------------------------------------------------
	public void setStatusBar (boolean show)				// set statusbar to default state
	{
		showStatusBar = show;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
		{
			if (show)
			{														// show status bar
				View decor = getWindow().getDecorView();
				int flags = decor.getSystemUiVisibility()
				&
				(
					~View.SYSTEM_UI_FLAG_FULLSCREEN				// Api 16-29
				&	~View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN		// Api 16-29
				);
				decor.setSystemUiVisibility (flags);
			}
			else
			{														// hide status bar
				View decor = getWindow().getDecorView();
				int flags = decor.getSystemUiVisibility()
				|
				(
					View.SYSTEM_UI_FLAG_FULLSCREEN				// Api 16-29
				|	View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN		// Api 16-29
				);
				decor.setSystemUiVisibility (flags);
			}
		}//_< Android 11
		else	// Android 11+
		{
	        final WindowInsetsController insetsController = getWindow().getInsetsController();

			if (insetsController == null) return;				// (should not happen)
			if (show)
				insetsController.show(WindowInsets.Type.statusBars()); // show status bar
			else
			{
				insetsController.hide(WindowInsets.Type.statusBars());	// hide status bar
				setHideColors ();								// dark text
			}
		}//_Android 11+
	}//_setStatusBar
//------------------------------------------------------
	private void setNavBar (boolean show)						// set navbar to default state
	{
		showNavBar = show;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) 
		{
			View decor = getWindow().getDecorView();
			int flags = 0;

			if (show)
			{
				flags = decor.getSystemUiVisibility()		// show
				&
				(
					~View.SYSTEM_UI_FLAG_HIDE_NAVIGATION	//Api 14
				&	~View.SYSTEM_UI_FLAG_IMMERSIVE			// Api 19
				);
			}//_show
			else
			{
				flags = decor.getSystemUiVisibility()			// hide
				|
				(
					View.SYSTEM_UI_FLAG_HIDE_NAVIGATION			//Api 14
				|	View.SYSTEM_UI_FLAG_IMMERSIVE				// Api 19
				);
			}//_hide
			decor.setSystemUiVisibility (flags);
		}//_< Android 11
		else	// Android 11+
		{
	        final WindowInsetsController insetsController = getWindow().getInsetsController();
			if (insetsController == null) return;				// (should not happen)

			if (show)
			{
				insetsController.show(WindowInsets.Type.navigationBars());	// show nav bars
			}//_show
			else												// hide nav bar
			{
				insetsController.hide(WindowInsets.Type.navigationBars());	// hide nav bars
//			if (Build.VERSION.SDK_INT < 31)						// swipe up to see
				insetsController.setSystemBarsBehavior(WindowInsetsController.BEHAVIOR_SHOW_BARS_BY_SWIPE); //Api 30 only
//				insetsController.setSystemBarsBehavior(WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE);
//			else												// ???
//				insetsController.setSystemBarsBehavior(WindowInsetsController.BEHAVIOR_DEFAULT); //Api 31
				setHideColors ();								// dark text
			}//_hide

//			if (!showStatusBar)			// if status bar is also hidden
//			{							// let's use up the empty area
//				getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//				getWindow().getAttributes().layoutInDisplayCutoutMode =
//					WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
//			}
		}//_Android 11+
	}//_setNavBar
//------------------------------------------------------
	public static void open_webBars ()					// open the bars for a dialog
	{													// and save their previous state
		Web wb = (Web)(Activity) Basic.mContextMgr.getContext(ContextManager.ACTIVITY_WEB);
		if (wb==null) return;
		saved_showStatusBar = showStatusBar;
		saved_showNavBar 	= showNavBar;

		if (!showStatusBar)	wb.setStatusBar(true);
		if (!showNavBar)	wb.setNavBar(true);
	}//_open_webBars
//------------------------------------------------------
	public static void restore_webBars ()				// restore bars to last saved state
	{
		Web wb = (Web)(Activity) Basic.mContextMgr.getContext(ContextManager.ACTIVITY_WEB);
		if (wb==null) return;
		if (!saved_showStatusBar)	wb.setStatusBar(false);
		if (!saved_showNavBar)		wb.setNavBar(false);
	}//_restore_webBars

	// ************************* Class startup Method ****************************

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.v(LOGTAG, "onCreate");
		super.onCreate(savedInstanceState);
		ContextManager cm = Basic.getContextManager();
		cm.registerContext(ContextManager.ACTIVITY_WEB, this);
		cm.setCurrent(ContextManager.ACTIVITY_WEB);


		Intent intent = getIntent();
		int flags = intent.getIntExtra(EXTRA_SHOW_DECORS, 1);
		showStatusBar		= (flags & 1)==1;				// -humpty 0415
		showActionBar		= (flags & 2)==2;
		showNavBar			= (flags & 4)==0;				// navbar inverts
		darkText			= (flags & 8)==8;

		int orientation = intent.getIntExtra(EXTRA_ORIENTATION, -1);
		setOrientation(orientation);

		if (showActionBar)							// show actionbar if wanted
		{
			getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
			mHandler = new Handler();
			htSetTitle(); htSetSubTitle();
			getActionBar().addOnMenuVisibilityListener(Run.menu_Listener());
			getActionBar().show();
		}

		setContentView(R.layout.web);

		setStatusBar(showStatusBar);						// -humpty 0415
		if (!showStatusBar || !showNavBar) install_BarListener(); // listen for bar changes

		View v = findViewById(R.id.web_engine);

		engine = (WebView)  v;

		WebSettings webSettings = engine.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setSupportZoom(true);
		webSettings.setAppCacheEnabled(true);
		webSettings.setDatabaseEnabled(true);
		webSettings.setDomStorageEnabled(true);
		webSettings.setAllowFileAccess(true);
		webSettings.setGeolocationEnabled(true);
		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

		engine.addJavascriptInterface(new JavaScriptInterface(), "Android");

		engine.setWebViewClient(new MyWebViewClient());

		aWebView = new TheWebView(this);

		engine.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
				//Required functionality here
				return super.onJsAlert(view, url, message, result);
			}
			// The 2 following methods allow to play fullscreen HTML5 videos:
			Dialog d;
			@Override
			public void onShowCustomView (View v, final CustomViewCallback c) {
				d = new Dialog (Basic.getContextManager().getContext(), 
						android.R.style.Theme_Black_NoTitleBar_Fullscreen);
				v.setBackgroundColor(getResources().getColor(android.R.color.black));
				d.setContentView(v);
				d.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss (DialogInterface d) {
						c.onCustomViewHidden();
						onHideCustomView();
					}
				});
				d.show();
			}
			@Override
			public void onHideCustomView () {
				d.hide();
				super.onHideCustomView();
			}
		});

	}
//--------------------------------------------------
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{		// Called when the menu key is pressed.
		super.onCreateOptionsMenu(menu);
		if (!Settings.getConsoleMenu(this)) { return false; }

		Run.menu_copy (Run.UserMenu, menu);			// copy the Run menu
		return true;
	}//_onCreateOptionsMenu
//--------------------------------------------------
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) 	// Executed when Menu key is pressed (before onCreateOptionsMenu() above.
	{
		super.onPrepareOptionsMenu(menu);
		invalidateOptionsMenu();					// force an update
		Run.ItemSelected = false;
		return true;
	}//_onPrepareOptionsMenu
//--------------------------------------------------
	@Override
	public boolean onOptionsItemSelected(MenuItem item) // A menu item is selected
	{
		Menu rM = Run.mMenu;
		rM.performIdentifierAction(item.getItemId(), 0);	// activate Run item
		Run.ItemSelected = true;
		return true;
	}//_onOptionsItemSelected
//--------------------------------------------------
	@Override
	protected void onResume() {
		Log.v(LOGTAG, "onResume " + this);
		Basic.getContextManager().onResume(ContextManager.ACTIVITY_WEB);
		Run.mEventList.add(new Run.EventHolder(WEB_STATE, ON_RESUME, null));
		mContext = this;
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.v(LOGTAG, "onPause");
		Basic.getContextManager().onPause(ContextManager.ACTIVITY_WEB);
		Run.mEventList.add(new Run.EventHolder(WEB_STATE, ON_PAUSE, null));
		mContext = null;
		super.onPause();
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	private void setOrientation(int orientation) {	// Convert and apply orientation setting
		switch (orientation) {
			default:
			case 1:  orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT; break;
			case 3:  orientation = (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD)
								 ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
								 : ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
				break;
			case 0:  orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE; break;
			case 2:  orientation = (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD)
								 ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
								 : ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
				break;
			case -1: orientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR; break;
		}
		setRequestedOrientation(orientation);
	}

	//************************** Local method to put data into the Run read data link queue

	public void addData(String type, String data) {
		String theData = type + ":" + data;
		Run.mEventList.add(new Run.EventHolder(DATALINK_ADD, theData));
	}

	//*****************************************  WebView Client Interface *****************************

	private class MyWebViewClient extends WebViewClient implements DownloadListener{
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			addData("LNK", url);
			view.setDownloadListener(this);
			return true;
		}

		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			if (failingUrl.contains("#")) { // Workaround to load a file if there is a hash appended to the file path
				Log.v("LOG", "failing url:" + failingUrl);
				String[] temp;
				temp = failingUrl.split("#");
				view.loadUrl(temp[0]); // load page without internal link

				try {
					Thread.sleep(400);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				view.loadUrl(failingUrl); // try again
			} else {
				int index = failingUrl.indexOf("FORM?");
				if (index == -1) { addData("ERR", errorCode + " " + description + failingUrl); }
			}
		}

		public void onLoadResource (WebView view, String url){
			int index = url.indexOf("FORM?");
			if (index != -1){
				String d = "&"+ URLDecoder.decode(url.substring(index+5));
				addData("FOR", d );
//				finish();
			}
		}

		public void onDownloadStart (String url, String agent, String disposition,
										String mimetype, long size) {
			addData("DNL", url);
		}
	}

	@Override
	protected void onStop() {
		//	aWebView = null; // otherwise html.load does not work after a return to BASIC! Thanks to LUCA! !! 2013-10-11 gt
		Log.v(Web.LOGTAG, "onStop ");
		super.onStop();
	}

	@Override
	public void finish() {
		// Tell the ContextManager we're done, if it doesn't already know.
		Basic.getContextManager().unregisterContext(ContextManager.ACTIVITY_WEB, this);
		super.finish();
	}

	@Override
	protected void onDestroy() {
		aWebView = null;
		Log.v(Web.LOGTAG, "onDestroy ");
		if (engine != null) engine.destroy();
		super.onDestroy();
	}

	// **************************  Methods called from Run.java ****************************

	public class TheWebView {

		public TheWebView(Context context) {
		}

		public void setOrientation(int orientation) {
			Web.this.setOrientation(orientation);
		}

		public void webLoadUrl(String URL) {
			if (engine != null)engine.loadUrl(URL);
		}

		public void webLoadString(String baseURL, String data) {
			if (engine == null) return;
			engine.loadDataWithBaseURL(baseURL, data, "text/html", "UTF-8", baseURL + "*");
		}

		public void webClose() {
			engine = null;
			finish();
		}

		public void goBack() {
			if ((engine != null) && engine.canGoBack()) { engine.goBack(); }
		}

		public void goForward(){
			if ((engine != null) && engine.canGoForward()) { engine.goForward(); }
		}

		public void clearCache() {
			if (engine != null) { engine.clearCache(true); }
		}

		public void clearHistory() {
			if (engine != null) { engine.clearHistory(); }
		}

		public void webPost(String URL, String htmlPostString) {
			engine.postUrl(URL, EncodingUtils.getBytes(htmlPostString, "BASE64"));
		}
	}

	//******************************** Intercept dataLink calls ***********************

	public class JavaScriptInterface {

		@JavascriptInterface
		public void dataLink(String data) {
			if (data.equals("STT")) {
				Intent intent = Run.buildVoiceRecognitionIntent();
				Run.sttListening = true;
				Run.sttDone = false;
				startActivityForResult(intent, Run.VOICE_RECOGNITION_REQUEST_CODE);
			}
			addData("DAT", data);
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case Run.VOICE_RECOGNITION_REQUEST_CODE:
			if (resultCode == RESULT_OK) {
				Run.sttResults = new ArrayList<String>();
				Run.sttResults = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
			}
			Run.sttDone = true;
		}
	}
}
