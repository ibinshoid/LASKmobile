/****************************************************************************************************

BASIC! is an implementation of the Basic programming language for
Android devices.

Copyright (C) 2010 - 2015 Paul Laughton

This file is part of BASIC! for Android

    BASIC! is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BASIC! is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BASIC!.  If not, see <http://www.gnu.org/licenses/>.

    You may contact the author or current maintainers at http://rfobasic.freeforums.org

*************************************************************************************************/

package com.rfo.LASKmobile;

import java.util.ArrayList;

import android.app.Activity;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.InputType;

import android.view.View;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.LayoutInflater;
import android.view.inputmethod.EditorInfo;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.view.Gravity;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import android.content.Context;
import android.util.Log;

public class TGet										// -humpty 0310 TGet overhaul
{														// use existing console instead of activity

	public static boolean in_TGET = false;		// are we in TGET input ?
	public static boolean in_recycle = false;	// are we recyling this view ?
	public static View group=null;				// group
	public static TextView label=null;			// label
	public static EditText et=null;				// we carry the view here

	private static int PromptIndex;
	private static String Prompt;

	private static String undoText  = "";		// undo buffer for illegal changes
	private static int    undoCurs  = 0;		// undo cursor for illegal position
	private static int    undoChar  = ' ';		// undo char for illegal backspace
	private static boolean undoFlag = false;

	private static TextWatcher				inputTextWatcher 	= null;
	private static OnEditorActionListener	inputActionListener = null;
	private static View.OnClickListener		inputClickListener	= null;
	private static OnKeyListener			inputKeyListener	= null;

	final static Context cx = Basic.mContextMgr.getContext(ContextManager.ACTIVITY_APP);
	final static InputMethodManager imm = (InputMethodManager)
								cx.getSystemService(Context.INPUT_METHOD_SERVICE);
//------------------------------------------
	public static void create_Listeners ()	// create listeners
	{
											// Listen for text changes
		if (inputTextWatcher == null) inputTextWatcher =
		new TextWatcher()
		{
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
				if (!in_TGET) return;
				if (undoFlag) return;				// undo not finished, ignore for now
		
				undoText = s.toString();		// copy the text that was there before
				undoCurs = getCursorPos();		// also save the cursor position
			}
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (!in_TGET) return;
			}
			public void afterTextChanged(Editable s)
			{
				if (!in_TGET) return;
				if (undoFlag) return;				// not finished undo, ignore

				if	(	!s.toString().startsWith(Prompt)	// if prompt was changed
					||	badCursorPos()						// or cursor before prompt
					)
				{									// then it's illegal
					undoFlag = true;
//					Basic.toaster(cx, "Undo ing");	// debug
					et.setText(undoText);			// restore to what was before
					putCursorAt(undoCurs);			// restore cursor position
					if (badCursorPos()) putCursorAtEnd();
					undoFlag = false;
				}//_if something corrupted
			}
		};//_inputTextWatcher
//-------------------------
										// Listen for Enter events
		if (inputActionListener == null) inputActionListener =
		new OnEditorActionListener()
		{
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				if (!in_TGET) return false;

//				Basic.toaster(cx, "Action:"+String.valueOf(actionId));	// debug

				if (event==null)			// null events are probably from soft keyboard
				{							// Capture all Soft-Enter in a EditText
					if	(  (actionId == EditorInfo.IME_ACTION_DONE)
						 ||(actionId == EditorInfo.IME_ACTION_NEXT)
						)
					{
						handleEOL();
						return true;
					}
				}//_if event null
				else											 // not null events are
				if (actionId==EditorInfo.IME_ACTION_UNSPECIFIED) // other enter keys
				{
					// Capture other Soft & Hard Enters in multi-line EditTexts
					if (event.getAction() == KeyEvent.ACTION_DOWN)
					{
						return true;				// ignore when key pressed.
					}
					else
					{
						handleEOL();				// consume upon release
						return true;
					}
				}//_IME_ACTION_UNSPECIFIED

				return false;	// pass-through anything that didn't match to system
			}//_onEditorAction
		};//_OnEditorActionListener
//-------------------------
									// Listen for Taps to check cursor position
		if (inputClickListener == null) inputClickListener =
		new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (!in_TGET) return;
//				Basic.toaster(cx, "Tapped");	// debug
				if (badCursorPos() )				// illegal position
				{
				 putCursorAtEnd();					// move cursor to end
				}
			}//_onClick
		};//_OnClickListener

//-------------------------
								// trap the back key = user cancelled
		if (inputKeyListener == null) inputKeyListener =
		new OnKeyListener()
		{
			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				if (!in_TGET) return false;

				// If the event is a key-up event on the "back" button
				if ((event.getAction() == KeyEvent.ACTION_UP) &&
						(keyCode == KeyEvent.KEYCODE_BACK))
				{
//					Basic.toaster(cx, "TGET BackKey");		// debug
					tGet_done(String.valueOf((char)0));		// indicate by NUL
					return true;						// consumed onKey
				}
				return false;				// pass through anything else
			}//_onKey
		};//_onKeyListener
	}//_create_Listeners
//------------------------------------------
	public static void initView (Activity activity)	// create TGET edittext view
	{
//		final Context cx = Basic.mContextMgr.getContext(ContextManager.ACTIVITY_APP);

		LayoutInflater inflater = (LayoutInflater) activity.getLayoutInflater();
        View view = inflater.inflate (R.layout.tget, null );

		group = view.findViewById (R.id.tget_group);

		label = (TextView) view.findViewById (R.id.tget_label);
		label.setFocusable(false);
		label.setText("Label");
		label.setGravity (Gravity.CENTER);			// always centered

		et = (EditText) view.findViewById (R.id.tget_text);

	}//_initView
//------------------------------------------
												// init TGET edittext
	public static void initTGET (String inPrompt, boolean append, int kbType)
	{

		if (!append)								// if not append mode
		{
			et.setGravity (Gravity.CENTER);			// input at center
			label.setText(inPrompt);
			if (inPrompt.equals(""))
				label.setVisibility(View.GONE);		// no prompt
			else
				label.setVisibility(View.VISIBLE);	// prompt at top
			Prompt = "";
			et.setText (Run.TextInputString);		// default value
		}
		else										// append mode (regular TGET)
		{
			et.setGravity (Gravity.LEFT);			// input at left
			Prompt = inPrompt;						// Leftside Prompt
			et.setText (Prompt);					// is part of the text
		}

		PromptIndex = Prompt.length();				// points just after the prompt
		undoText = Prompt;							// our undo buffer
		undoCurs = PromptIndex;
//		Basic.toaster(cx, "InitTGET:"+Prompt);		// debug

		create_Listeners ();						// if neccessary, create listeners
		et.setImeOptions(EditorInfo.IME_ACTION_DONE);
//		et.setRawInputType(InputType.TYPE_CLASS_TEXT);
		et.setInputType(kbType);					// keyboard type
													// add listeners
		et.addTextChangedListener (inputTextWatcher);
		et.setOnEditorActionListener (inputActionListener);
		et.setOnClickListener (inputClickListener);
		et.setOnKeyListener (inputKeyListener);

		et.setEnabled (true);						// make sure widget is enabled
		et.setFocusable (true);
		et.setFocusableInTouchMode (true);
		et.setCursorVisible (true);
		et.setVisibility(View.VISIBLE);

		et.requestFocus ();
		putCursorAtEnd();

		et.postDelayed(new Runnable()				// bring up the software keyboard
		{
			@Override
			public void run()
			{
				imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
			}
		}, 100);//_postDelayed

	}//_initTGET
//------------------------------------------
	public static void hide ()				// hide the widget
	{
		et.clearFocus();
		et.setFocusable(false);
		et.setVisibility(View.GONE);
		et.setEnabled(false);

		label.setVisibility(View.GONE);
	}//_hide
//------------------------------------------
	private static boolean badCursorPos ()			// is the cursor position illegal ?
	{
		int cursor = et.getSelectionStart();

		if (cursor < PromptIndex) return true;		// less than the prompt is not allowed
//		if (cursor != et.getSelectionEnd()) return false; // don't allow selection
//		if (cursor > et.getText().length()) return false; // greater than end (- can't happen)
		return false;
	}
//------------------------------------------
	private static int getCursorPos ()				// get the current cursor position
	{
		return (et.getSelectionStart());
	}
//------------------------------------------
	private static boolean isCursorAtEnd ()			// is the cursor at end of text ?
	{
		return (getCursorPos() == PromptIndex);
	}
//------------------------------------------
	private static void putCursorAt (int i)			// move the cursor to i
	{
		et.setSelection(i); 						// place cursor here
		et.requestFocus();
	}
//------------------------------------------
	private static void putCursorAtEnd ()			// move the cursor to the end of text
	{
		putCursorAt (et.getText().length());		// place cursor at end
	}//_putCursorAtEnd
//------------------------------------------
	private static void handleEOL()
	{
		String s = et.getText().toString();
		if (PromptIndex < s.length())
			s= s.substring(PromptIndex);			// strip out the prompt
		else s="";
//		Basic.toaster(cx, "EOL>"+s+","+s.length());	// debug
		tGet_done(s);								// ..leaving the input text
	}//_handleEOL
//------------------------------------------
	public static void tGet_done (String text)		// all done, tidyup, return text
	{												// text | null=>Stop | "\0"=>backKey
		if (!in_TGET) return;
													// remove listeners
		et.removeTextChangedListener(inputTextWatcher);
		et.setOnEditorActionListener (null);
		et.setOnClickListener (null);
		et.setOnKeyListener (null);

		hide();										// hide widget
													// bring down keyboard
		imm.hideSoftInputFromWindow(et.getWindowToken(), 0);

		in_TGET = false;							// out of TGET
		synchronized (Run.LOCK)
		{
			Run.TextInputString = text;				// return result
			Run.mWaitForLock = false;				// release the lock that Run is waiting for
			Run.LOCK.notify();
		}

//	Basic.toaster(cx, "Shutdown");		// debug
	}//_tGet_done
}//_TGet
