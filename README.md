### LASKmobile ist die Android-App zur Dateneingabe für die Ackerschlagkartei [LASK](https://codeberg.org/ibinshoid/LASK).

So kann die Dokumentation gleich bei der Arbeit mit dem Handy gemacht werden und dann später ein Abgleich mit dem PC gemacht werden.  
* Der komplette Betrieb mit allen Feldern, Betriebsmitteln und Aktionen in allen Erntejahren ist auf dem Handy.
* Es ist keine Internetverbindung notwendig.
* Die Aktionen Saat, Bodenbearbeitung, Pflanzenschutz, Düngung und Ernte können eingegeben werden.
* Zu jeder Aktion werden die Kosten erfasst.
* Der Abgleich mit dem PC erfolgt über das USB-Kabel.
* Alle Daten bleiben bei Ihnen! Es wird nichts an irgendeine Cloud oder sonst wohin gesendet!

Die fertige App gibt es bei [F-Droid](https://f-droid.org/de/packages/com.rfo.LASKmobile/).

<img src="https://user-images.githubusercontent.com/16139662/36062750-898035cc-0e6a-11e8-8afc-cb37a7915852.png" width="30%"></img> <img src="https://user-images.githubusercontent.com/16139662/36062751-8cabf786-0e6a-11e8-8de0-8087a7380a0a.png" width="30%"></img> <img src="https://user-images.githubusercontent.com/16139662/36062754-8f45382c-0e6a-11e8-9c48-a5ac266da07d.png" width="30%"></img> 

